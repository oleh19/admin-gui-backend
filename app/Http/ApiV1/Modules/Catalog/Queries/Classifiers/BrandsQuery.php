<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\Classifiers;

use App\Http\ApiV1\Modules\Catalog\Queries\PimQuery;
use Ensi\PimClient\Api\BrandsApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\BrandResponse;
use Ensi\PimClient\Dto\SearchBrandsRequest;
use Ensi\PimClient\Dto\SearchBrandsResponse;
use Illuminate\Http\Request;

class BrandsQuery extends PimQuery
{
    public function __construct(Request $request, protected BrandsApi $brandsApi)
    {
        parent::__construct($request, SearchBrandsRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function searchById($brandId): BrandResponse
    {
        return $this->brandsApi->getBrand($brandId);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchBrandsResponse
    {
        return $this->brandsApi->searchBrands($request);
    }
}
