<?php

namespace App\Http\ApiV1\Modules\Units\Requests\SellerUsers;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchSellerUserRequest extends BaseFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            "seller_id" => ['sometimes', 'required', 'int'],
            "active" => ['sometimes', 'required', 'bool'],
            "login" => ['sometimes', 'required', 'string'],
            "last_name" => ['nullable', 'string'],
            "first_name" => ['nullable', 'string'],
            "middle_name" => ['nullable', 'string'],
            "email" => ['sometimes', 'required', 'email'],
            "phone" => ['sometimes', 'required', 'regex:/^\+7\d{10}$/'],
            "timezone" => ['sometimes', 'required', 'timezone'],
            "password" => ['sometimes', 'required', 'string'],
        ];
    }
}
