<?php

namespace App\Providers;

use Ackintosh\Ganesha;
use Ackintosh\Ganesha\Builder;
use Ackintosh\Ganesha\GuzzleMiddleware;
use Ackintosh\Ganesha\Storage\Adapter\Apcu as ApcuAdapter;
use Ensi\AdminAuthClient\AdminAuthClientProvider;
use Ensi\BasketsClient\BasketsClientProvider;
use Ensi\BuClient\BuClientProvider;
use Ensi\CmsClient\CmsClientProvider;
use Ensi\CommunicationManagerClient\CommunicationManagerClientProvider;
use Ensi\CrmClient\CrmClientProvider;
use Ensi\CustomerAuthClient\CustomerAuthClientProvider;
use Ensi\CustomersClient\CustomersClientProvider;
use Ensi\InternalMessenger\InternalMessengerClientProvider;
use Ensi\LogisticClient\LogisticClientProvider;
use Ensi\MarketingClient\MarketingClientProvider;
use Ensi\OffersClient\OffersClientProvider;
use Ensi\OmsClient\OmsClientProvider;
use Ensi\PimClient\PimClientProvider;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Utils;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;
use LogicException;

class OpenApiClientsServiceProvider extends ServiceProvider
{
    private const DEFAULT_TIMEOUT = 30;

    public function register(): void
    {
        $handler = $this->configureHandler();

        $this->registerService(
            handler: $handler,
            domain: 'catalog',
            serviceName: 'offers',
            configurationClassName: OffersClientProvider::$configuration,
            apisClassNames: OffersClientProvider::$apis
        );

        $this->registerService(
            handler: $handler,
            domain: 'catalog',
            serviceName: 'pim',
            configurationClassName: PimClientProvider::$configuration,
            apisClassNames: PimClientProvider::$apis
        );

        $this->registerService(
            handler: $handler,
            domain: 'communication',
            serviceName: 'communication-manager',
            configurationClassName: CommunicationManagerClientProvider::$configuration,
            apisClassNames: CommunicationManagerClientProvider::$apis
        );

        $this->registerService(
            handler: $handler,
            domain: 'communication',
            serviceName: 'internal-messenger',
            configurationClassName: InternalMessengerClientProvider::$configuration,
            apisClassNames: InternalMessengerClientProvider::$apis
        );

        $this->registerService(
            handler: $handler,
            domain: 'customers',
            serviceName: 'customer-auth',
            configurationClassName: CustomerAuthClientProvider::$configuration,
            apisClassNames: CustomerAuthClientProvider::$apis
        );

        $this->registerService(
            handler: $handler,
            domain: 'customers',
            serviceName: 'customers',
            configurationClassName: CustomersClientProvider::$configuration,
            apisClassNames: CustomersClientProvider::$apis
        );

        $this->registerService(
            handler: $handler,
            domain: 'customers',
            serviceName: 'crm',
            configurationClassName: CrmClientProvider::$configuration,
            apisClassNames: CrmClientProvider::$apis
        );

        $this->registerService(
            handler: $handler,
            domain: 'cms',
            serviceName: 'cms',
            configurationClassName: CmsClientProvider::$configuration,
            apisClassNames: CmsClientProvider::$apis
        );

        $this->registerService(
            handler: $handler,
            domain: 'logistic',
            serviceName: 'logistic',
            configurationClassName: LogisticClientProvider::$configuration,
            apisClassNames: LogisticClientProvider::$apis
        );

        $this->registerService(
            handler: $handler,
            domain: 'marketing',
            serviceName: 'marketing',
            configurationClassName: MarketingClientProvider::$configuration,
            apisClassNames: MarketingClientProvider::$apis
        );

        $this->registerService(
            handler: $handler,
            domain: 'orders',
            serviceName: 'oms',
            configurationClassName: OmsClientProvider::$configuration,
            apisClassNames: OmsClientProvider::$apis
        );

        $this->registerService(
            handler: $handler,
            domain: 'orders',
            serviceName: 'baskets',
            configurationClassName: BasketsClientProvider::$configuration,
            apisClassNames: BasketsClientProvider::$apis
        );

        $this->registerService(
            handler: $handler,
            domain: 'units',
            serviceName: 'admin-auth',
            configurationClassName: AdminAuthClientProvider::$configuration,
            apisClassNames: AdminAuthClientProvider::$apis
        );

        $this->registerService(
            handler: $handler,
            domain: 'units',
            serviceName: 'bu',
            configurationClassName: BuClientProvider::$configuration,
            apisClassNames: BuClientProvider::$apis
        );
    }

    private function configureHandler(): HandlerStack
    {
        $stack = new HandlerStack(Utils::chooseHandler());

        $stack->push(Middleware::httpErrors(), 'http_errors');
        $stack->push(Middleware::redirect(), 'allow_redirects');
        $stack->push(Middleware::prepareBody(), 'prepare_body');
        if (!config('ganesha.disable_middleware', false)) {
            $stack->push($this->configureGaneshaMiddleware());
        }

        return $stack;
    }

    private function configureGaneshaMiddleware(): GuzzleMiddleware
    {
        $config = config('ganesha');

        $ganesha = Builder::withRateStrategy()
            ->timeWindow($config['time_window'])
            ->failureRateThreshold($config['failure_rate_threshold'])
            ->minimumRequests($config['minimum_requests'])
            ->intervalToHalfOpen($config['interval_to_half_open'])
            ->adapter(new ApcuAdapter())
            ->build();


        $ganesha->subscribe(function ($event, $service, $message) {
            switch ($event) {
                case Ganesha::EVENT_TRIPPED:
                    Log::warning(
                        "Ganesha has tripped! It seems that a failure has occurred in {$service}. {$message}."
                    );

                    break;
                case Ganesha::EVENT_CALMED_DOWN:
                    Log::info(
                        "The failure in {$service} seems to have calmed down :). {$message}."
                    );

                    break;
                case Ganesha::EVENT_STORAGE_ERROR:
                    Log::error($message);

                    break;
            }
        });

        return new GuzzleMiddleware($ganesha);
    }

    private function registerService(HandlerStack $handler, string $domain, string $serviceName, string $configurationClassName, array $apisClassNames): void
    {
        $config = config("openapi-clients.$domain.$serviceName");
        if (!$config) {
            throw new LogicException("Config openapi-clients.$domain.$serviceName is not found");
        }

        $baseUri = $config['base_uri'];
        $this->app->bind($this->trimFQCN($configurationClassName), fn () => (new $configurationClassName())->setHost($baseUri));
        foreach ($apisClassNames as $api) {
            $this->app->when($this->trimFQCN($api))
                ->needs(ClientInterface::class)
                ->give(fn () => new Client([
                    'handler' => $handler,
                    'base_uri' => $baseUri,
                    'ganesha.service_name' => $domain . '_' . $serviceName,
                    'timeout' => $config['timeout'] ?? self::DEFAULT_TIMEOUT,
                ]));
        }
    }

    private function trimFQCN(string $name): string
    {
        return ltrim($name, '\\');
    }
}
