<?php

namespace App\Domain\Common\Data\Meta\Enum;

use Exception;
use JsonSerializable;

abstract class AbstractEnumInfo implements JsonSerializable
{
    protected ?string $endpointName = null;
    protected array $endpointParams = [];
    protected array $values = [];

    protected function addValue(string $id, string $title): self
    {
        $this->values[] = ['id' => $id, 'title' => $title];

        return $this;
    }

    public function jsonSerialize()
    {
        $result = [];
        if ($this->endpointName) {
            $result['endpoint'] = route($this->endpointName, $this->endpointParams, false);
        } elseif ($this->values) {
            $result['values'] = $this->values;
        } else {
            throw new Exception("Необходимо заполнить инфо о перечислении");
        }

        return $result;
    }
}
