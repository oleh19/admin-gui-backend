<?php

namespace App\Domain\Common\Data\Meta\Enum;

use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Ensi\LogisticClient\Dto\GetDeliveryMethodsResponse;
use GuzzleHttp\Promise\PromiseInterface;

class LogisticDeliveryMethodEnumInfo extends AbstractEnumInfo implements AsyncEnumInfo
{
    public function __construct(protected DeliveryServicesApi $deliveryServicesApi)
    {
    }

    public function requestAsync(): PromiseInterface
    {
        return $this->deliveryServicesApi->getDeliveryMethodsAsync();
    }

    /**
     * @param GetDeliveryMethodsResponse $response
     */
    public function processResponse($response)
    {
        foreach ($response->getData() as $deliveryMethod) {
            $this->addValue($deliveryMethod->getId(), $deliveryMethod->getName());
        }
    }
}
