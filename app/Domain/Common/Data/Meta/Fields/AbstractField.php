<?php

namespace App\Domain\Common\Data\Meta\Fields;

use App\Http\ApiV1\OpenApiGenerated\Dto\FieldFilterTypeEnum;
use Exception;
use JsonSerializable;

abstract class AbstractField implements JsonSerializable
{
    /** @var bool Поле является ссылкой на детальную страницу */
    public bool $detailLink = false;

    /** @var null|string Тип фильтра */
    public ?string $filter = null;
    /** @var null|string Символьный код поля для фильтрации */
    public ?string $filterKey = null;
    /** @var null|string Символьный код поля для фильтрации по диапазону (ОТ) */
    public ?string $filterRangeKeyFrom = null;
    /** @var null|string Символьный код поля для фильтрации по диапазону (ДО) */
    public ?string $filterRangeKeyTo = null;
    /** @var bool Фильтрация по этому полю выводится по-умолчанию */
    public bool $filterDefault = false;

    /** @var bool Возможна ли сортировка по полю */
    public bool $sort = false;
    /** @var null|string Символьный код поля для сортировки */
    public ?string $sortKey = null;
    /** @var bool Сортировка по этому полю является сортировкой по-умолчанию */
    public bool $sortDefault = false;

    /** @var bool Возможен ли вывод в таблицу */
    public bool $list = true;
    /** @var bool Поле выводится в таблицу по умолчанию */
    public bool $listDefault = false;

    public function __construct(
        public string $code,
        public string $name,
    ) {
        $this->init();
    }

    /**
     * Получить тип поля
     * @return string
     */
    abstract protected function type(): string;

    /**
     * Установить настройки поля по умолчанию
     */
    protected function init()
    {
        //
    }

    /**
     * Сбросить настройки фильтрации
     * @return $this
     */
    public function resetFilter(): self
    {
        $this->filter = null;
        $this->filterKey = null;
        $this->filterRangeKeyFrom = null;
        $this->filterRangeKeyTo = null;

        return $this;
    }

    /**
     * Разрешить обычную фильтрацию
     * @param null|string $filterKey Ключ фильтра
     * @return $this
     */
    public function filter(?string $filterKey = null): self
    {
        $this->resetFilter();
        $this->filter = FieldFilterTypeEnum::DEFAULT;
        $this->filterKey = $filterKey ?: $this->code;

        return $this;
    }

    /**
     * Разрешить фильтрацию по подстроке
     * @param null|string $filterKey Ключ фильтра
     * @return $this
     */
    public function filterLike(?string $filterKey = null): self
    {
        $this->resetFilter();
        $this->filter = FieldFilterTypeEnum::LIKE;
        $this->filterKey = $filterKey ?: "{$this->code}_like";

        return $this;
    }

    /**
     * Разрешить фильтрацию с множественным значением
     * @param null|string $filterKey Ключ фильтра
     * @return $this
     */
    public function filterMany(?string $filterKey = null): self
    {
        $this->resetFilter();
        $this->filter = FieldFilterTypeEnum::MANY;
        $this->filterKey = $filterKey ?: $this->code;

        return $this;
    }

    /**
     * Разрешить фильтрацию по диапазону
     * @param string|null $filterKeyFrom
     * @param string|null $filterKeyTo
     * @return $this
     */
    public function filterRange(?string $filterKeyFrom = null, ?string $filterKeyTo = null): self
    {
        $this->resetFilter();
        $this->filter = FieldFilterTypeEnum::RANGE;
        $this->filterRangeKeyFrom = $filterKeyFrom ?: "{$this->code}_from";
        $this->filterRangeKeyTo = $filterKeyTo ?: "{$this->code}_to";

        return $this;
    }

    /**
     * Отметить поле для фильтрации по-умолчанию выводимым
     * @return $this
     */
    public function filterDefault(): self
    {
        if (is_null($this->filter)) {
            throw new Exception("Нельзя установить по-умолчанию не включенный фильтр");
        }
        $this->filterDefault = true;

        return $this;
    }

    /**
     * Включена ли по умолчанию фильтрация по этому полю
     * @return bool
     */
    public function isFilterDefault(): bool
    {
        return $this->filterDefault;
    }

    /**
     * Сбросить настройки сортировки
     * @return $this
     */
    public function resetSort(): self
    {
        $this->sort = false;
        $this->sortKey = null;

        return $this;
    }

    /**
     * Разрешить сортировку по полю
     * @param null|string $sortKey Ключ сортировки
     * @return $this
     */
    public function sort(?string $sortKey = null): self
    {
        if (!$this->list) {
            throw new Exception("Нельзя установить сортировку по полю, которое не выводится в таблицу");
        }
        $this->resetSort();
        $this->sort = true;
        $this->sortKey = $sortKey ?: $this->code;

        return $this;
    }

    /**
     * Разрешить сортировку по полю и отметить поле для сортировки по-умолчанию
     * @param null|string $sortKey Ключ сортировки
     * @return $this
     */
    public function sortDefault(?string $sortKey = null): self
    {
        $this->sort($sortKey);
        $this->sortDefault = true;

        return $this;
    }

    /**
     * Является ли поле сортировкой по-умолчанию
     * @return bool
     */
    public function isSortDefault(): bool
    {
        return $this->sortDefault;
    }

    /**
     * Сбросить настройки отображения в таблице
     * @return $this
     */
    public function resetList(): self
    {
        $this->list = true;
        $this->listDefault = false;

        return $this;
    }

    /**
     * Не разрешать вывод в таблицу
     * @return $this
     */
    public function listHide(): self
    {
        $this->resetList();
        $this->resetSort();
        $this->list = false;

        return $this;
    }

    /**
     * Данное поле выводится по умолчанию в таблицу
     * @return $this
     */
    public function listDefault(): self
    {
        $this->resetList();
        $this->listDefault = true;

        return $this;
    }

    /**
     * Является ли поле сортировкой по-умолчанию
     * @return bool
     */
    public function isListDefault(): bool
    {
        return $this->listDefault;
    }

    /**
     * Отметить поле как ссылку на детальную страницу
     * @return $this
     */
    public function detailLink(): self
    {
        $this->detailLink = true;
        $this->listDefault();

        return $this;
    }

    /**
     * Является ли поле ссылкой на детальную страницу
     * @return bool
     */
    public function isDetailLink(): bool
    {
        return $this->detailLink;
    }

    public function jsonSerialize()
    {
        $response = [
            'code' => $this->code,
            'name' => $this->name,
            'list' => $this->list,
        ];

        $response['type'] = $this->type();

        $response['sort'] = $this->sort;
        if ($this->sort) {
            $response['sort_key'] = $this->sortKey;
        }

        $response['filter'] = $this->filter;
        if ($this->filter) {
            if ($this->filter == FieldFilterTypeEnum::RANGE) {
                $response['filter_range_key_from'] = $this->filterRangeKeyFrom;
                $response['filter_range_key_to'] = $this->filterRangeKeyTo;
            } else {
                $response['filter_key'] = $this->filterKey;
            }
        }

        return $response;
    }
}
