<?php

use App\Domain\Orders\Tests\Factories\Orders\OrderFactory;
use App\Domain\Orders\Tests\Factories\Refunds\RefundFactory;
use App\Domain\Orders\Tests\Factories\Refunds\RefundReasonFactory;
use App\Http\ApiV1\Modules\Orders\Tests\Refunds\Factories\RefundReasonRequestFactory;
use App\Http\ApiV1\Modules\Orders\Tests\Refunds\Factories\RefundRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use App\Http\ApiV1\Support\Tests\Factories\PaginationFactory;
use Ensi\OmsClient\Dto\RefundStatusEnum;
use Ensi\OmsClient\Dto\SearchRefundsRequest;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function PHPUnit\Framework\assertEquals;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'orders', 'orders.refunds');

test('GET /api/v1/orders/refunds/{id} 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $refundId = 1;

    $this->mockOmsRefundsApi()->allows([
        'getRefund' => RefundFactory::new()->makeResponseOne(),
    ]);

    getJson("/api/v1/orders/refunds/$refundId")
        ->assertStatus(200);
});

test('PATCH /api/v1/orders/refunds/{id} 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $refundId = 1;
    $refundStatus = RefundStatusEnum::getAllowableEnumValues()[0];

    $this->mockOmsRefundsApi()->allows([
        'patchRefund' => RefundFactory::new()->makeResponseOne(['id' => $refundId, 'status' => $refundStatus]),
    ]);

    patchJson("/api/v1/orders/refunds/$refundId", ['status' => $refundStatus])
        ->assertStatus(200)
        ->assertJsonPath('data.id', $refundId)
        ->assertJsonPath('data.status', $refundStatus);
});

test('POST /api/v1/orders/refunds:search 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $refundId = 1;
    $refundStatus = RefundStatusEnum::getAllowableEnumValues()[0];

    $this->mockOmsRefundsApi()->allows([
        'searchRefunds' => RefundFactory::new()->makeResponseSearch(['id' => $refundId, 'status' => $refundStatus]),
    ]);

    postJson("/api/v1/orders/refunds:search", ['pagination' => PaginationFactory::new()->makeOffsetRequest()])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $refundId)
        ->assertJsonPath('data.0.status', $refundStatus);
});

test('POST /api/v1/orders/refunds:search all includes 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $refundId = 1;
    $orderId = 1;
    $omsIncludes = [
        'order',
        'items',
        'reasons',
        'files',
    ];

    $order = OrderFactory::new()->make(['id' => $orderId]);
    $refundsResponse = RefundFactory::new()
        ->withOrder($order)
        ->withItem()
        ->withReason()
        ->withFile()
        ->makeResponseSearch(['id' => $refundId, 'order_id' => $orderId]);
    $this->mockOmsRefundsApi()
        ->shouldReceive('searchRefunds')
        ->withArgs(function ($arg) use ($omsIncludes) {
            /** @var SearchRefundsRequest $arg */
            assertEquals($omsIncludes, $arg->getInclude());

            return true;
        })
        ->andReturn($refundsResponse);

    postJson("/api/v1/orders/refunds:search", ['include' => $omsIncludes])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $refundId)
        ->assertJsonPath('data.0.order.id', $orderId)
        ->assertJsonCount(1, 'data.0.items')
        ->assertJsonCount(1, 'data.0.reasons')
        ->assertJsonCount(1, 'data.0.files');
});

test('POST /api/v1/orders/refunds 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $refundStatus = RefundStatusEnum::getAllowableEnumValues()[0];
    $refund = RefundRequestFactory::new()->make(['status' => $refundStatus]);

    $this->mockOmsRefundsApi()->allows([
        'createRefund' => RefundFactory::new()->makeResponseOne(['status' => $refundStatus]),
    ]);

    postJson("/api/v1/orders/refunds", $refund)
        ->assertStatus(200)
        ->assertJsonPath('data.status', $refundStatus);
});

test('POST /api/v1/orders/refund-reasons 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $name = 'refund name';
    $code = 'REFUND_CODE';

    $refund = RefundReasonRequestFactory::new()->make([
        'name' => $name,
        'code' => $code,
    ]);

    $this->mockOmsEnumsApi()->allows([
        'createRefundReason' => RefundReasonFactory::new()->makeResponseOne([
            'name' => $name,
            'code' => $code,
        ]),
    ]);

    postJson("/api/v1/orders/refund-reasons", $refund)
        ->assertStatus(200)
        ->assertJsonPath('data.name', $name)
        ->assertJsonPath('data.code', $code);
});

test('PATCH /api/v1/orders/refund-reasons/{id} 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $refundReasonId = 1;
    $name = 'new reason name';

    $this->mockOmsEnumsApi()->allows([
        'patchRefundReason' => RefundReasonFactory::new()->makeResponseOne(['id' => $refundReasonId, 'name' => $name]),
    ]);

    patchJson("/api/v1/orders/refund-reasons/$refundReasonId", ['name' => $name])
        ->assertStatus(200)
        ->assertJsonPath('data.id', $refundReasonId)
        ->assertJsonPath('data.name', $name);
});

test('GET /api/v1/orders/refund-reasons 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $id = 12345;
    $name = 'example name';

    $this->mockOmsEnumsApi()->allows([
        'getRefundReasons' => RefundReasonFactory::new()->makeResponseSearch(['id' => $id, 'name' => $name]),
    ]);

    getJson("/api/v1/orders/refund-reasons")
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $id)
        ->assertJsonPath('data.0.name', $name);
});
