<?php


namespace App\Http\ApiV1\Modules\Orders\Resources\Refunds;

use App\Domain\Orders\Data\Refunds\RefundData;
use App\Http\ApiV1\Modules\Orders\Resources\Orders\OrderItemsResource;
use App\Http\ApiV1\Modules\Orders\Resources\Orders\OrdersResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class RefundsResource
 * @package App\Http\ApiV1\Modules\Orders\Resources
 * @mixin RefundData
 */
class RefundsResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->refund->getId(),
            'order_id' => $this->refund->getOrderId(),
            'manager_id' => $this->refund->getManagerId(),
            'responsible_id' => $this->refund->getResponsibleId(),
            'source' => $this->refund->getSource(),
            'status' => $this->refund->getStatus(),
            'price' => $this->refund->getPrice(),
            'is_partial' => $this->refund->getIsPartial(),
            'user_comment' => $this->refund->getUserComment(),
            'rejection_comment' => $this->refund->getRejectionComment(),

            'created_at' => $this->dateTimeToIso($this->refund->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->refund->getUpdatedAt()),

            'order' => OrdersResource::make($this->whenNotNull($this->getOrder())),
            'items' => OrderItemsResource::collection($this->whenNotNull($this->getOrderItems())),
            'reasons' => RefundReasonsResource::collection($this->whenNotNull($this->refund->getReasons())),
            'files' => RefundFilesResource::collection($this->whenNotNull($this->refund->getFiles())),
        ];
    }
}
