<?php

namespace App\Http\ApiV1\Modules\Menu\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/**
 * Class MenuResource
 * @package App\Http\ApiV1\Modules\Menu\Resources
 */
class MenuResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'items' => $this->resource,
        ];
    }
}
