<?php

namespace App\Http\ApiV1\Modules\Customers\Queries;

use Ensi\CrmClient\Api\CustomerFavoritesApi;
use Ensi\CrmClient\Dto\SearchCustomerFavoritesRequest;
use Ensi\CrmClient\Dto\SearchCustomerFavoritesResponse;
use Illuminate\Http\Request;

class CustomersFavouritesQuery extends CrmQuery
{
    public function __construct(
        protected Request              $httpRequest,
        protected CustomerFavoritesApi $customerFavoritesApi
    ) {
        parent::__construct($httpRequest, SearchCustomerFavoritesRequest::class);
    }

    protected function search($request): SearchCustomerFavoritesResponse
    {
        return $this->customerFavoritesApi->searchCustomerFavorite($request);
    }
}
