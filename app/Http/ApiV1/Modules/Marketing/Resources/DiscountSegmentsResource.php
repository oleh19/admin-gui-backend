<?php


namespace App\Http\ApiV1\Modules\Marketing\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\MarketingClient\Dto\DiscountSegment;

/**
 * Class DiscountSegmentsResource
 * @package App\Http\ApiV1\Modules\Marketing\Resources
 * @mixin DiscountSegment
 */
class DiscountSegmentsResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),
            'discount_id' => $this->getDiscountId(),
            'segment_id' => $this->getSegmentId(),
        ];
    }
}
