<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Products;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class ReplaceAttributesValuesRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'values' => ['nullable', 'array'],
            'values.*.property_id' => ['required', 'integer'],
            'values.*.value' => ['present'],
        ];
    }
}
