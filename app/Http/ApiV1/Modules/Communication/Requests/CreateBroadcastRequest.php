<?php

namespace App\Http\ApiV1\Modules\Communication\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\CommunicationManagerClient\Dto\UserTypeEnum;
use Illuminate\Validation\Rule;

/**
 * Class CreateBroadcastRequest
 */
class CreateBroadcastRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'user_ids' => ['required', 'array'],
            'user_ids.*' => ['required', 'integer'],
            'user_type' => ['required', Rule::in(UserTypeEnum::getAllowableEnumValues())],
            'theme' => ['required', 'string'],
            'status_id' => ['required', 'integer'],
            'type_id' => ['required', 'integer'],
            'message' => ['required', 'string'],
            'files' => ['array'],
            'files.*' => ['string'],
      ];
    }
}
