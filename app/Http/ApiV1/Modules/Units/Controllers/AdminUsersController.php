<?php

namespace App\Http\ApiV1\Modules\Units\Controllers;

use App\Domain\Units\Actions\AdminUsers\AddRolesToAdminUserAction;
use App\Domain\Units\Actions\AdminUsers\CreateAdminUsersAction;
use App\Domain\Units\Actions\AdminUsers\DeleteRoleFromAdminUserAction;
use App\Domain\Units\Actions\AdminUsers\MassChangeActiveAction;
use App\Domain\Units\Actions\AdminUsers\PatchAdminUsersAction;
use App\Domain\Units\Actions\AdminUsers\RefreshPasswordTokenAction;
use App\Domain\Units\Actions\AdminUsers\SetPasswordForAdminUserAction;
use App\Http\ApiV1\Modules\Units\Queries\AdminUsersQuery;
use App\Http\ApiV1\Modules\Units\Requests\AdminUsers\AddRolesToAdminUserRequest;
use App\Http\ApiV1\Modules\Units\Requests\AdminUsers\CreateAdminUsersRequest;
use App\Http\ApiV1\Modules\Units\Requests\AdminUsers\DeleteRoleFromAdminUserRequest;
use App\Http\ApiV1\Modules\Units\Requests\AdminUsers\MassChangeActiveRequest;
use App\Http\ApiV1\Modules\Units\Requests\AdminUsers\PatchAdminUsersRequest;
use App\Http\ApiV1\Modules\Units\Requests\AdminUsers\SetPasswordForAdminUserRequest;
use App\Http\ApiV1\Modules\Units\Resources\AdminUserEnumValueResource;
use App\Http\ApiV1\Modules\Units\Resources\AdminUsersResource;
use App\Http\ApiV1\Support\Requests\CommonFilterEnumValuesRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class AdminUsersController
{
    public function search(AdminUsersQuery $query): AnonymousResourceCollection
    {
        return AdminUsersResource::collectPage($query->get());
    }

    public function searchEnumValues(CommonFilterEnumValuesRequest $request, AdminUsersQuery $query): AnonymousResourceCollection
    {
        return AdminUserEnumValueResource::collection($query->searchEnums());
    }

    public function create(CreateAdminUsersRequest $request, CreateAdminUsersAction $action): AdminUsersResource
    {
        return new AdminUsersResource($action->execute($request->validated()));
    }

    public function get(int $id, AdminUsersQuery $query): AdminUsersResource
    {
        return AdminUsersResource::make($query->find($id));
    }

    public function patch(int $id, PatchAdminUsersRequest $request, PatchAdminUsersAction $action): AdminUsersResource
    {
        return AdminUsersResource::make($action->execute($id, $request->validated()));
    }

    public function addRoles(int $id, AddRolesToAdminUserRequest $request, AddRolesToAdminUserAction $action): EmptyResource
    {
        $validated = $request->validated();
        $action->execute($id, $validated);

        return new EmptyResource();
    }

    public function deleteRole(int $id, DeleteRoleFromAdminUserRequest $request, DeleteRoleFromAdminUserAction $action): EmptyResource
    {
        $validated = $request->validated();
        $action->execute($id, $validated);

        return new EmptyResource();
    }

    public function setPassword(SetPasswordForAdminUserRequest $request, SetPasswordForAdminUserAction $action): EmptyResource
    {
        $action->execute($request->validated());

        return new EmptyResource();
    }

    public function refreshPasswordToken(int $id, RefreshPasswordTokenAction $action)
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function massChangeActive(MassChangeActiveRequest $request, MassChangeActiveAction $action)
    {
        $action->execute($request->validated());

        return new EmptyResource();
    }
}
