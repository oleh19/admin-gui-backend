<?php

namespace App\Domain\Logistic\Actions\DeliveryServices;

use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\AddPaymentMethodsToDeliveryServiceRequest;

class AddPaymentMethodsToDeliveryServiceAction
{
    public function __construct(protected DeliveryServicesApi $deliveryServicesApi)
    {
    }

    /**
     * @param int $deliveryServiceId
     * @param array $paymentMethods
     * @throws ApiException
     */
    public function execute(int $deliveryServiceId, array $paymentMethods): void
    {
        $dto = new AddPaymentMethodsToDeliveryServiceRequest();
        $dto->setPaymentMethods($paymentMethods);
        $this->deliveryServicesApi->addPaymentMethodsToDeliveryService($deliveryServiceId, $dto);
    }
}
