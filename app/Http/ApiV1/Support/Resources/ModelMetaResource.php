<?php

namespace App\Http\ApiV1\Support\Resources;

use App\Domain\Common\Data\Meta\Fields\AbstractField;
use Illuminate\Contracts\Support\Responsable;
use Symfony\Component\HttpFoundation\Response;

class ModelMetaResource implements Responsable
{
    /**
     * ModelMetaResource constructor.
     * @param AbstractField[] $fields
     */
    public function __construct(protected array $fields)
    {
    }

    /**
     * Create an HTTP response that represents the object.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function toResponse($request): Response
    {
        $defaultSort = null;
        $detailLink = null;
        $defaultList = [];
        $defaultFilter = [];
        foreach ($this->fields as $field) {
            if ($field->isSortDefault()) {
                $defaultSort = $field->code;
            }
            if ($field->isDetailLink()) {
                $detailLink = $field->code;
            }
            if ($field->isListDefault()) {
                $defaultList[] = $field->code;
            }
            if ($field->isFilterDefault()) {
                $defaultFilter[] = $field->code;
            }
        }

        return response()->json(['data' => [
            'fields' => $this->fields,
            'detail_link' => $detailLink,
            'default_sort' => $defaultSort,
            'default_list' => $defaultList,
            'default_filter' => $defaultFilter,
        ]]);
    }
}
