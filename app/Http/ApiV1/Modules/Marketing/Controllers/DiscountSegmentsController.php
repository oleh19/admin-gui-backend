<?php


namespace App\Http\ApiV1\Modules\Marketing\Controllers;

use App\Domain\Marketing\Actions\Discounts\CreateDiscountSegmentAction;
use App\Domain\Marketing\Actions\Discounts\DeleteDiscountSegmentAction;
use App\Domain\Marketing\Actions\Discounts\PatchDiscountSegmentAction;
use App\Http\ApiV1\Modules\Marketing\Queries\DiscountSegmentsQuery;
use App\Http\ApiV1\Modules\Marketing\Requests\CreateDiscountSegmentRequest;
use App\Http\ApiV1\Modules\Marketing\Requests\PatchDiscountSegmentRequest;
use App\Http\ApiV1\Modules\Marketing\Resources\DiscountSegmentsResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class DiscountSegmentsController
{
    public function findById(int $discountSegmentId, DiscountSegmentsQuery $query)
    {
        return new DiscountSegmentsResource($query->find($discountSegmentId));
    }

    public function search(DiscountSegmentsQuery $query)
    {
        return DiscountSegmentsResource::collectPage($query->get());
    }

    public function create(CreateDiscountSegmentRequest $request, CreateDiscountSegmentAction $action): DiscountSegmentsResource
    {
        return new DiscountSegmentsResource($action->execute($request->validated()));
    }

    public function patch(
        int $discountSegmentId,
        PatchDiscountSegmentRequest $request,
        PatchDiscountSegmentAction $action
    ): DiscountSegmentsResource {
        return new DiscountSegmentsResource($action->execute($discountSegmentId, $request->validated()));
    }

    public function delete(int $discountSegmentId, DeleteDiscountSegmentAction $action): EmptyResource
    {
        $action->execute($discountSegmentId);

        return new EmptyResource();
    }
}
