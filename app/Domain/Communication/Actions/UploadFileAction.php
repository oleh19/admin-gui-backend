<?php

namespace App\Domain\Communication\Actions;

use Ensi\InternalMessenger\Api\AttachmentsApi;

class UploadFileAction
{
    public function __construct(private AttachmentsApi $attachmentsApi)
    {
    }

    public function execute($file, $name = null)
    {
        return $this->attachmentsApi->createAttachment($file, $name)->getData();
    }
}
