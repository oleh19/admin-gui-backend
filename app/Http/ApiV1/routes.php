<?php

use Illuminate\Support\Facades\Route;

$modules = [
    "auth" => "Auth",
    "catalog" => "Catalog",
    "cms" => "Cms",
    "common" => "Common",
    "communication" => "Communication",
    "customers" => "Customers",
    "logistic" => "Logistic",
    "marketing" => "Marketing",
    "menu" => "Menu",
    "orders" => "Orders",
    "units" => "Units",
];

foreach ($modules as $prefix => $module) {
    Route::prefix($prefix)->group(__DIR__ . "/Modules/{$module}/routes.php");
}
