<?php

namespace App\Http\ApiV1\Modules\Logistic\Requests\DeliveryPrices;

use App\Http\ApiV1\OpenApiGenerated\Dto\LogisticDeliveryMethodEnum;
use App\Http\ApiV1\OpenApiGenerated\Dto\LogisticDeliveryServiceEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

/**
 * Class CreateOrReplaceDeliveryPriceRequest
 * @package App\Http\ApiV1\Modules\Logistic\Requests\DeliveryPrices
 */
class CreateOrReplaceDeliveryPriceRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'federal_district_id' => ['required', 'integer'],
            'region_id' => ['nullable', 'integer'],
            'region_guid' => ['nullable', 'string'],
            'delivery_service' => ['required', Rule::in(LogisticDeliveryServiceEnum::getAllowableEnumValues())],
            'delivery_method' => ['required', Rule::in(LogisticDeliveryMethodEnum::getAllowableEnumValues())],
            'price' => ['required', 'integer'],
      ];
    }
}
