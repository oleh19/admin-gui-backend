<?php

namespace App\Http\ApiV1\Modules\Units\Requests\SellerUsers;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateOrReplaceSellerUserRequest extends BaseFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            "seller_id" => ['required', 'int'],
            "active" => ['required', 'bool'],
            "login" => ['required', 'string'],
            "last_name" => ['nullable', 'string'],
            "first_name" => ['nullable', 'string'],
            "middle_name" => ['nullable', 'string'],
            "email" => ['required', 'email'],
            "phone" => ['required', 'regex:/^\+7\d{10}$/'],
            "timezone" => ['required', 'timezone'],
            "password" => ['required', 'string'],
        ];
    }
}
