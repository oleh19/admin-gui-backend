<?php

namespace App\Domain\Customers\Actions\Attributes;

use Ensi\CustomersClient\Dto\CustomerAttributeForCreateOrReplace;
use Ensi\CustomersClient\Dto\CustomerAttributes;

class ReplaceCustomerAttributeAction extends CustomerAttributeAction
{
    /**
     * @param int $attributeId
     * @param array $fields
     * @return CustomerAttributes
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function execute(int $attributeId, array $fields): CustomerAttributes
    {
        return $this->attributesApi->replaceCustomerAttribute(
            $attributeId,
            new CustomerAttributeForCreateOrReplace($fields)
        )->getData();
    }
}
