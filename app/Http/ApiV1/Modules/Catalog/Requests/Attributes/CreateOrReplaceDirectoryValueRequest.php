<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Attributes;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateOrReplaceDirectoryValueRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'code' => ['nullable', 'string'],
        ];
    }
}
