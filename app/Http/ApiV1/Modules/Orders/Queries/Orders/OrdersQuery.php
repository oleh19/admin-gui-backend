<?php


namespace App\Http\ApiV1\Modules\Orders\Queries\Orders;

use App\Domain\Customers\Data\CustomerData;
use App\Domain\Orders\Data\Orders\OrderData;
use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\CrmClient\Api\CustomersApi;
use Ensi\CrmClient\Dto\Customer;
use Ensi\CrmClient\Dto\PaginationTypeEnum as PaginationTypeEnumCrm;
use Ensi\CrmClient\Dto\RequestBodyPagination as RequestBodyPaginationCrm;
use Ensi\CrmClient\Dto\SearchCustomersFilter;
use Ensi\CrmClient\Dto\SearchCustomersRequest;
use Ensi\CustomerAuthClient\Api\UsersApi as UsersApiCustomerAuth;
use Ensi\CustomerAuthClient\Dto\PaginationTypeEnum as PaginationTypeEnumCustomerAuth;
use Ensi\CustomerAuthClient\Dto\RequestBodyPagination as RequestBodyPaginationCustomerAuth;
use Ensi\CustomerAuthClient\Dto\SearchUsersRequest as SearchUsersRequestCustomerAuth;
use Ensi\OffersClient\Api\OffersApi;
use Ensi\OffersClient\Dto\SearchOffersRequest;
use Ensi\OmsClient\Api\OrdersApi;
use Ensi\OmsClient\Dto\Order;
use Ensi\OmsClient\Dto\RequestBodyPagination as RequestBodyPaginationOms;
use Ensi\OmsClient\Dto\SearchOrdersRequest;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\PaginationTypeEnum as PaginationTypeEnumPim;
use Ensi\PimClient\Dto\RequestBodyPagination as RequestBodyPaginationPim;
use Ensi\PimClient\Dto\SearchProductsRequest;
use Ensi\AdminAuthClient\Api\UsersApi as UsersApiAdminAuth;
use Ensi\AdminAuthClient\Dto\PaginationTypeEnum as PaginationTypeEnumAdminAuth;
use Ensi\AdminAuthClient\Dto\RequestBodyPagination as RequestBodyPaginationAdminAuth;
use Ensi\AdminAuthClient\Dto\SearchUsersRequest as SearchUsersRequestAdminAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class OrdersQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;
    use QueryBuilderFindTrait;

    protected bool $withCustomerUser = false;
    protected bool $withCustomer = false;

    protected bool $withResponsible = false;

    protected bool $withOffers = false;

    protected bool $withProduct = false;
    protected bool $withProductImage = false;
    protected bool $withProductCategory = false;
    protected bool $withProductBrand = false;

    protected bool $isShipmentsOrderItems = false;

    public function __construct(
        protected Request $httpRequest,
        protected OrdersApi $ordersApi,
        protected CustomersApi $customersApi,
        protected UsersApiCustomerAuth $customerUsersApi,
        protected UsersApiAdminAuth $adminUsersApi,
        protected OffersApi $offersApi,
        protected ProductsApi $productsApi,
    ) {
        parent::__construct($httpRequest);
    }

    protected function paginationClass(): string
    {
        return RequestBodyPaginationOms::class;
    }

    protected function requestGetClass(): string
    {
        return SearchOrdersRequest::class;
    }

    protected function search($request)
    {
        return $this->ordersApi->searchOrders($request);
    }

    protected function searchById($id)
    {
        return $this->ordersApi->getOrder($id, $this->getInclude());
    }

    protected function convertGetToItems($response)
    {
        return $this->convertArray($response->getData());
    }

    protected function convertFindToItem($response)
    {
        return current($this->convertArray([$response->getData()]));
    }

    protected function convertArray(array $orders)
    {
        /** @var Collection|Order[] $orders */
        $orders = collect($orders);

        $customers = $this->loadCustomers($this->prepareFilterValues($orders->pluck('customer_id')));
        $customerUsers = $this->loadCustomerUsers($this->prepareFilterValues($customers->pluck('user_id')));
        $adminUsers = $this->loadAdminUsers($this->prepareFilterValues($orders->pluck('responsible_id')));
        $offers = $this->loadOffers($this->prepareFilterValues($this->getOrderItems($orders)->pluck('offer_id')));
        $products = $this->loadProducts(array_values(array_unique(Arr::pluck($offers, 'product_id'))));

        $ordersData = [];
        foreach ($orders as $order) {
            $orderData = new OrderData($order);

            /** @var Customer $customer */
            $customer = $customers->get($order->getCustomerId());
            if ($customer) {
                $orderData->customer = new CustomerData($customer);
                $orderData->customer->user = $customerUsers->get($customer->getUserId());
            }
            $orderData->offers = $offers;
            $orderData->products = $products;

            $orderData->responsible = $order->getResponsibleId() ? $adminUsers->get($order->getResponsibleId()) : null;

            $ordersData[] = $orderData;
        }

        return $ordersData;
    }

    protected function getHttpInclude(): array
    {
        $httpIncludes = parent::getHttpInclude();

        $includes = [];
        foreach ($httpIncludes as $include) {
            switch ($include) {
                case "customer":
                    $this->withCustomer();

                    break;
                case "customer.user":
                    $this->withUser();

                    break;
                case "orderItems.product":
                    $this->withProduct();

                    break;
                case "orderItems.product.images":
                    $this->withProductImage();

                    break;
                case "orderItems.product.category":
                    $this->withProductCategory();

                    break;
                case "orderItems.product.brand":
                    $this->withProductBrand();

                    break;
                case "deliveries.shipments.orderItems":
                    $this->withShipmentsOrderItems();

                    break;
                case "responsible":
                    $this->withResponsible();

                    break;
                default:
                    $includes[] = $include;
            }
        }

        return $includes;
    }

    public function withShipmentsOrderItems(): self
    {
        $this->include[] = 'deliveries.shipments.orderItems';
        $this->isShipmentsOrderItems = true;

        return $this;
    }

    public function withUser(): self
    {
        $this->withCustomerUser = true;
        $this->withCustomer();

        return $this;
    }

    public function withCustomer(): self
    {
        $this->withCustomer = true;

        return $this;
    }

    public function withResponsible(): self
    {
        $this->withResponsible = true;

        return $this;
    }

    public function withOffer(): self
    {
        $this->withOffers = true;

        return $this;
    }

    public function withProduct(): self
    {
        $this->withProduct = true;
        $this->withOffer();

        return $this;
    }

    public function withProductImage(): self
    {
        $this->withProductImage = true;
        $this->withProduct();

        return $this;
    }

    public function withProductCategory(): self
    {
        $this->withProductCategory = true;
        $this->withProduct();

        return $this;
    }

    public function withProductBrand(): self
    {
        $this->withProductBrand = true;
        $this->withProduct();

        return $this;
    }

    protected function getOrderItems(Collection $orders): Collection
    {
        if ($this->isShipmentsOrderItems) {
            return $orders
                ->pluck('deliveries')->collapse()
                ->pluck('shipments')->collapse()
                ->pluck('order_items')->collapse();
        }

        return collect();
    }

    protected function loadCustomers(array $customerIds): Collection
    {
        if (!$this->withCustomer || !$customerIds) {
            return collect();
        }

        $request = new SearchCustomersRequest();
        $request->setFilter(new SearchCustomersFilter(['id' => $customerIds,]));
        $request->setPagination(
            (new RequestBodyPaginationCrm())
                ->setLimit(count($customerIds))
                ->setType(PaginationTypeEnumCrm::CURSOR)
        );
        $response = $this->customersApi->searchCustomers($request);

        return collect($response->getData())->keyBy('id');
    }

    protected function loadCustomerUsers(array $userIds): Collection
    {
        if (!$this->withCustomerUser || !$userIds) {
            return collect();
        }

        $request = new SearchUsersRequestCustomerAuth();
        $request->setFilter((object)[
            'id' => $userIds,
        ]);
        $request->setPagination(
            (new RequestBodyPaginationCustomerAuth())
                ->setLimit(count($userIds))
                ->setType(PaginationTypeEnumCustomerAuth::CURSOR)
        );
        $response = $this->customerUsersApi->searchUsers($request);

        return collect($response->getData())->keyBy('id');
    }

    protected function loadAdminUsers(array $userIds): Collection
    {
        if (!$this->withResponsible || !$userIds) {
            return collect();
        }

        $request = new SearchUsersRequestAdminAuth();
        $request->setFilter((object)[
            'id' => $userIds,
        ]);
        $request->setPagination(
            (new RequestBodyPaginationAdminAuth())
                ->setLimit(count($userIds))
                ->setType(PaginationTypeEnumAdminAuth::CURSOR)
        );
        $response = $this->adminUsersApi->searchUsers($request);

        return collect($response->getData())->keyBy('id');
    }

    protected function loadOffers(array $offerIds): array
    {
        if (!$this->withOffers || !$offerIds) {
            return [];
        }

        $response = $this->offersApi->searchOffers((new SearchOffersRequest())->setFilter((object)[
            'id' => $offerIds,
        ]));

        return $response->getData();
    }

    protected function loadProducts(array $productIds): array
    {
        if (!$this->withProduct || !$productIds) {
            return [];
        }

        $request = new SearchProductsRequest();
        $request->setFilter((object)[
            'id' => $productIds,
        ]);
        $includes = [];
        if ($this->withProductCategory) {
            $includes[] = 'category';
        }
        if ($this->withProductBrand) {
            $includes[] = 'brand';
        }
        if ($this->withProductImage) {
            $includes[] = 'images';
        }
        if ($includes) {
            $request->setInclude($includes);
        }
        $request->setPagination(
            (new RequestBodyPaginationPim())
                ->setLimit(count($productIds))
                ->setType(PaginationTypeEnumPim::CURSOR)
        );
        $response = $this->productsApi->searchProducts($request);

        return $response->getData();
    }

    protected function prepareFilterValues(Collection $values): array
    {
        return $values->unique()->values()->filter()->all();
    }
}
