<?php

namespace App\Http\ApiV1\Support\Queries;

/**
 * @mixin QueryBuilder
 */
trait QueryBuilderSortTrait
{
    protected function fillSort($request)
    {
        $httpSort = $this->httpRequest->get('sort');
        if ($httpSort) {
            $request->setSort($httpSort);
        }
    }
}
