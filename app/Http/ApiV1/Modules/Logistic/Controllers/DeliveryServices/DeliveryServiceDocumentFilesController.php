<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryServices;

use App\Domain\Logistic\Actions\DeliveryServiceDocument\DeleteDeliveryServiceDocumentFileAction;
use App\Domain\Logistic\Actions\DeliveryServiceDocument\SaveDeliveryServiceDocumentFileAction;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryServices\UploadDeliveryServiceDocumentFileRequest;
use App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices\DeliveryServiceDocumentsResource;

class DeliveryServiceDocumentFilesController
{
    public function upload(
        int $deliveryServiceDocumentId,
        UploadDeliveryServiceDocumentFileRequest $request,
        SaveDeliveryServiceDocumentFileAction $action
    ): DeliveryServiceDocumentsResource {
        return new DeliveryServiceDocumentsResource($action->execute($deliveryServiceDocumentId, $request->file('file')));
    }

    public function delete(
        int $deliveryServiceDocumentId,
        DeleteDeliveryServiceDocumentFileAction $action
    ): DeliveryServiceDocumentsResource {
        return new DeliveryServiceDocumentsResource($action->execute($deliveryServiceDocumentId));
    }
}
