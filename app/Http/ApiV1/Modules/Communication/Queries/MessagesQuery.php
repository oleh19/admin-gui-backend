<?php

namespace App\Http\ApiV1\Modules\Communication\Queries;

use App\Domain\Communication\Data\MessageData;
use Ensi\CommunicationManagerClient\ApiException;
use Ensi\InternalMessenger\Api\MessagesApi;
use Ensi\InternalMessenger\Dto\Message;
use Ensi\InternalMessenger\Dto\SearchMessagesRequest;
use Ensi\InternalMessenger\Dto\SearchMessagesResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class MessagesQuery extends CommunicationQuery
{
    public function __construct(Request $request, private MessagesApi $api)
    {
        parent::__construct($request, SearchMessagesRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchMessagesResponse
    {
        return $this->api->searchMessages($request);
    }

    protected function convertGetToItems($response)
    {
        return $this->convertArray($response->getData());
    }

    protected function convertArray(array $messages)
    {
        /** @var Collection|Message[] $messages */
        $messages = collect($messages);

        $messagesData = [];
        foreach ($messages as $message) {
            $messageData = new MessageData($message);

            $messagesData[] = $messageData;
        }

        return $messagesData;
    }
}
