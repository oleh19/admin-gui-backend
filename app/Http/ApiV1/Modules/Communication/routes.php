<?php

use App\Http\ApiV1\Modules\Communication\Controllers\BroadcastsController;
use App\Http\ApiV1\Modules\Communication\Controllers\ChannelsController;
use App\Http\ApiV1\Modules\Communication\Controllers\ChatsController;
use App\Http\ApiV1\Modules\Communication\Controllers\FilesController;
use App\Http\ApiV1\Modules\Communication\Controllers\MessagesController;
use App\Http\ApiV1\Modules\Communication\Controllers\StatusesController;
use App\Http\ApiV1\Modules\Communication\Controllers\ThemesController;
use App\Http\ApiV1\Modules\Communication\Controllers\TypesController;
use Illuminate\Support\Facades\Route;

// Статусы
Route::post('statuses:search', [StatusesController::class, 'search']);
Route::prefix('statuses')->group(function () {
    Route::prefix('{id}')->group(function () {
        Route::patch('', [StatusesController::class, 'patch']);
        Route::delete('', [StatusesController::class, 'delete']);
    });
    Route::post('', [StatusesController::class, 'create']);
});

// Темы
Route::post('themes:search', [ThemesController::class, 'search']);
Route::prefix('themes')->group(function () {
    Route::prefix('{id}')->group(function () {
        Route::patch('', [ThemesController::class, 'patch']);
        Route::delete('', [ThemesController::class, 'delete']);
    });
    Route::post('', [ThemesController::class, 'create']);
});

// Типы
Route::post('types:search', [TypesController::class, 'search']);
Route::prefix('types')->group(function () {
    Route::prefix('{id}')->group(function () {
        Route::patch('', [TypesController::class, 'patch']);
        Route::delete('', [TypesController::class, 'delete']);
    });
    Route::post('', [TypesController::class, 'create']);
});

// Каналы
Route::post('channels:search', [ChannelsController::class, 'search']);
Route::prefix('channels')->group(function () {
    Route::prefix('{id}')->group(function () {
        Route::patch('', [ChannelsController::class, 'patch']);
        Route::delete('', [ChannelsController::class, 'delete']);
    });
    Route::post('', [ChannelsController::class, 'create']);
});

//region Сообщения
Route::post('messages:search', [MessagesController::class, 'search']);
Route::post('messages', [MessagesController::class, 'create']);
//endregion

//region Чаты
Route::post('chats:search', [ChatsController::class, 'search']);
Route::post('chats', [ChatsController::class, 'create']);
Route::patch('chats/{chatId}', [ChatsController::class, 'patch']);
//endregion

Route::post('file-upload', [FilesController::class, 'upload']);
Route::post('file-delete', [FilesController::class, 'delete']);

Route::post('broadcasts', [BroadcastsController::class, 'create']);
