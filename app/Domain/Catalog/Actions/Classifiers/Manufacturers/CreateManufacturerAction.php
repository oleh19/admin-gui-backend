<?php

namespace App\Domain\Catalog\Actions\Classifiers\Manufacturers;

use Ensi\PimClient\Api\ManufacturersApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\CreateManufacturerRequest;
use Ensi\PimClient\Dto\Manufacturer;

class CreateManufacturerAction
{
    public function __construct(
        private ManufacturersApi $manufacturersApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): Manufacturer
    {
        $request = new CreateManufacturerRequest($fields);

        return $this->manufacturersApi->createManufacturer($request)->getData();
    }
}
