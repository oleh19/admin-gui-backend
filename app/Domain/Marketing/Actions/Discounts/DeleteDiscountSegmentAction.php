<?php

namespace App\Domain\Marketing\Actions\Discounts;

use Ensi\MarketingClient\Api\DiscountSegmentsApi;

class DeleteDiscountSegmentAction
{
    public function __construct(protected DiscountSegmentsApi $discountSegmentsApi)
    {
    }

    public function execute(int $discountSegmentId): void
    {
        $this->discountSegmentsApi->deleteDiscountSegment($discountSegmentId);
    }
}
