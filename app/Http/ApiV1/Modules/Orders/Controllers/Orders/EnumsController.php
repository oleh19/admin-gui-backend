<?php


namespace App\Http\ApiV1\Modules\Orders\Controllers\Orders;

use App\Http\ApiV1\Modules\Orders\Resources\Orders\Enums\DeliveryStatusesResource;
use App\Http\ApiV1\Modules\Orders\Resources\Orders\Enums\OrderSourcesResource;
use App\Http\ApiV1\Modules\Orders\Resources\Orders\Enums\OrderStatusesResource;
use App\Http\ApiV1\Modules\Orders\Resources\Orders\Enums\PaymentMethodsResource;
use App\Http\ApiV1\Modules\Orders\Resources\Orders\Enums\PaymentStatusesResource;
use App\Http\ApiV1\Modules\Orders\Resources\Orders\Enums\ShipmentStatusesResource;
use Ensi\OmsClient\Api\EnumsApi;

class EnumsController
{
    public function orderStatuses(EnumsApi $enumsApi)
    {
        return OrderStatusesResource::collection($enumsApi->getOrderStatuses()->getData());
    }

    public function orderSources(EnumsApi $enumsApi)
    {
        return OrderSourcesResource::collection($enumsApi->getOrderSources()->getData());
    }

    public function paymentMethods(EnumsApi $enumsApi)
    {
        return PaymentMethodsResource::collection($enumsApi->getPaymentMethods()->getData());
    }

    public function paymentStatuses(EnumsApi $enumsApi)
    {
        return PaymentStatusesResource::collection($enumsApi->getPaymentStatuses()->getData());
    }

    public function deliveryStatuses(EnumsApi $enumsApi)
    {
        return DeliveryStatusesResource::collection($enumsApi->getDeliveryStatuses()->getData());
    }

    public function shipmentStatuses(EnumsApi $enumsApi)
    {
        return ShipmentStatusesResource::collection($enumsApi->getShipmentStatuses()->getData());
    }
}
