<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Variants;

use App\Http\ApiV1\Modules\Catalog\Resources\Attributes\ProductAttributesResource;
use App\Http\ApiV1\Modules\Catalog\Resources\Products\ProductsResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\VariantGroup;

/**
 * @mixin VariantGroup
 */
class VariantGroupsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'product_ids' => $this->getProductIds(),
            'attribute_ids' => $this->getAttributeIds(),

            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),

            'products' => ProductsResource::collection(
                $this->whenNotNull($this->getProducts())
            ),
            'attributes' => ProductAttributesResource::collection(
                $this->whenNotNull($this->getAttributes())
            ),
        ];
    }
}
