<?php

namespace App\Domain\Catalog\Actions\Categories;

use Ensi\PimClient\Api\CategoriesApi;
use Ensi\PimClient\Dto\Category;
use Ensi\PimClient\Dto\ReplaceCategoryRequest;

class ReplaceCategoryAction
{
    public function __construct(private CategoriesApi $api)
    {
    }

    public function execute(int $categoryId, array $fields): Category
    {
        $request = new ReplaceCategoryRequest($fields);

        return $this->api->replaceCategory($categoryId, $request)->getData();
    }
}
