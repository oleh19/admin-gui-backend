<?php

namespace App\Http\ApiV1\Support\Tests\Factories;

use App\Http\ApiV1\OpenApiGenerated\Dto\PaginationTypeEnum;

class PaginationFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $limit = $this->faker->numberBetween(1, 200);
        $offset = $this->faker->numberBetween(0, 200);

        return [
            'type' => $this->faker->randomElement(PaginationTypeEnum::getAllowableEnumValues()),
            'cursor' => $this->faker->bothify('******************************'),
            'next_cursor' => $this->faker->bothify('******************************'),
            'previous_cursor' => $this->faker->bothify('******************************'),
            'limit' => $limit,
            'offset' => $offset,
            'total' => $this->faker->numberBetween($limit + $offset, $limit + $offset + 200),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function makeOffsetByClass(string $className)
    {
        return new $className(
            $this->only(['type', 'limit', 'offset', 'total'])->make(['type' => PaginationTypeEnum::OFFSET]),
        );
    }

    public function makeOffsetRequest(): array
    {
        return $this->only(['type', 'limit', 'offset'])->make(['type' => PaginationTypeEnum::OFFSET]);
    }
}
