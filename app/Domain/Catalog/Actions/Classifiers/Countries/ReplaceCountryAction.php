<?php

namespace App\Domain\Catalog\Actions\Classifiers\Countries;

use Ensi\PimClient\Api\CountriesApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\Country;
use Ensi\PimClient\Dto\ReplaceCountryRequest;

class ReplaceCountryAction
{
    public function __construct(
        private CountriesApi $countriesApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $countryId, array $fields): Country
    {
        $request = new ReplaceCountryRequest($fields);

        return $this->countriesApi->updateCountry($countryId, $request)->getData();
    }
}
