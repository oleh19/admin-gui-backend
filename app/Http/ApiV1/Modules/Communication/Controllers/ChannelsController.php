<?php

namespace App\Http\ApiV1\Modules\Communication\Controllers;

use App\Domain\Communication\Actions\CreateChannelAction;
use App\Domain\Communication\Actions\DeleteChannelAction;
use App\Domain\Communication\Actions\PatchChannelAction;
use App\Http\ApiV1\Modules\Communication\Queries\SearchChannelsQuery;
use App\Http\ApiV1\Modules\Communication\Requests\CreateChannelRequest;
use App\Http\ApiV1\Modules\Communication\Requests\PatchChannelRequest;
use App\Http\ApiV1\Modules\Communication\Resources\ChannelsResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class ChannelsController
{
    public function search(SearchChannelsQuery $query)
    {
        return ChannelsResource::collectPage($query->get());
    }

    public function create(CreateChannelRequest $request, CreateChannelAction $action): ChannelsResource
    {
        return new ChannelsResource($action->execute($request->validated()));
    }

    public function patch(int $channelId, PatchChannelRequest $request, PatchChannelAction $action): ChannelsResource
    {
        return new ChannelsResource($action->execute($channelId, $request->validated()));
    }

    public function delete(int $channelId, DeleteChannelAction $action): EmptyResource
    {
        $action->execute($channelId);

        return new EmptyResource();
    }
}
