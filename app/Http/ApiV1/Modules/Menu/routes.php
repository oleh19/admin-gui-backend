<?php

use App\Http\ApiV1\Modules\Menu\Controllers\MenuController;
use Illuminate\Support\Facades\Route;

Route::get('/', [MenuController::class, 'menu']);
