<?php

namespace App\Http\ApiV1\Modules\Logistic\Requests\DeliveryPrices;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

/**
 * Class GetDeliveryPriceRequest
 * @package App\Http\ApiV1\Modules\Logistic\Requests\DeliveryPrices
 */
class GetDeliveryPriceRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'include' => ['nullable', 'string'],
      ];
    }
}
