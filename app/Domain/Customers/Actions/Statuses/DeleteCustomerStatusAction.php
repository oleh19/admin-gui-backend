<?php

namespace App\Domain\Customers\Actions\Statuses;

class DeleteCustomerStatusAction extends CustomerStatusAction
{
    /**
     * @param int $statusId
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function execute(int $statusId): void
    {
        $this->statusApi->deleteCustomerStatus($statusId);
    }
}
