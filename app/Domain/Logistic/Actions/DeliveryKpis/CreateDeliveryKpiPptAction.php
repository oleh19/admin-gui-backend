<?php


namespace App\Domain\Logistic\Actions\DeliveryKpis;

use Ensi\LogisticClient\Api\KpiApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\CreateDeliveryKpiPptRequest;
use Ensi\LogisticClient\Dto\DeliveryKpiPpt;

/**
 * Class CreateDeliveryKpiPptAction
 * @package App\Domain\Logistic\Actions\DeliveryKpis
 */
class CreateDeliveryKpiPptAction
{
    /**
     * CreateDeliveryKpiPptAction constructor.
     * @param KpiApi $kpiApi
     */
    public function __construct(protected KpiApi $kpiApi)
    {
    }

    /**
     * @param int $sellerId
     * @param array $fields
     * @return DeliveryKpiPpt
     * @throws ApiException
     */
    public function execute(int $sellerId, array $fields): DeliveryKpiPpt
    {
        $request = new CreateDeliveryKpiPptRequest();
        $request->setPpt($fields['ppt']);

        return $this->kpiApi->createDeliveryKpiPpt($sellerId, $request)->getData();
    }
}
