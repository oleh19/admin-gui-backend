<?php

namespace App\Domain\Marketing\Actions\Discounts;

use Ensi\MarketingClient\Api\DiscountsApi;

class DeleteDiscountAction
{
    public function __construct(protected DiscountsApi $discountsApi)
    {
    }

    public function execute(int $discountId): void
    {
        $this->discountsApi->deleteDiscount($discountId);
    }
}
