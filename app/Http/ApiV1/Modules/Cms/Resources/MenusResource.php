<?php

namespace App\Http\ApiV1\Modules\Cms\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CmsClient\Dto\Menu;
use Illuminate\Http\Request;

/** @mixin Menu */
class MenusResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $items = $this->getItems();
        $itemsTree = $this->getItemsTree();

        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'code' => $this->getCode(),
            'items' => MenuItemsResource::collection($this->when(!is_null($items), $items)),
            'itemsTree' => MenuTreesResource::collection($this->when(!is_null($itemsTree), $itemsTree)),
        ];
    }
}
