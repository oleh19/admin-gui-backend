<?php

use App\Domain\Customers\Tests\Factories\CustomerFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/customers/customer-enum-values:search 200', function ($key, $value) {
    /** @var ApiV1ComponentTestCase $this */
    $user_id = 1;
    $email = 'test@mail.com';
    $this->mockCustomersCustomersApi()->allows([
        'searchCustomers' => CustomerFactory::new()->makeResponseSearch(['user_id' => $user_id, 'email' => $email]),
    ]);

    postJson("/api/v1/customers/customer-enum-values:search", ['filter' => [$key => $value]])->assertStatus(200);
})->with([
    ['id', [1, 2]],
    ['query', 'test'],
]);
