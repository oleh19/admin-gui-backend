<?php


namespace App\Http\ApiV1\Modules\Marketing\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\MarketingClient\Dto\Discount;

/**
 * Class DiscountsResource
 * @package App\Http\ApiV1\Modules\Marketing\Resources
 * @mixin Discount
 */
class DiscountsResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),
            'type' => $this->getType(),
            'user_id' => $this->getUserId(),
            'seller_id' => $this->getSellerId(),
            'name' => $this->getName(),
            'value_type' => $this->getValueType(),
            'value' => $this->getValue(),
            'status' => $this->getStatus(),
            'start_date' => $this->dateTimeToIso($this->getStartDate()),
            'end_date' => $this->dateTimeToIso($this->getEndDate()),
            'promo_code_only' => $this->getPromoCodeOnly(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'offers' => DiscountOffersResource::collection($this->whenNotNull($this->getOffers())),
            'brands' => DiscountBrandsResource::collection($this->whenNotNull($this->getBrands())),
            'categories' => DiscountCategoriesResource::collection($this->whenNotNull($this->getCategories())),
            'segments' => DiscountSegmentsResource::collection($this->whenNotNull($this->getSegments())),
            'conditions' => DiscountConditionsResource::collection($this->whenNotNull($this->getConditions())),
        ];
    }
}
