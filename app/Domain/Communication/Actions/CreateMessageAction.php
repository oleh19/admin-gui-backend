<?php

namespace App\Domain\Communication\Actions;

use App\Domain\Communication\Data\MessageData;
use Ensi\InternalMessenger\Api\MessagesApi;
use Ensi\InternalMessenger\Dto\MessageForCreate;

class CreateMessageAction
{
    public function __construct(protected MessagesApi $messagesApi)
    {
    }

    public function execute(array $fields): MessageData
    {
        $message = $this->messagesApi->createMessage(new MessageForCreate($fields))->getData();

        return new MessageData($message);
    }
}
