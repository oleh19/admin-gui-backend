ProductReadonlyProperties:
  type: object
  properties:
    id:
      type: integer
      description: Идентификатор товара
      example: 15324
    created_at:
      description: "Дата создания"
      format: date-time
      type: string
      example: "2021-06-19T10:36:24.000000Z"

ProductFillableProperties:
  type: object
  properties:
    external_id:
      type: string
      description: Код товара
      example: "14211"
    name:
      type: string
      description: Название товара
      example: "Яблочный сок"
    code:
      type: string
      description: Код товара (выводится в URL)
      example: "steyk-miratorg-ribay-ohlazhdennyy-320-g"
    description:
      type: string
      description: Описание товара
      example: "Свежевыжатый яблочный сок"
    product_type_id:
      type: integer
      description: Идентификатор типа товара
      example: 256
    brand_id:
      type: integer
      description: Идентификатор бренда товара
      example: 256
    category_id:
      type: integer
      description: Идентификатор категории
      example: 11
    manufacturer_id:
      type: integer
      description: Идентификатор производителя товара
      example: 128
    country_id:
      type: integer
      description: Идентификатор страны-производителя
      example: 64
    barcode:
      type: string
      description: Артикул (EAN)
      example: "1003912039"
    weight:
      type: number
      description: Масса нетто, кг
      example: 0.5
    weight_gross:
      type: number
      description: Масса брутто, г
      example: 0.75
    length:
      type: integer
      description: Длина, мм
      example: 100
    width:
      type: integer
      description: Ширина, мм
      example: 150
    height:
      type: integer
      description: Высота, мм
      example: 50
    ingredients:
      type: string
      description: Состав
      example: "Яблочный сок, вода"
    is_new:
      type: boolean
      description: Флажок новинки
      example: true
    archive:
      type: integer
      description: Тип архивности из CatalogProductArchiveTypeEnum
      example: 1

ProductSaleProperties:
  type: object
  properties:
    sale_active:
      type: boolean
      description: На витрине
      example: false
    cost:
      type: number
      description: Цена товара (без учета скидок)
      example: 100
    price:
      type: number
      description: Цена товара (с учетом скидок)
      example: 99

ProductRequiredProperties:
  type: object
  required:
    - name
    - external_id

ProductIncludes:
  type: object
  properties:
    brand:
      $ref: './brands.yaml#/Brand'
    country:
      $ref: './countries.yaml#/Country'
    manufacturer:
      $ref: './manufacturers.yaml#/Manufacturer'
    type:
      $ref: './product_types.yaml#/ProductType'
    category:
      $ref: './categories.yaml#/Category'
    images:
      type: array
      items:
        $ref: './product_images.yaml#/ProductImage'
    attributes:
      type: array
      items:
        $ref: './product_attributes.yaml#/ProductAttributeValue'

Product:
  allOf:
    - $ref: '#/ProductReadonlyProperties'
    - $ref: '#/ProductFillableProperties'
    - $ref: '#/ProductSaleProperties'
    - $ref: '#/ProductIncludes'

CreateProductRequest:
  allOf:
    - $ref: '#/ProductFillableProperties'
    - $ref: '#/ProductRequiredProperties'

ReplaceProductRequest:
  allOf:
    - $ref: '#/ProductFillableProperties'
    - $ref: '#/ProductRequiredProperties'

PatchProductRequest:
  allOf:
    - $ref: '#/ProductFillableProperties'

ProductResponse:
  type: object
  properties:
    data:
      $ref: '#/Product'
    meta:
      type: object
  required:
    - data

SearchProductsFilter:
  type: object
  properties:
    id:
      type: array
      items:
        type: integer
      description: Идентификаторы товаров
      example: [1254, 5241]
    name:
      type: string
      description: Название или часть названия товара
      example: "телевизор"
    code:
      type: array
      items:
        type: string
      description: Коды товаров
      example: ['tv-1']
    external_id:
      type: array
      items:
        type: string
      description: Артикулы товаров
      example: ['521004', '264807']
    category_id:
      type: array
      items:
        type: integer
      description: Идентификаторы категорий
      example: [11]
    brand_id:
      type: array
      items:
        type: integer
      description: Идентификаторы брендов
      example: [125, 14]
    product_type_id:
      type: array
      items:
        type: integer
      description: Идентификаторы типов товаров
      example: [12]
    created_at_from:
      description: Дата создания от
      example: "2020-05-03T18:00:00.000000Z"
      type: string
      format: date-time
    created_at_to:
      description: Дата создания до
      example: "2020-05-29T11:12:29.000000Z"
      type: string
      format: date-time
    price_from:
      description: Цена от, руб.
      example: 1000
      type: integer
    price_to:
      description: Цена до, руб.
      example: 100000
      type: integer
    active:
      description: Имеет активные предложения
      example: true
      type: boolean
    qty_from:
      description: Количество от
      example: 1
      type: number
    qty_to:
      description: Количество до
      example: 10
      type: number

SearchProductsRequest:
  type: object
  properties:
    sort:
      $ref: '../../common_schemas.yaml#/RequestBodySort'
    include:
      $ref: '../../common_schemas.yaml#/RequestBodyInclude'
    pagination:
      $ref: '../../common_schemas.yaml#/RequestBodyPagination'
    filter:
      $ref: '#/SearchProductsFilter'

SearchProductsResponse:
  type: object
  properties:
    data:
      type: array
      items:
        $ref: '#/Product'
    meta:
      type: object
      properties:
        pagination:
          $ref: '../../common_schemas.yaml#/ResponseBodyPagination'
  required:
    - data
    - meta
