<?php


namespace App\Domain\Orders\Data\Orders;

use Ensi\OffersClient\Dto\Offer;
use Ensi\OmsClient\Dto\Delivery;
use Ensi\PimClient\Dto\Product;

class DeliveryData
{
    /** @var Offer[] */
    public array $offers = [];
    /** @var Product[] */
    public array $products = [];

    public function __construct(public Delivery $delivery)
    {
    }

    /**
     * @return ShipmentData[]|null
     */
    public function getShipments(): ?array
    {
        if (is_null($this->delivery->getShipments())) {
            return null;
        }
        $shipmentsData = [];
        foreach ($this->delivery->getShipments() as $shipment) {
            $shipmentData = new ShipmentData($shipment);
            $shipmentData->offers = $this->offers;
            $shipmentData->products = $this->products;
            $shipmentsData[] = $shipmentData;
        }

        return $shipmentsData;
    }
}
