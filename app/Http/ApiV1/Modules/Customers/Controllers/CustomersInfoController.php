<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\CustomersInfo\CreateCustomersInfoAction;
use App\Domain\Customers\Actions\CustomersInfo\DeleteCustomersInfoAction;
use App\Domain\Customers\Actions\CustomersInfo\ReplaceCustomersInfoAction;
use App\Http\ApiV1\Modules\Customers\Queries\CustomersCustomersInfoQuery;
use App\Http\ApiV1\Modules\Customers\Requests\CustomersInfo\CreateCustomersInfoRequest;
use App\Http\ApiV1\Modules\Customers\Requests\CustomersInfo\ReplaceCustomersInfoRequest;
use App\Http\ApiV1\Modules\Customers\Resources\CustomersInfoResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CustomersInfoController
{
    /**
     * @param CustomersCustomersInfoQuery $query
     * @return AnonymousResourceCollection
     */
    public function search(CustomersCustomersInfoQuery $query): AnonymousResourceCollection
    {
        return CustomersInfoResource::collectPage($query->get());
    }

    /**
     * @param CreateCustomersInfoRequest $request
     * @param CreateCustomersInfoAction $action
     * @return CustomersInfoResource
     */
    public function create(
        CreateCustomersInfoRequest $request,
        CreateCustomersInfoAction  $action
    ): CustomersInfoResource {
        return new CustomersInfoResource($action->execute($request->validated()));
    }

    /**
     * @param int $customerInfoId
     * @param CustomersCustomersInfoQuery $query
     * @return CustomersInfoResource
     * @throws \Ensi\CrmClient\ApiException
     */
    public function get(int $customerInfoId, CustomersCustomersInfoQuery $query): CustomersInfoResource
    {
        return new CustomersInfoResource($query->searchById($customerInfoId));
    }

    /**
     * @param int $customerInfoId
     * @param ReplaceCustomersInfoRequest $request
     * @param ReplaceCustomersInfoAction $action
     * @return CustomersInfoResource
     * @throws \Ensi\CrmClient\ApiException
     */
    public function patch(
        int                         $customerInfoId,
        ReplaceCustomersInfoRequest $request,
        ReplaceCustomersInfoAction  $action
    ): CustomersInfoResource {
        return new CustomersInfoResource($action->execute($customerInfoId, $request->validated()));
    }

    /**
     * @param int $customerInfoId
     * @param DeleteCustomersInfoAction $action
     * @return EmptyResource
     * @throws \Ensi\CrmClient\ApiException
     */
    public function delete(int $customerInfoId, DeleteCustomersInfoAction $action): EmptyResource
    {
        $action->execute($customerInfoId);

        return new EmptyResource();
    }
}
