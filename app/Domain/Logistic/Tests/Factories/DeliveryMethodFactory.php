<?php

namespace App\Domain\Logistic\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\LogisticClient\Dto\DeliveryMethod;
use Ensi\LogisticClient\Dto\DeliveryMethodEnum;
use Ensi\LogisticClient\Dto\GetDeliveryMethodsResponse;

class DeliveryMethodFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->randomElement(DeliveryMethodEnum::getAllowableEnumValues()),
            'name' => $this->faker->text(20),
        ];
    }

    public function make(array $extra = []): DeliveryMethod
    {
        return new DeliveryMethod($this->makeArray($extra));
    }

    public function makeResponseGet(array $extra = []): GetDeliveryMethodsResponse
    {
        return new GetDeliveryMethodsResponse([
            'data' => [$this->make($this->makeArray($extra))],
        ]);
    }
}
