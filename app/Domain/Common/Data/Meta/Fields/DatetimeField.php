<?php

namespace App\Domain\Common\Data\Meta\Fields;

use App\Http\ApiV1\OpenApiGenerated\Dto\FieldTypeEnum;

class DatetimeField extends AbstractField
{
    protected function type(): string
    {
        return FieldTypeEnum::DATETIME;
    }

    protected function init()
    {
        $this->sort()->filterRange();
    }
}
