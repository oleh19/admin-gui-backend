<?php


namespace App\Http\ApiV1\Modules\Orders\Controllers\Refunds;

use App\Domain\Orders\Actions\Refunds\CreateRefundReasonAction;
use App\Domain\Orders\Actions\Refunds\PatchRefundReasonAction;
use App\Http\ApiV1\Modules\Orders\Requests\Refunds\CreateRefundReasonRequest;
use App\Http\ApiV1\Modules\Orders\Requests\Refunds\PatchRefundReasonRequest;
use App\Http\ApiV1\Modules\Orders\Resources\Refunds\RefundReasonsResource;

class RefundReasonsController
{
    public function create(CreateRefundReasonRequest $request, CreateRefundReasonAction $action): RefundReasonsResource
    {
        return new RefundReasonsResource($action->execute($request->validated()));
    }

    public function patch(int $id, PatchRefundReasonRequest $request, PatchRefundReasonAction $action): RefundReasonsResource
    {
        return new RefundReasonsResource($action->execute($id, $request->validated()));
    }
}
