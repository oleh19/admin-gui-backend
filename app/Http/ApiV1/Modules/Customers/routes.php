<?php

use App\Http\ApiV1\Modules\Customers\Controllers\CustomerAttributesController;
use App\Http\ApiV1\Modules\Customers\Controllers\CustomersAddressController;
use App\Http\ApiV1\Modules\Customers\Controllers\CustomersBonusOperationsController;
use App\Http\ApiV1\Modules\Customers\Controllers\CustomersController;
use App\Http\ApiV1\Modules\Customers\Controllers\CustomersFavouritesController;
use App\Http\ApiV1\Modules\Customers\Controllers\CustomersInfoController;
use App\Http\ApiV1\Modules\Customers\Controllers\CustomersPreferencesController;
use App\Http\ApiV1\Modules\Customers\Controllers\CustomersProductSubscribesController;
use App\Http\ApiV1\Modules\Customers\Controllers\CustomerStatusesController;
use App\Http\ApiV1\Modules\Customers\Controllers\CustomersUserController;
use Illuminate\Support\Facades\Route;

Route::post('customers:search', [CustomersController::class, 'search']);
Route::post('customer-enum-values:search', [CustomersController::class, 'searchEnumValues'])->name('customers.searchCustomerEnumValues');
Route::post('customers', [CustomersController::class, 'create']);
Route::get('customers/{id}', [CustomersController::class, 'get']);
Route::put('customers/{id}', [CustomersController::class, 'replace']);
Route::delete('customers/{id}', [CustomersController::class, 'delete']);
Route::post('customers/{id}:upload-avatar', [CustomersController::class, 'uploadAvatar']);
Route::post('customers/{id}:delete-avatar', [CustomersController::class, 'deleteAvatar']);

Route::post('users:search', [CustomersUserController::class, 'search']);
Route::post('users:search-one', [CustomersUserController::class, 'searchOne']);
Route::post('users', [CustomersUserController::class, 'create']);
Route::get('users/{id}', [CustomersUserController::class, 'get']);
Route::patch('users/{id}', [CustomersUserController::class, 'patch']);
Route::delete('users/{id}', [CustomersUserController::class, 'delete']);

Route::post('addresses:search', [CustomersAddressController::class, 'search']);
Route::post('addresses', [CustomersAddressController::class, 'create']);
Route::get('addresses/{id}', [CustomersAddressController::class, 'get']);
Route::put('addresses/{id}', [CustomersAddressController::class, 'replace']);
Route::delete('addresses/{id}', [CustomersAddressController::class, 'delete']);
Route::post('addresses/{id}:set-as-default', [CustomersAddressController::class, 'setDefault']);

Route::post('statuses:search', [CustomerStatusesController::class, 'search']);
Route::post('statuses', [CustomerStatusesController::class, 'create']);
Route::get('statuses/{id}', [CustomerStatusesController::class, 'get']);
Route::patch('statuses/{id}', [CustomerStatusesController::class, 'patch']);
Route::delete('statuses/{id}', [CustomerStatusesController::class, 'delete']);

Route::post('attributes:search', [CustomerAttributesController::class, 'search']);
Route::post('attributes', [CustomerAttributesController::class, 'create']);
Route::get('attributes/{id}', [CustomerAttributesController::class, 'get']);
Route::patch('attributes/{id}', [CustomerAttributesController::class, 'patch']);
Route::delete('attributes/{id}', [CustomerAttributesController::class, 'delete']);

Route::post('customers-info:search', [CustomersInfoController::class, 'search']);
Route::post('customers-info', [CustomersInfoController::class, 'create']);
Route::get('customers-info/{id}', [CustomersInfoController::class, 'get']);
Route::patch('customers-info/{id}', [CustomersInfoController::class, 'patch']);
Route::delete('customers-info/{id}', [CustomersInfoController::class, 'delete']);

Route::post('favourites:search', [CustomersFavouritesController::class, 'search']);
Route::post('favourites', [CustomersFavouritesController::class, 'create']);
Route::post('favourites:delete-product', [CustomersFavouritesController::class, 'delete']);
Route::post('favourites:clear', [CustomersFavouritesController::class, 'clear']);

Route::post('product-subscribes:search', [CustomersProductSubscribesController::class, 'search']);
Route::post('product-subscribes', [CustomersProductSubscribesController::class, 'create']);
Route::post('product-subscribes:delete-product', [CustomersProductSubscribesController::class, 'delete']);
Route::post('product-subscribes:clear', [CustomersProductSubscribesController::class, 'clear']);

Route::post('bonus-operations:search', [CustomersBonusOperationsController::class, 'search']);
Route::post('bonus-operations', [CustomersBonusOperationsController::class, 'create']);
Route::get('bonus-operations/{id}', [CustomersBonusOperationsController::class, 'get']);
Route::patch('bonus-operations/{id}', [CustomersBonusOperationsController::class, 'patch']);
Route::delete('bonus-operations/{id}', [CustomersBonusOperationsController::class, 'delete']);

Route::post('preferences:search', [CustomersPreferencesController::class, 'search']);
