<?php


namespace App\Domain\Orders\Actions\Refunds;

use Ensi\OmsClient\Api\RefundsApi;
use Ensi\OmsClient\Dto\RefundForDeleteFiles;

class DeleteRefundFilesAction
{
    public function __construct(protected RefundsApi $refundsApi)
    {
    }

    public function execute(int $id, array $fileIds): void
    {
        $this->refundsApi->deleteRefundFiles($id, (new RefundForDeleteFiles())->setFileIds($fileIds));
    }
}
