<?php

namespace App\Http\ApiV1\Modules\Units\Requests\Sellers;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\BuClient\Dto\SellerStatusEnum;
use Illuminate\Validation\Rule;

class PatchSellerRequest extends BaseFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'legal_name' => ['sometimes', 'required', 'string'],
            'external_id' => ['sometimes', 'required', 'string'],
            'legal_address' => ['nullable', 'string'],
            'fact_address'  => ['nullable', 'string'],
            'inn' => ['nullable', 'string'],
            'kpp' => ['nullable', 'string'],
            'payment_account' => ['nullable', 'string'],
            'bank' => ['nullable', 'string'],
            'bank_address' => ['nullable', 'string'],
            'bank_bik' => ['nullable', 'string'],
            'correspondent_account' => ['nullable', 'string'],
            'status' => ['nullable', 'integer', Rule::in(SellerStatusEnum::getAllowableEnumValues())],
            'manager_id' => ['nullable', 'integer'],
            'rating_id' => ['nullable', 'integer'],
            'storage_address' => ['nullable', 'string'],
            'site' => ['nullable', 'string'],
            'can_integration' => ['nullable', 'boolean'],
            'sale_info' => ['nullable', 'string'],
            'city'  => ['nullable', 'string'],
        ];
    }
}
