<?php

use App\Http\ApiV1\Modules\Marketing\Controllers\DiscountBrandsController;
use App\Http\ApiV1\Modules\Marketing\Controllers\DiscountCategoriesController;
use App\Http\ApiV1\Modules\Marketing\Controllers\DiscountConditionsController;
use App\Http\ApiV1\Modules\Marketing\Controllers\DiscountOffersController;
use App\Http\ApiV1\Modules\Marketing\Controllers\DiscountsController;
use App\Http\ApiV1\Modules\Marketing\Controllers\DiscountSegmentsController;
use App\Http\ApiV1\Modules\Marketing\Controllers\EnumsController;
use App\Http\ApiV1\Modules\Marketing\Controllers\PromoCodesController;
use Illuminate\Support\Facades\Route;

Route::post('promo-codes:search', [PromoCodesController::class, 'search']);
Route::get('promo-codes/{id}', [PromoCodesController::class, 'findById']);
Route::post('promo-codes', [PromoCodesController::class, 'create']);
Route::put('promo-codes/{id}', [PromoCodesController::class, 'replace']);
Route::patch('promo-codes/{id}', [PromoCodesController::class, 'patch']);
Route::delete('promo-codes/{id}', [PromoCodesController::class, 'delete']);

Route::post('discounts:search', [DiscountsController::class, 'search']);
Route::get('discounts/{id}', [DiscountsController::class, 'findById']);
Route::post('discounts', [DiscountsController::class, 'create']);
Route::put('discounts/{id}', [DiscountsController::class, 'replace']);
Route::patch('discounts/{id}', [DiscountsController::class, 'patch']);
Route::delete('discounts/{id}', [DiscountsController::class, 'delete']);
Route::post('discounts:mass-status-update', [DiscountsController::class, 'massStatusUpdate']);

Route::post('discounts/discount-offers:search', [DiscountOffersController::class, 'search']);
Route::get('discounts/discount-offers/{id}', [DiscountOffersController::class, 'findById']);
Route::post('discounts/discount-offers', [DiscountOffersController::class, 'create']);
Route::patch('discounts/discount-offers/{id}', [DiscountOffersController::class, 'patch']);
Route::delete('discounts/discount-offers/{id}', [DiscountOffersController::class, 'delete']);

Route::post('discounts/discount-brands:search', [DiscountBrandsController::class, 'search']);
Route::get('discounts/discount-brands/{id}', [DiscountBrandsController::class, 'findById']);
Route::post('discounts/discount-brands', [DiscountBrandsController::class, 'create']);
Route::patch('discounts/discount-brands/{id}', [DiscountBrandsController::class, 'patch']);
Route::delete('discounts/discount-brands/{id}', [DiscountBrandsController::class, 'delete']);

Route::post('discounts/discount-categories:search', [DiscountCategoriesController::class, 'search']);
Route::get('discounts/discount-categories/{id}', [DiscountCategoriesController::class, 'findById']);
Route::post('discounts/discount-categories', [DiscountCategoriesController::class, 'create']);
Route::patch('discounts/discount-categories/{id}', [DiscountCategoriesController::class, 'patch']);
Route::delete('discounts/discount-categories/{id}', [DiscountCategoriesController::class, 'delete']);

Route::post('discounts/discount-segments:search', [DiscountSegmentsController::class, 'search']);
Route::get('discounts/discount-segments/{id}', [DiscountSegmentsController::class, 'findById']);
Route::post('discounts/discount-segments', [DiscountSegmentsController::class, 'create']);
Route::patch('discounts/discount-segments/{id}', [DiscountSegmentsController::class, 'patch']);
Route::delete('discounts/discount-segments/{id}', [DiscountSegmentsController::class, 'delete']);

Route::post('discounts/discount-conditions:search', [DiscountConditionsController::class, 'search']);
Route::get('discounts/discount-conditions/{id}', [DiscountConditionsController::class, 'findById']);
Route::post('discounts/discount-conditions', [DiscountConditionsController::class, 'create']);
Route::patch('discounts/discount-conditions/{id}', [DiscountConditionsController::class, 'patch']);
Route::delete('discounts/discount-conditions/{id}', [DiscountConditionsController::class, 'delete']);

Route::get('discount-statuses', [EnumsController::class, 'discountStatuses']);
Route::get('discount-types', [EnumsController::class, 'discountTypes']);
Route::get('discount-condition-types', [EnumsController::class, 'discountConditionTypes']);
Route::get('discount-condition-type-props', [EnumsController::class, 'discountConditionTypeProps']);
Route::get('promo-code-statuses', [EnumsController::class, 'promoCodeStatuses']);
Route::get('promo-code-types', [EnumsController::class, 'promoCodeTypes']);
