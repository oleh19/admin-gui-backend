<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\Geos;

use App\Domain\Logistic\Actions\Geos\CreateFederalDistrictAction;
use App\Domain\Logistic\Actions\Geos\DeleteFederalDistrictAction;
use App\Domain\Logistic\Actions\Geos\PatchFederalDistrictAction;
use App\Domain\Logistic\Actions\Geos\ReplaceFederalDistrictAction;
use App\Http\ApiV1\Modules\Logistic\Queries\Geos\FederalDistrictsQuery;
use App\Http\ApiV1\Modules\Logistic\Requests\Geos\CreateOrReplaceFederalDistrictRequest;
use App\Http\ApiV1\Modules\Logistic\Requests\Geos\PatchFederalDistrictRequest;
use App\Http\ApiV1\Modules\Logistic\Resources\Geos\FederalDistrictsResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Ensi\LogisticClient\ApiException;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class FederalDistrictsController
 * @package App\Http\ApiV1\Modules\Logistic\Controllers\Geos
 */
class FederalDistrictsController
{
    /**
     * @param  CreateOrReplaceFederalDistrictRequest  $request
     * @param  CreateFederalDistrictAction  $action
     * @return FederalDistrictsResource
     * @throws ApiException
     */
    public function create(CreateOrReplaceFederalDistrictRequest $request, CreateFederalDistrictAction $action): FederalDistrictsResource
    {
        return new FederalDistrictsResource($action->execute($request->validated()));
    }

    /**
     * @param  int  $federalDistrictId
     * @param  CreateOrReplaceFederalDistrictRequest  $request
     * @param  ReplaceFederalDistrictAction  $action
     * @return FederalDistrictsResource
     * @throws ApiException
     */
    public function replace(
        int $federalDistrictId,
        CreateOrReplaceFederalDistrictRequest $request,
        ReplaceFederalDistrictAction $action
    ): FederalDistrictsResource {
        return new FederalDistrictsResource($action->execute($federalDistrictId, $request->validated()));
    }

    /**
     * @param  int  $federalDistrictId
     * @param  PatchFederalDistrictRequest  $request
     * @param  PatchFederalDistrictAction  $action
     * @return FederalDistrictsResource
     * @throws ApiException
     */
    public function patch(int $federalDistrictId, PatchFederalDistrictRequest $request, PatchFederalDistrictAction $action): FederalDistrictsResource
    {
        return new FederalDistrictsResource($action->execute($federalDistrictId, $request->validated()));
    }

    /**
     * @param  int  $federalDistrictId
     * @param  DeleteFederalDistrictAction  $action
     * @return EmptyResource
     * @throws ApiException
     */
    public function delete(int $federalDistrictId, DeleteFederalDistrictAction $action): EmptyResource
    {
        $action->execute($federalDistrictId);

        return new EmptyResource();
    }

    /**
     * @param  int  $federalDistrictId
     * @param  FederalDistrictsQuery  $query
     * @return FederalDistrictsResource
     */
    public function get(
        int $federalDistrictId,
        FederalDistrictsQuery $query
    ): FederalDistrictsResource {
        return new FederalDistrictsResource($query->find($federalDistrictId));
    }

    /**
     * @param  FederalDistrictsQuery  $query
     * @return AnonymousResourceCollection
     */
    public function search(FederalDistrictsQuery $query): AnonymousResourceCollection
    {
        return FederalDistrictsResource::collectPage(
            $query->get()
        );
    }

    /**
     * @param  FederalDistrictsQuery  $query
     * @return FederalDistrictsResource
     */
    public function searchOne(FederalDistrictsQuery $query): FederalDistrictsResource
    {
        return new FederalDistrictsResource($query->first());
    }
}
