<?php


namespace App\Http\ApiV1\Modules\Orders\Controllers\Refunds;

use App\Http\ApiV1\Modules\Orders\Resources\Refunds\Enums\RefundStatusesResource;
use App\Http\ApiV1\Modules\Orders\Resources\Refunds\RefundReasonsResource;
use Ensi\OmsClient\Api\EnumsApi;

class EnumsController
{
    public function refundStatuses(EnumsApi $enumsApi)
    {
        return RefundStatusesResource::collection($enumsApi->getRefundStatuses()->getData());
    }

    public function refundReasons(EnumsApi $enumsApi)
    {
        return RefundReasonsResource::collection($enumsApi->getRefundReasons()->getData());
    }
}
