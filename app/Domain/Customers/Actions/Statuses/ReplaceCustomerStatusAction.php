<?php

namespace App\Domain\Customers\Actions\Statuses;

use Ensi\CustomersClient\Dto\CustomerStatuses;
use Ensi\CustomersClient\Dto\CustomerStatusForCreateOrReplace;

class ReplaceCustomerStatusAction extends CustomerStatusAction
{
    /**
     * @param int $statusId
     * @param array $fields
     * @return CustomerStatuses
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function execute(int $statusId, array $fields): CustomerStatuses
    {
        return $this->statusApi->replaceCustomerStatuses(
            $statusId,
            new CustomerStatusForCreateOrReplace($fields)
        )->getData();
    }
}
