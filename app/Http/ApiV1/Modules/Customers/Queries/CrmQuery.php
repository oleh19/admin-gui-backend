<?php

namespace App\Http\ApiV1\Modules\Customers\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\CrmClient\Dto\RequestBodyPagination;
use Illuminate\Http\Request;

/**
 * Class CrmQuery
 * @package App\Http\ApiV1\Modules\Customers\Queries
 */
abstract class CrmQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;

    /**
     * CrmQuery constructor.
     * @param Request $request
     * @param string $requestGetClass
     */
    public function __construct(Request $request, protected string $requestGetClass)
    {
        parent::__construct($request);
    }

    /**
     * @return string
     */
    protected function requestGetClass(): string
    {
        return $this->requestGetClass;
    }

    /**
     * @return string
     */
    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }
}
