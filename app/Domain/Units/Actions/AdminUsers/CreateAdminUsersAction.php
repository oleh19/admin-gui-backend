<?php

namespace App\Domain\Units\Actions\AdminUsers;

use Ensi\AdminAuthClient\Api\UsersApi as AdminUsersApi;
use Ensi\AdminAuthClient\Dto\CreateUserRequest as CreateAdminUserRequest;
use Ensi\AdminAuthClient\Dto\User as AdminUser;

class CreateAdminUsersAction
{
    public function __construct(
        protected AdminUsersApi $adminUsersApi,
    ) {
    }

    public function execute(array $data): AdminUser
    {
        $request = new CreateAdminUserRequest($data);
        $response =  $this->adminUsersApi->createUser($request);

        return $response->getData();
    }
}
