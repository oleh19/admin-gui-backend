<?php

namespace App\Domain\Customers\Actions\ProductSubscribes;

use Ensi\CrmClient\Dto\DeleteProductSubscribesRequest;

class DeleteCustomerProductSubscribesAction extends ProductSubscribesAction
{
    public function execute(array $fields): void
    {
        $this->productSubscribesApi->deleteProductSubscribe(
            new DeleteProductSubscribesRequest($fields)
        );
    }
}
