<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Products;

use App\Http\ApiV1\OpenApiGenerated\Dto\CatalogProductImageTypeEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class PatchProductImageRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'type' => ['sometimes', Rule::in(CatalogProductImageTypeEnum::getAllowableEnumValues())],
        ];
    }
}
