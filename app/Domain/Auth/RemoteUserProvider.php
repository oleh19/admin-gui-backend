<?php

namespace App\Domain\Auth;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Api\UsersApi;
use Ensi\AdminAuthClient\ApiException;
use Ensi\AdminAuthClient\Dto\User as UserDTO;
use Ensi\BuClient\Api\OperatorsApi;
use Illuminate\Log\LogManager;

class RemoteUserProvider
{
    public function __construct(
        private UsersApi $usersApi,
        private OperatorsApi $operatorsApi,
        private LogManager $logger
    ) {
    }

    /**
     * Запрашивает данные пользователя по идентификатору.
     *
     * @param int $identifier
     * @return User|null
     */
    public function retrieveById($identifier): ?User
    {
        return $this->retrieveUser($identifier);
    }

    /**
     * Загружает данные текущего пользователя.
     *
     * @param string $token
     * @return User|null
     */
    public function retrieveCurrent(string $token): ?User
    {
        try {
            $this->usersApi->getConfig()->setAccessToken($token);
            $response = $this->usersApi->getCurrentUser();

            $user = $this->constructUser($response->getData());
            $user->setRememberToken($token);

            return $user;
        } catch (ApiException $e) {
            $this->logger->error($e->getMessage());

            return null;
        }
    }

    /**
     * Загружает данные пользователя.
     *
     * @param int $identifier
     * @return User|null
     */
    private function retrieveUser(int $identifier): ?User
    {
        try {
            $response = $this->usersApi->getUser($identifier);

            return $this->constructUser($response->getData());
        } catch (ApiException $e) {
            $this->logger->error($e->getMessage());

            return null;
        }
    }

    private function constructUser(UserDTO $userDTO): User
    {
        $fields = [
            'id' => $userDTO->getId(),
            'login' => $userDTO->getLogin(),
            'active' => $userDTO->getActive(),
            'roles' => collect($userDTO->getRoles())->pluck('id')->values()->all(),
            'name' =>  $userDTO->getFullName(),
            'phone' => $userDTO->getPhone(),
        ];

        return new User($fields);
    }
}
