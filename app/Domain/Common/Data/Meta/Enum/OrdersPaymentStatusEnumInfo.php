<?php

namespace App\Domain\Common\Data\Meta\Enum;

use Ensi\OmsClient\Api\EnumsApi;
use Ensi\OmsClient\Dto\PaymentStatusesResponse;
use GuzzleHttp\Promise\PromiseInterface;

class OrdersPaymentStatusEnumInfo extends AbstractEnumInfo implements AsyncEnumInfo
{
    public function __construct(protected EnumsApi $enumsApi)
    {
    }

    public function requestAsync(): PromiseInterface
    {
        return $this->enumsApi->getPaymentStatusesAsync();
    }

    /**
     * @param PaymentStatusesResponse $response
     */
    public function processResponse($response)
    {
        foreach ($response->getData() as $status) {
            $this->addValue($status->getId(), $status->getName());
        }
    }
}
