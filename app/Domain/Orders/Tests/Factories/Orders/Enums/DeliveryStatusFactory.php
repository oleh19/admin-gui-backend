<?php

namespace App\Domain\Orders\Tests\Factories\Orders\Enums;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\OmsClient\Dto\DeliveryStatus;
use Ensi\OmsClient\Dto\DeliveryStatusEnum;
use Ensi\OmsClient\Dto\DeliveryStatusesResponse;

class DeliveryStatusFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->randomElement(DeliveryStatusEnum::getAllowableEnumValues()),
            'name' => $this->faker->text(20),
        ];
    }

    public function make(array $extra = []): DeliveryStatus
    {
        return new DeliveryStatus($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extra = []): DeliveryStatusesResponse
    {
        return new DeliveryStatusesResponse([
            'data' => [$this->make($this->makeArray($extra))],
        ]);
    }
}
