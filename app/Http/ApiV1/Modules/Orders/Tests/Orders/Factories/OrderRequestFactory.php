<?php

namespace App\Http\ApiV1\Modules\Orders\Tests\Orders\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\LogisticClient\Dto\DeliveryMethodEnum;
use Ensi\LogisticClient\Dto\DeliveryServiceEnum;
use Ensi\OmsClient\Dto\OrderStatusEnum;

class OrderRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $deliveryMethod = $this->faker->randomElement(DeliveryMethodEnum::getAllowableEnumValues());
        $isDelivery = $deliveryMethod == DeliveryMethodEnum::DELIVERY;

        $deliveryPrice = $this->faker->numberBetween(0, 200);

        return [
            'id' => $this->faker->randomNumber(),
            'status' => $this->faker->randomElement(OrderStatusEnum::getAllowableEnumValues()),
            'is_problem' => $this->faker->boolean(),
            'problem_comment' => $this->faker->text(50),
            'client_comment' => $this->faker->optional()->text(50),
            'receiver_name' => $this->faker->name(),
            'receiver_phone' => $this->faker->numerify('+7##########'),
            'receiver_email' => $this->faker->email(),
            'responsible_id' => $this->faker->randomNumber(),
            'delivery_service' => $this->faker->randomElement(DeliveryServiceEnum::getAllowableEnumValues()),
            'delivery_method' => $deliveryMethod,
            'delivery_tariff_id' => $this->faker->randomNumber(),
            'delivery_point_id' => $isDelivery ? null : $this->faker->randomNumber(),
            'delivery_price' => $deliveryPrice,
            'delivery_cost' => $this->faker->numberBetween($deliveryPrice, $deliveryPrice + 500),
            'delivery_comment' => $isDelivery ? $this->faker->optional()->text(50) : null,
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function makePathOrder(array $extra = []): array
    {
        return $this->only([
            'responsible_id',
            'status',
            'client_comment',
            'receiver_name',
            'receiver_phone',
            'receiver_email',
            'is_problem',
            'problem_comment',
        ])->make($extra);
    }

    public function makeChangeDelivery(): array
    {
        return $this->only([
            'delivery_service',
            'delivery_method',
            'delivery_tariff_id',
            'delivery_point_id',
            'delivery_price',
            'delivery_cost',
            'delivery_comment',
        ])->make();
    }
}
