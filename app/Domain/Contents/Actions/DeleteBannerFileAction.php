<?php

namespace App\Domain\Contents\Actions;

use Ensi\CmsClient\Api\BannersApi;
use Ensi\CmsClient\Dto\DeleteBannerFileRequest;

class DeleteBannerFileAction
{
    public function __construct(
        private BannersApi $bannersApi
    ) {
    }

    public function execute(int $id, string $type): void
    {
        $deleteBannerFileRequest = new DeleteBannerFileRequest();
        $deleteBannerFileRequest->setType($type);

        $this->bannersApi->deleteBannerFiles($id, $deleteBannerFileRequest);
    }
}
