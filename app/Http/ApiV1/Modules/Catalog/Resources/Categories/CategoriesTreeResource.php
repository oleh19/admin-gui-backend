<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Categories;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\CategoriesTreeItem;

/**
 * @mixin CategoriesTreeItem
 */
class CategoriesTreeResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'code' => $this->getCode(),
            'name' => $this->getName(),
            'children' => static::collection($this->whenNotNull($this->getChildren())),
        ];
    }
}
