<?php


namespace App\Domain\Orders\ProtectedFiles;

use App\Domain\Common\ProtectedFiles\ProtectedFileLoader;
use Ensi\OmsClient\Api\RefundsApi;
use Ensi\OmsClient\Dto\Refund;
use Ensi\OmsClient\Dto\SearchRefundsRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class RefundFileLoader extends ProtectedFileLoader
{
    public function __construct(protected RefundsApi $refundsApi)
    {
    }

    public function getRootPath(): ?string
    {
        $refund = $this->loadRefund();

        // Сейчас любой пользователь может просматривать инфо по файлу в админке, далее нужна будет проверка по роли/доступу к чату в соответствии с ФЗ

        foreach ($refund->getFiles() as $file) {
            if ($file->getId() == $this->file->file) {
                return $file->getFile()->getRootPath();
            }
        }

        return null;
    }

    protected function loadRefund(): Refund
    {
        $request = new SearchRefundsRequest();
        $request->setFilter((object)['id' => $this->file->entity_id]);
        $request->setInclude(['files']);
        $refunds = $this->refundsApi->searchRefunds($request)->getData();
        if ($refunds) {
            return current($refunds);
        }

        throw new ModelNotFoundException();
    }
}
