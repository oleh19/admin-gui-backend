<?php

use App\Http\ApiV1\Modules\Units\Controllers\AdminUserRolesController;
use App\Http\ApiV1\Modules\Units\Controllers\AdminUsersController;
use App\Http\ApiV1\Modules\Units\Controllers\EnumsController;
use App\Http\ApiV1\Modules\Units\Controllers\SellersController;
use App\Http\ApiV1\Modules\Units\Controllers\SellerUserRolesController;
use App\Http\ApiV1\Modules\Units\Controllers\SellerUsersController;
use App\Http\ApiV1\Modules\Units\Controllers\StoreContactsController;
use App\Http\ApiV1\Modules\Units\Controllers\StorePickupTimesController;
use App\Http\ApiV1\Modules\Units\Controllers\StoresController;
use App\Http\ApiV1\Modules\Units\Controllers\StoreWorkingsController;
use Illuminate\Support\Facades\Route;

// Продавцы
Route::post('sellers:search', [SellersController::class, 'search'])->name('searchSellers');
Route::post('sellers', [SellersController::class, 'create'])->name('createSeller');
Route::prefix('sellers/{id}')->group(function () {
    Route::get('', [SellersController::class, 'get'])->name('getSeller');
    Route::patch('', [SellersController::class, 'patch'])->name('patchSeller');
    Route::put('', [SellersController::class, 'replace'])->name('replaceSeller');
});

Route::get('sellers/statuses', [EnumsController::class, 'sellerStatuses']);

// Склады
Route::post('stores:search', [StoresController::class, 'search'])->name('searchStores');
Route::post('stores', [StoresController::class, 'create'])->name('createStore');
Route::prefix('stores/{id}')->group(function () {
    Route::get('', [StoresController::class, 'get'])->name('getStore');
    Route::patch('', [StoresController::class, 'patch'])->name('patchStore');
    Route::put('', [StoresController::class, 'replace'])->name('replaceStore');
});

// Время работы складов
Route::post('stores-workings', [StoreWorkingsController::class, 'create'])->name('createStoreWorking');
Route::prefix('stores-workings/{id}')->group(function () {
    Route::patch('', [StoreWorkingsController::class, 'patch'])->name('patchStoreWorking');
    Route::put('', [StoreWorkingsController::class, 'replace'])->name('replaceStoreWorking');
});

// Время отгрузки складов
Route::post('stores-pickup-times', [StorePickupTimesController::class, 'create'])->name('createStorePickupTime');
Route::prefix('stores-pickup-times/{id}')->group(function () {
    Route::patch('', [StorePickupTimesController::class, 'patch'])->name('patchStorePickupTime');
    Route::put('', [StorePickupTimesController::class, 'replace'])->name('replaceStorePickupTime');
});

// Контактные лица складов
Route::post('stores-contacts', [StoreContactsController::class, 'create'])->name('createStoreContact');
Route::prefix('stores-contacts/{id}')->group(function () {
    Route::patch('', [StoreContactsController::class, 'patch'])->name('patchStoreContact');
    Route::put('', [StoreContactsController::class, 'replace'])->name('replaceStoreContact');
    Route::delete('', [StoreContactsController::class, 'delete'])->name('deleteStoreContact');
});

//Пользователи продавцов (Кабинет продавца)
Route::post('seller-users:search', [SellerUsersController::class, 'search'])->name('searchSellerUsers');
Route::post('seller-users', [SellerUsersController::class, 'create'])->name('createSellerUser');
Route::get('seller-users/{id}', [SellerUsersController::class, 'get'])->where(['id' => '[0-9]+'])->name('getSellerUser');
Route::patch('seller-users/{id}', [SellerUsersController::class, 'patch'])->where(['id' => '[0-9]+'])->name('patchSellerUser');
Route::post('seller-users/{id}:add-roles', [SellerUsersController::class, 'addRoles'])->where(['id' => '[0-9]+'])->name('addRolesToSellerUser');
Route::post('seller-users/{id}:delete-role', [SellerUsersController::class, 'deleteRole'])->where(['id' => '[0-9]+'])->name('deleteRoleFromSellerUser');

Route::post('seller-user-roles:search', [SellerUserRolesController::class, 'search'])->name('searchRole');

// Пользователи (Админы)
Route::post('admin-users:search', [AdminUsersController::class, 'search']);
Route::post('admin-user-enum-values:search', [AdminUsersController::class, 'searchEnumValues'])->name('units.searchAdminUserEnumValues');
Route::post('admin-users', [AdminUsersController::class, 'create']);
Route::get('admin-users/{id}', [AdminUsersController::class, 'get'])->where(['id' => '[0-9]+']);
Route::patch('admin-users/{id}', [AdminUsersController::class, 'patch'])->where(['id' => '[0-9]+']);
Route::post('admin-users/{id}:add-roles', [AdminUsersController::class, 'addRoles'])->where(['id' => '[0-9]+']);
Route::post('admin-users/{id}:delete-role', [AdminUsersController::class, 'deleteRole'])->where(['id' => '[0-9]+']);
Route::post('admin-users/{id}:refresh-password-token', [AdminUsersController::class, 'refreshPasswordToken'])->where(['id' => '[0-9]+']);
Route::post('admin-users/mass/change-active', [AdminUsersController::class, 'massChangeActive']);
Route::post('admin-users/set-password', [AdminUsersController::class, 'setPassword'])->withoutMiddleware('auth');

Route::post('admin-user-roles:search', [AdminUserRolesController::class, 'search']);
