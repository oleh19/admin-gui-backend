<?php

namespace App\Domain\Catalog\Actions\Products;

use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\EditProductAttributeValue;
use Ensi\PimClient\Dto\Product;
use Ensi\PimClient\Dto\ReplaceProductAttributesRequest;

class ReplaceAttributesValuesAction
{
    public function __construct(private ProductsApi $api)
    {
    }

    public function execute(int $productId, array $attributes): Product
    {
        $attributes['values'] = collect($attributes['values'] ?? [])
            ->mapInto(EditProductAttributeValue::class)
            ->all();

        $request = new ReplaceProductAttributesRequest($attributes);

        return $this->api->replaceProductAttributes($productId, $request)->getData();
    }
}
