<?php

namespace App\Domain\Units\Actions\Sellers;

use App\Domain\Units\Data\SellerData;
use Ensi\BuClient\Api\SellersApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\SellerForReplace;

class ReplaceSellerAction
{
    public function __construct(
        private SellersApi $sellersApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): SellerData
    {
        $seller = new SellerForReplace($fields);

        return new SellerData($this->sellersApi->replaceSeller($id, $seller)->getData());
    }
}
