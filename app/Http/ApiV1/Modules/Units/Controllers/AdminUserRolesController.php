<?php

namespace App\Http\ApiV1\Modules\Units\Controllers;

use App\Http\ApiV1\Modules\Units\Queries\AdminUserRolesQuery;
use App\Http\ApiV1\Modules\Units\Resources\AdminUserRolesResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class AdminUserRolesController
{
    public function search(AdminUserRolesQuery $query): AnonymousResourceCollection
    {
        return AdminUserRolesResource::collectPage($query->get());
    }
}
