<?php


namespace App\Http\ApiV1\Modules\Orders\Controllers\BasketsCommon;

use App\Domain\Orders\Actions\BasketsCommon\PatchSeveralSettingsAction;
use App\Http\ApiV1\Modules\Orders\Requests\BasketsCommon\PatchSeveralSettingsRequest;
use App\Http\ApiV1\Modules\Orders\Resources\BasketsCommon\SettingResource;
use Ensi\BasketsClient\Api\CommonApi;

class SettingsController
{
    public function search(CommonApi $commonApi)
    {
        return SettingResource::collection($commonApi->searchSettings()->getData());
    }

    public function patchSeveral(PatchSeveralSettingsAction $action, PatchSeveralSettingsRequest $request)
    {
        return SettingResource::collection($action->execute($request->validated()));
    }
}
