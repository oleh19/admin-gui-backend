<?php


namespace App\Http\ApiV1\Modules\Marketing\Controllers;

use App\Domain\Marketing\Actions\Discounts\CreateDiscountConditionAction;
use App\Domain\Marketing\Actions\Discounts\DeleteDiscountConditionAction;
use App\Domain\Marketing\Actions\Discounts\PatchDiscountConditionAction;
use App\Http\ApiV1\Modules\Marketing\Queries\DiscountConditionsQuery;
use App\Http\ApiV1\Modules\Marketing\Requests\CreateDiscountConditionRequest;
use App\Http\ApiV1\Modules\Marketing\Requests\PatchDiscountConditionRequest;
use App\Http\ApiV1\Modules\Marketing\Resources\DiscountConditionsResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class DiscountConditionsController
{
    public function findById(int $discountConditionId, DiscountConditionsQuery $query)
    {
        return new DiscountConditionsResource($query->find($discountConditionId));
    }

    public function search(DiscountConditionsQuery $query)
    {
        return DiscountConditionsResource::collectPage($query->get());
    }

    public function create(CreateDiscountConditionRequest $request, CreateDiscountConditionAction $action): DiscountConditionsResource
    {
        return new DiscountConditionsResource($action->execute($request->validated()));
    }

    public function patch(
        int $discountConditionId,
        PatchDiscountConditionRequest $request,
        PatchDiscountConditionAction $action
    ): DiscountConditionsResource {
        return new DiscountConditionsResource($action->execute($discountConditionId, $request->validated()));
    }

    public function delete(int $discountConditionId, DeleteDiscountConditionAction $action): EmptyResource
    {
        $action->execute($discountConditionId);

        return new EmptyResource();
    }
}
