<?php

namespace App\Http\ApiV1\Modules\Marketing\Requests;

use App\Http\ApiV1\OpenApiGenerated\Dto\MarketingDiscountStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Dto\MarketingDiscountValueTypeEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class PatchDiscountRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'seller_id' => ['nullable', 'integer'],
            'name' => ['string'],
            'value_type' => ['integer', Rule::in(MarketingDiscountValueTypeEnum::getAllowableEnumValues())],
            'value' => ['integer'],
            'status' => ['integer', Rule::in(MarketingDiscountStatusEnum::getAllowableEnumValues())],
            'start_date' => ['nullable', 'date'],
            'end_date' => ['nullable', 'date', 'after_or_equal:start_date'],
            'promo_code_only' => ['boolean'],
        ];
    }
}
