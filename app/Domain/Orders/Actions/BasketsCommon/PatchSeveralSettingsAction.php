<?php


namespace App\Domain\Orders\Actions\BasketsCommon;

use Ensi\BasketsClient\Api\CommonApi;
use Ensi\BasketsClient\Dto\PatchSeveralSettingsRequest;

class PatchSeveralSettingsAction
{
    public function __construct(protected CommonApi $commonApi)
    {
    }

    public function execute(array $fields): array
    {
        return $this->commonApi->patchSettings(new PatchSeveralSettingsRequest($fields))->getData();
    }
}
