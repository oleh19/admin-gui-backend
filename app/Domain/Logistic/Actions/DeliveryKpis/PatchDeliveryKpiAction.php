<?php


namespace App\Domain\Logistic\Actions\DeliveryKpis;

use Ensi\LogisticClient\Api\KpiApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\DeliveryKpi;
use Ensi\LogisticClient\Dto\PatchDeliveryKpiRequest;

/**
 * Class PatchDeliveryKpiAction
 * @package App\Domain\Logistic\Actions\DeliveryKpis
 */
class PatchDeliveryKpiAction
{
    /**
     * PatchDeliveryKpiAction constructor.
     * @param KpiApi $kpiApi
     */
    public function __construct(protected KpiApi $kpiApi)
    {
    }

    /**
     * @param array $fields
     * @return DeliveryKpi
     * @throws ApiException
     */
    public function execute(array $fields): DeliveryKpi
    {
        $request = new PatchDeliveryKpiRequest($fields);

        return $this->kpiApi->patchDeliveryKpi($request)->getData();
    }
}
