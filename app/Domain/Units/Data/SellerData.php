<?php

namespace App\Domain\Units\Data;

use Ensi\AdminAuthClient\Dto\User;
use Ensi\BuClient\Dto\Seller;

/**
 * Class SellerData
 * @package App\Domain\Units\Data
 */
class SellerData
{
    public ?SellerUserData $owner = null;
    public ?User $manager = null;

    /**
     * SellerData constructor.
     * @param Seller $seller
     */
    public function __construct(public Seller $seller)
    {
    }
}
