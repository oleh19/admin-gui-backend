<?php

namespace App\Http\ApiV1\Modules\Units\Resources;

use App\Domain\Units\Data\SellerUserData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/**
 * Class SellerUserResource
 * @package App\Http\ApiV1\Modules\Units\Resources
 * @mixin SellerUserData
 */
class SellerUsersResource extends BaseJsonResource
{
    /**
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        $roles = $this->user->getRoles();

        return [
            "id" => $this->operator->getId(),
            "full_name" => $this->user->getFullName(),
            "short_name" => $this->user->getShortName(),
            "created_at" => $this->dateTimeToIso($this->user->getCreatedAt()),
            "updated_at" => $this->dateTimeToIso($this->user->getUpdatedAt()),
            "seller_id" => $this->operator->getSellerId(),
            "active" => $this->user->getActive(),
            "login" => $this->user->getLogin(),
            "last_name" => $this->user->getLastName(),
            "first_name" => $this->user->getFirstName(),
            "middle_name" => $this->user->getMiddleName(),
            "email" => $this->user->getEmail(),
            "phone" => $this->user->getPhone(),
            "timezone" => $this->user->getTimezone(),
            "roles" => AdminUserRolesResource::collection($this->when(!is_null($roles), $roles)),
        ];
    }
}
