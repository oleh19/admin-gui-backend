<?php

namespace App\Domain\Units\Actions\AdminUsers;

use Ensi\AdminAuthClient\Api\UsersApi as AdminUsersApi;
use Ensi\AdminAuthClient\Dto\DeleteRoleFromUserRequest as DeleteRoleFromAdminUserRequest;

class DeleteRoleFromAdminUserAction
{
    public function __construct(
        protected AdminUsersApi $adminUsersApi,
    ) {
    }

    public function execute(int $id, array $data): void
    {
        $request = new DeleteRoleFromAdminUserRequest($data);
        $this->adminUsersApi->deleteRoleFromUser($id, $request);
    }
}
