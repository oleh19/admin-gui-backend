<?php

namespace App\Domain\Catalog\Actions\Products;

use Ensi\PimClient\Api\ImagesApi;
use Ensi\PimClient\Dto\ProductImage;
use Illuminate\Http\UploadedFile;

class UploadProductImageAction
{
    public function __construct(private ImagesApi $api)
    {
    }

    public function execute(int $imageId, UploadedFile $file): ProductImage
    {
        return $this->api->uploadProductImage($imageId, $file)->getData();
    }
}
