<?php

namespace App\Domain\Contents\Actions;

use Ensi\CmsClient\Api\MenusApi;
use Ensi\CmsClient\ApiException;
use Ensi\CmsClient\Dto\MenuTree;
use Ensi\CmsClient\Dto\UpdateMenuTreesRequest;

class ReplaceMenuTreesAction
{
    public function __construct(
        protected MenusApi $menusApi
    ) {
    }

    /**
     * @param int $id
     * @param array $data
     * @return array|MenuTree[]
     * @throws ApiException
     */
    public function execute(int $id, array $data): array
    {
        $requestMenusApi = new UpdateMenuTreesRequest($data);
        $responseMenusApi =  $this->menusApi->updateMenuTrees($id, $requestMenusApi);

        return $responseMenusApi->getData();
    }
}
