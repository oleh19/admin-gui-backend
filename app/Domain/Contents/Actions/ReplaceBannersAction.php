<?php

namespace App\Domain\Contents\Actions;

use Ensi\CmsClient\Api\BannersApi;
use Ensi\CmsClient\Dto\Banner;
use Ensi\CmsClient\Dto\BannerForReplace;

class ReplaceBannersAction
{
    public function __construct(
        private BannersApi $bannersApi
    ) {
    }

    public function execute(int $id, $data): Banner
    {
        $requestBannersApi = new BannerForReplace($data);
        $responseBannersApi =  $this->bannersApi->replaceBanners($id, $requestBannersApi);

        return $responseBannersApi->getData();
    }
}
