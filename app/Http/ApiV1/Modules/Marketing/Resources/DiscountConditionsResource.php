<?php


namespace App\Http\ApiV1\Modules\Marketing\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\MarketingClient\Dto\DiscountCondition;

/**
 * Class DiscountConditionsResource
 * @package App\Http\ApiV1\Modules\Marketing\Resources
 * @mixin DiscountCondition
 */
class DiscountConditionsResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),
            'discount_id' => $this->getDiscountId(),
            'type' => $this->getType(),
            'condition' => $this->getCondition(),
        ];
    }
}
