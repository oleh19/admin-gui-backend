<?php

namespace App\Http\ApiV1\Modules\Communication\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CommunicationManagerClient\Dto\Theme;

/** @mixin Theme */
class ThemesResource extends BaseJsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'active' => $this->getActive(),
            'channel' => $this->getChannel(),
        ];
    }
}
