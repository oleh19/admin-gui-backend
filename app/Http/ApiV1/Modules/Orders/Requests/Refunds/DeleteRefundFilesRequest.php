<?php

namespace App\Http\ApiV1\Modules\Orders\Requests\Refunds;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class DeleteRefundFilesRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'file_ids' => ['required', 'array', 'min:1'],
        ];
    }

    public function getFileIds(): array
    {
        return $this->get('file_ids');
    }
}
