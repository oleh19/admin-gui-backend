<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\Attributes;

use App\Http\ApiV1\Modules\Catalog\Queries\PimQuery;
use Ensi\PimClient\Api\PropertiesApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\DirectoryValueResponse;
use Ensi\PimClient\Dto\SearchDirectoryValuesRequest;
use Ensi\PimClient\Dto\SearchDirectoryValuesResponse;
use Illuminate\Http\Request;

class DirectoryValuesQuery extends PimQuery
{
    public function __construct(Request $request, private PropertiesApi $api)
    {
        parent::__construct($request, SearchDirectoryValuesRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function searchById($id): DirectoryValueResponse
    {
        return $this->api->getDirectoryValue($id);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchDirectoryValuesResponse
    {
        return $this->api->searchDirectoryValues($request);
    }
}
