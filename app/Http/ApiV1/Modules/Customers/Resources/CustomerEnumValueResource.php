<?php

namespace App\Http\ApiV1\Modules\Customers\Resources;

use App\Domain\Customers\Data\CustomerData;
use App\Http\ApiV1\Support\Resources\EnumValueResource;

/**
 * Class CustomerEnumValueResource
 * @package App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices
 *
 * @mixin CustomerData
 */
class CustomerEnumValueResource extends EnumValueResource
{
    protected function getEnumId(): string
    {
        return $this->customer->getId();
    }

    protected function getEnumTitle(): string
    {
        return $this->customer->getEmail();
    }
}
