<?php

namespace App\Http\ApiV1\Modules\Logistic\Queries\DeliveryServices;

use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Ensi\LogisticClient\ApiException;
use Illuminate\Support\Collection;

/**
 * Class GetDeliveryMethodsQuery
 * @package App\Http\ApiV1\Modules\Logistic\Queries\DeliveryServices
 */
class GetDeliveryMethodsQuery
{
    private DeliveryServicesApi $deliveryServicesApi;

    /**
     * GetDeliveryMethodsQuery constructor.
     * @param DeliveryServicesApi $deliveryServicesApi
     */
    public function __construct(DeliveryServicesApi $deliveryServicesApi)
    {
        $this->deliveryServicesApi = $deliveryServicesApi;
    }

    /**
     * @return Collection
     * @throws ApiException
     */
    public function execute(): Collection
    {
        return collect($this->deliveryServicesApi->getDeliveryMethods()->getData());
    }
}
