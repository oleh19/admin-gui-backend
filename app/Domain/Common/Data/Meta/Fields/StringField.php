<?php

namespace App\Domain\Common\Data\Meta\Fields;

use App\Http\ApiV1\OpenApiGenerated\Dto\FieldTypeEnum;

class StringField extends AbstractField
{
    protected function type(): string
    {
        return FieldTypeEnum::STRING;
    }

    protected function init()
    {
        $this->filterLike();
    }
}
