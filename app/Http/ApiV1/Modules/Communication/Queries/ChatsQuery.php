<?php

namespace App\Http\ApiV1\Modules\Communication\Queries;

use App\Domain\Communication\Data\ChatData;
use Ensi\CommunicationManagerClient\ApiException;
use Ensi\InternalMessenger\Api\ChatsApi;
use Ensi\InternalMessenger\Dto\Chat;
use Ensi\InternalMessenger\Dto\SearchChatsRequest;
use Ensi\InternalMessenger\Dto\SearchChatsResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ChatsQuery extends CommunicationQuery
{
    public function __construct(Request $request, private ChatsApi $api)
    {
        parent::__construct($request, SearchChatsRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchChatsResponse
    {
        return $this->api->searchChats($request);
    }

    protected function convertGetToItems($response)
    {
        return $this->convertArray($response->getData());
    }

    protected function convertArray(array $chats)
    {
        /** @var Collection|Chat[] $chats */
        $chats = collect($chats);

        $chatsData = [];
        foreach ($chats as $chat) {
            $chatData = new ChatData($chat);

            $chatsData[] = $chatData;
        }

        return $chatsData;
    }
}
