<?php

namespace App\Domain\Catalog\Actions\Classifiers\Brands;

use Ensi\PimClient\Api\BrandsApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\Brand;
use Ensi\PimClient\Dto\CreateBrandRequest;

class CreateBrandAction
{
    public function __construct(
        private BrandsApi $brandsApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): Brand
    {
        $request = new CreateBrandRequest($fields);

        return $this->brandsApi->createBrand($request)->getData();
    }
}
