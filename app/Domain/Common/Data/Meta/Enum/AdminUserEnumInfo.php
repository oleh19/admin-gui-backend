<?php

namespace App\Domain\Common\Data\Meta\Enum;

class AdminUserEnumInfo extends AbstractEnumInfo
{
    public function __construct()
    {
        $this->endpointName = 'units.searchAdminUserEnumValues';
    }
}
