<?php

namespace App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\LogisticClient\Dto\MetroStation;
use Illuminate\Http\Request;

/**
 * Class MetroStationsResource
 * @package App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices
 *
 * @mixin MetroStation
 */
class MetroStationsResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'metro_line_id' => $this->getMetroLineId(),
            'city_guid' => $this->getCityGuid(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),
            'metro_line' => MetroLinesResource::collection($this->whenNotNull($this->getMetroLine())),
        ];
    }
}
