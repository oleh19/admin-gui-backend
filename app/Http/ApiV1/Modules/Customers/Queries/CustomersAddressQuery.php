<?php

namespace App\Http\ApiV1\Modules\Customers\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use Ensi\CustomersClient\Api\AddressesApi;
use Ensi\CustomersClient\Dto\CustomerAddress;
use Ensi\CustomersClient\Dto\SearchCustomerAddressesRequest;
use Ensi\CustomersClient\Dto\SearchCustomerAddressesResponse;
use Illuminate\Http\Request;

/**
 * Class CustomersAddressQuery
 * @package App\Http\ApiV1\Modules\Customers\Queries
 */
class CustomersAddressQuery extends CrmQuery
{
    use QueryBuilderFindTrait;

    /**
     * CustomersAddressQuery constructor.
     * @param Request $httpRequest
     * @param AddressesApi $addressesApi
     */
    public function __construct(
        protected Request      $httpRequest,
        protected AddressesApi $addressesApi
    ) {
        parent::__construct($httpRequest, SearchCustomerAddressesRequest::class);
    }

    /**
     * @param $request
     * @return SearchCustomerAddressesResponse
     * @throws \Ensi\CustomersClient\ApiException
     */
    protected function search($request): SearchCustomerAddressesResponse
    {
        return $this->addressesApi->searchCustomerAddresses($request);
    }

    /**
     * @param int $id
     * @return CustomerAddress
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function searchById($id): CustomerAddress
    {
        return $this->addressesApi->getCustomerAddress($id)->getData();
    }
}
