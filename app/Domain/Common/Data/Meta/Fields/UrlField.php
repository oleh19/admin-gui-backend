<?php

namespace App\Domain\Common\Data\Meta\Fields;

use App\Http\ApiV1\OpenApiGenerated\Dto\FieldTypeEnum;

class UrlField extends AbstractField
{
    protected function type(): string
    {
        return FieldTypeEnum::URL;
    }
}
