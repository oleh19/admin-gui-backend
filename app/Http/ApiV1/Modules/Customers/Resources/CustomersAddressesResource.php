<?php

namespace App\Http\ApiV1\Modules\Customers\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CustomersClient\Dto\CustomerAddress;
use Illuminate\Http\Request;

/**
 * Class CustomerAddressesResource
 * @package App\Http\ApiV1\Modules\Customers\Resources
 * @mixin CustomerAddress
 */
class CustomersAddressesResource extends BaseJsonResource
{
    /**
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            "id" => $this->getId(),
            "customer_id" => $this->getCustomerId(),
            "address_string" => $this->getAddressString(),
            "default" => $this->getDefault(),
            "post_index" => $this->getPostIndex(),
            "country_code" => $this->getCountryCode(),
            "region" => $this->getRegion(),
            "region_guid" => $this->getRegionGuid(),
            "area" => $this->getArea(),
            "area_guid" => $this->getAreaGuid(),
            "city" => $this->getCity(),
            "city_guid" => $this->getCityGuid(),
            "street" => $this->getStreet(),
            "house" => $this->getHouse(),
            "block" => $this->getBlock(),
            "porch" => $this->getPorch(),
            "intercom" => $this->getIntercom(),
            "floor" => $this->getFloor(),
            "flat" => $this->getFlat(),
            "comment" => $this->getComment(),
            "geo_lat" => $this->getGeoLat(),
            "geo_lon" => $this->getGeoLon(),
        ];
    }
}
