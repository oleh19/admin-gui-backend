<?php

namespace App\Http\ApiV1\Modules\Customers\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use Ensi\CrmClient\Api\CustomersInfoApi;
use Ensi\CrmClient\Dto\CustomerInfo;
use Ensi\CrmClient\Dto\SearchCustomersInfoRequest;
use Ensi\CrmClient\Dto\SearchCustomersInfoResponse;
use Illuminate\Http\Request;

class CustomersCustomersInfoQuery extends CrmQuery
{
    use QueryBuilderFindTrait;

    /**
     * CustomersCustomersInfoQuery constructor.
     * @param Request $httpRequest
     * @param CustomersInfoApi $customersInfoApi
     */
    public function __construct(
        protected Request          $httpRequest,
        protected CustomersInfoApi $customersInfoApi
    ) {
        parent::__construct($httpRequest, SearchCustomersInfoRequest::class);
    }

    /**
     * @param $request
     * @return SearchCustomersInfoResponse
     * @throws \Ensi\CrmClient\ApiException
     */
    protected function search($request): SearchCustomersInfoResponse
    {
        return $this->customersInfoApi->searchCustomersInfo($request);
    }

    /**
     * @param $id
     * @return CustomerInfo
     * @throws \Ensi\CrmClient\ApiException
     */
    public function searchById($id): CustomerInfo
    {
        return $this->customersInfoApi->getCustomerInfo($id)->getData();
    }
}
