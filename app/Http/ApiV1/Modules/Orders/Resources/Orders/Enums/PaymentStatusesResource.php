<?php


namespace App\Http\ApiV1\Modules\Orders\Resources\Orders\Enums;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\OmsClient\Dto\PaymentStatus;

/**
 * Class PaymentStatusesResource
 * @package App\Http\ApiV1\Modules\Orders\Resources
 * @mixin PaymentStatus
 */
class PaymentStatusesResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
        ];
    }
}
