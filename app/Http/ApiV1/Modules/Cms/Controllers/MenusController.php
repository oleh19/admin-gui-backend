<?php

namespace App\Http\ApiV1\Modules\Cms\Controllers;

use App\Http\ApiV1\Modules\Cms\Queries\SearchMenusQuery;
use App\Http\ApiV1\Modules\Cms\Resources\MenusResource;

class MenusController
{
    public function search(SearchMenusQuery $query)
    {
        return MenusResource::collectPage($query->get());
    }

    public function searchOne(SearchMenusQuery $query)
    {
        return MenusResource::make($query->first());
    }
}
