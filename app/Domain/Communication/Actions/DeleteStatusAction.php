<?php

namespace App\Domain\Communication\Actions;

use Ensi\CommunicationManagerClient\Api\StatusesApi;
use Ensi\CommunicationManagerClient\ApiException;

class DeleteStatusAction
{
    public function __construct(
        private StatusesApi $statusesApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $statusId)
    {
        return $this->statusesApi->deleteStatus($statusId);
    }
}
