<?php

use App\Http\ApiV1\Modules\Common\Controllers\FilesController;
use Illuminate\Support\Facades\Route;

Route::post('files/download-protected', [FilesController::class, 'downloadProtected']);
