<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers;

use App\Domain\Catalog\Actions\Products\CreateProductAction;
use App\Domain\Catalog\Actions\Products\DeleteProductAction;
use App\Domain\Catalog\Actions\Products\PatchAttributesValuesAction;
use App\Domain\Catalog\Actions\Products\ReplaceAttributesValuesAction;
use App\Domain\Catalog\Actions\Products\ReplaceProductAction;
use App\Http\ApiV1\Modules\Catalog\Queries\Products\ProductsQuery;
use App\Http\ApiV1\Modules\Catalog\Requests\Products\CreateOrReplaceProductRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Products\ReplaceAttributesValuesRequest;
use App\Http\ApiV1\Modules\Catalog\Resources\Products\ProductsResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ProductsController
{
    public function create(CreateOrReplaceProductRequest $request, CreateProductAction $action): ProductsResource
    {
        return new ProductsResource($action->execute($request->validated()));
    }

    public function replace(
        int $productId,
        CreateOrReplaceProductRequest $request,
        ReplaceProductAction $action
    ): ProductsResource {
        return new ProductsResource($action->execute($productId, $request->validated()));
    }

    public function delete(int $productId, DeleteProductAction $action): EmptyResource
    {
        $action->execute($productId);

        return new EmptyResource();
    }

    public function get(int $productId, ProductsQuery $query): ProductsResource
    {
        return new ProductsResource($query->find($productId));
    }

    public function search(ProductsQuery $query): AnonymousResourceCollection
    {
        return ProductsResource::collectPage($query->get());
    }

    public function replaceAttributes(
        int $productId,
        ReplaceAttributesValuesRequest $request,
        ReplaceAttributesValuesAction $action
    ): ProductsResource {
        return new ProductsResource($action->execute($productId, $request->validated()));
    }

    public function patchAttributes(
        int $productId,
        ReplaceAttributesValuesRequest $request,
        PatchAttributesValuesAction $action
    ): ProductsResource {
        return new ProductsResource($action->execute($productId, $request->validated()));
    }
}
