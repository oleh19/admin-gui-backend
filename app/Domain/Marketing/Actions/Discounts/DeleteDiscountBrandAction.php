<?php

namespace App\Domain\Marketing\Actions\Discounts;

use Ensi\MarketingClient\Api\DiscountBrandsApi;

class DeleteDiscountBrandAction
{
    public function __construct(protected DiscountBrandsApi $discountBrandsApi)
    {
    }

    public function execute(int $discountBrandId): void
    {
        $this->discountBrandsApi->deleteDiscountBrand($discountBrandId);
    }
}
