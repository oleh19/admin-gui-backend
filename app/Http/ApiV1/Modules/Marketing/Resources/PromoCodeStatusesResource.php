<?php


namespace App\Http\ApiV1\Modules\Marketing\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\MarketingClient\Dto\PromoCodeStatus;

/**
 * Class PromoCodeStatusesResource
 * @package App\Http\ApiV1\Modules\Marketing\Resources
 * @mixin PromoCodeStatus
 */
class PromoCodeStatusesResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
        ];
    }
}
