<?php

namespace App\Domain\Customers\Actions\Attributes;

use Ensi\CustomersClient\Api\AttributesApi;

abstract class CustomerAttributeAction
{
    /**
     * CustomerAddressAction constructor.
     * @param AttributesApi $attributesApi
     */
    public function __construct(protected AttributesApi $attributesApi)
    {
    }
}
