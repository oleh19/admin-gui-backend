<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Attributes;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\Property;

/**
 * @mixin Property
 */
class ProductAttributesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'code' => $this->getCode(),
            'type' => $this->getType(),
            'name' => $this->getName(),
            'display_name' => $this->getDisplayName(),
            'is_multiple' => $this->getIsMultiple(),
            'is_filterable' => $this->getIsFilterable(),
            'is_color' => $this->getIsColor(),
            'directory' => DirectoryValuesResource::collection($this->whenNotNull($this->getDirectory())),
        ];
    }
}
