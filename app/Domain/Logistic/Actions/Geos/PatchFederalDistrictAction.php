<?php


namespace App\Domain\Logistic\Actions\Geos;

use Ensi\LogisticClient\Api\GeosApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\FederalDistrict;
use Ensi\LogisticClient\Dto\PatchFederalDistrictRequest;

/**
 * Class PatchFederalDistrictAction
 * @package App\Domain\Logistic\Actions\Geos
 */
class PatchFederalDistrictAction
{
    protected GeosApi $geosApi;

    /**
     * PatchFederalDistrictAction constructor.
     * @param  GeosApi  $geosApi
     */
    public function __construct(GeosApi $geosApi)
    {
        $this->geosApi = $geosApi;
    }

    /**
     * @param  int  $federalDistrictId
     * @param  array  $fields
     * @return FederalDistrict
     * @throws ApiException
     */
    public function execute(int $federalDistrictId, array $fields): FederalDistrict
    {
        $request = new PatchFederalDistrictRequest();

        if (isset($fields['name'])) {
            $request->setName($fields['name']);
        }

        return $this->geosApi->patchFederalDistrict($federalDistrictId, $request)->getData();
    }
}
