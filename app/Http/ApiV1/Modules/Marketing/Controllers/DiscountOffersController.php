<?php


namespace App\Http\ApiV1\Modules\Marketing\Controllers;

use App\Domain\Marketing\Actions\Discounts\CreateDiscountOfferAction;
use App\Domain\Marketing\Actions\Discounts\DeleteDiscountOfferAction;
use App\Domain\Marketing\Actions\Discounts\PatchDiscountOfferAction;
use App\Http\ApiV1\Modules\Marketing\Queries\DiscountOffersQuery;
use App\Http\ApiV1\Modules\Marketing\Requests\CreateDiscountOfferRequest;
use App\Http\ApiV1\Modules\Marketing\Requests\PatchDiscountOfferRequest;
use App\Http\ApiV1\Modules\Marketing\Resources\DiscountOffersResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class DiscountOffersController
{
    public function findById(int $discountOfferId, DiscountOffersQuery $query)
    {
        return new DiscountOffersResource($query->find($discountOfferId));
    }

    public function search(DiscountOffersQuery $query)
    {
        return DiscountOffersResource::collectPage($query->get());
    }

    public function create(
        CreateDiscountOfferRequest $request,
        CreateDiscountOfferAction $action
    ): DiscountOffersResource {
        return new DiscountOffersResource($action->execute($request->validated()));
    }

    public function patch(
        int $discountOfferId,
        PatchDiscountOfferRequest $request,
        PatchDiscountOfferAction $action
    ): DiscountOffersResource {
        return new DiscountOffersResource($action->execute($discountOfferId, $request->validated()));
    }

    public function delete(int $discountOfferId, DeleteDiscountOfferAction $action): EmptyResource
    {
        $action->execute($discountOfferId);

        return new EmptyResource();
    }
}
