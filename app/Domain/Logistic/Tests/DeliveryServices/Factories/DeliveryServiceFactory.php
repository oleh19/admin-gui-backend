<?php

namespace App\Domain\Logistic\Tests\DeliveryServices\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\LogisticClient\Dto\DeliveryService;
use Ensi\LogisticClient\Dto\SearchDeliveryServicesResponse;

class DeliveryServiceFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        // todo доделать остальные поля
        return [
            'id' => $this->faker->randomNumber(),
            'name' => $this->faker->text(20),
        ];
    }

    public function make(array $extra = []): DeliveryService
    {
        return new DeliveryService($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extra = []): SearchDeliveryServicesResponse
    {
        return new SearchDeliveryServicesResponse([
            'data' => [$this->make($this->makeArray($extra))],
        ]);
    }
}
