<?php

namespace App\Http\ApiV1\Modules\Units\Resources\Stores;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\BuClient\Dto\StoreContact;

/** @mixin StoreContact */
class StoreContactsResource extends BaseJsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),
            'store_id' => $this->getStoreId(),
            'name' => $this->getName(),
            'phone' => $this->getPhone(),
            'email' => $this->getEmail(),
            'created_at' => $this->getCreatedAt(),
            'updated_at' => $this->getUpdatedAt(),
        ];
    }
}
