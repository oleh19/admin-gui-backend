<?php

namespace App\Domain\Catalog\Actions\Products;

use Ensi\PimClient\Api\ImagesApi;
use Ensi\PimClient\Dto\PatchProductImageRequest;
use Ensi\PimClient\Dto\ProductImage;

class PatchProductImageAction
{
    public function __construct(private ImagesApi $api)
    {
    }

    public function execute(int $imageId, array $fields): ProductImage
    {
        $request = new PatchProductImageRequest($fields);

        return $this->api->patchProductImage($imageId, $request)->getData();
    }
}
