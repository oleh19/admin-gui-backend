<?php

use App\Domain\Orders\ProtectedFiles\RefundFile;
use App\Domain\Orders\Tests\Factories\Refunds\RefundFactory;
use App\Domain\Orders\Tests\Factories\Refunds\RefundFileFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use App\Http\ApiV1\Support\Tests\Factories\EnsiFileFactory;
use Ensi\OmsClient\Dto\File;
use Illuminate\Http\UploadedFile;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\post;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'orders', 'orders.refundFiles');

test('POST /api/v1/orders/refunds/{id}:attach-file 200', function (string $ext) {
    /** @var ApiV1ComponentTestCase $this */
    $refundId = 1;
    $fileName = "file.{$ext}";
    $fileId = 1;

    $this->mockOmsRefundsApi()->allows([
        'attachRefundFile' => RefundFileFactory::new()->makeResponseOne(['id' => $fileId,]),
    ]);

    $requestBody = ['file' => UploadedFile::fake()->create($fileName, 100)];
    post("/api/v1/orders/refunds/{$refundId}:attach-file", $requestBody, ['Content-Type' => "multipart/form-data"])
        ->assertStatus(200)
        ->assertJsonPath('data.id', $fileId);
})->with(['jpg', 'pdf']);


test('POST /common/files/download-protected refund-file 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $refundId = 1;
    $refundFile = RefundFileFactory::new()->make([
        'refund_id' => $refundId,
        'file' => new File(EnsiFileFactory::new()->makeReal()),
    ]);

    $this->mockOmsRefundsApi()->allows([
        'searchRefunds' => RefundFactory::new()->withFile($refundFile)->makeResponseSearch(['id' => $refundId]),
    ]);

    postJson("/api/v1/common/files/download-protected", RefundFile::createFromModel($refundFile)->jsonSerialize())
        ->assertStatus(200);
});

test('POST /api/v1/orders/refunds/{id}:delete-files 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $refundId = 1;

    $this->mockOmsRefundsApi()->shouldReceive('deleteRefundFiles');

    deleteJson("/api/v1/orders/refunds/{$refundId}:delete-files", ['file_ids' => [1, 2]])
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});
