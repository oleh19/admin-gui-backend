<?php

namespace App\Domain\Logistic\Actions\DeliveryServices;

use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\DeletePaymentMethodFromDeliveryServiceRequest;

class DeletePaymentMethodFromDeliveryServiceAction
{
    public function __construct(protected DeliveryServicesApi $deliveryServicesApi)
    {
    }

    /**
     * @param int $deliveryServiceId
     * @param int $paymentMethod
     * @throws ApiException
     */
    public function execute(int $deliveryServiceId, int $paymentMethod): void
    {
        $dto = new DeletePaymentMethodFromDeliveryServiceRequest();
        $dto->setPaymentMethod($paymentMethod);
        $this->deliveryServicesApi->deletePaymentMethodFromDeliveryService($deliveryServiceId, $dto);
    }
}
