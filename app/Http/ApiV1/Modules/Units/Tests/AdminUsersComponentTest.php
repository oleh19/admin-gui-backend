<?php

use App\Domain\Units\Tests\Factories\AdminUserFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/units/admin-user-enum-values:search 200', function ($key, $value) {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockAdminAuthUsersApi()->allows([
        'searchUsers' => AdminUserFactory::new()->makeResponseSearch(),
    ]);

    postJson("/api/v1/units/admin-user-enum-values:search", ['filter' => [$key => $value]])->assertStatus(200);
})->with([
    ['id', [1, 2]],
    ['query', 'name'],
]);
