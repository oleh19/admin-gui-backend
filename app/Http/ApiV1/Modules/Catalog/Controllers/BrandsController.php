<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers;

use App\Domain\Catalog\Actions\Classifiers\Brands\CreateBrandAction;
use App\Domain\Catalog\Actions\Classifiers\Brands\DeleteBrandAction;
use App\Domain\Catalog\Actions\Classifiers\Brands\DeleteBrandImageAction;
use App\Domain\Catalog\Actions\Classifiers\Brands\ReplaceBrandAction;
use App\Domain\Catalog\Actions\Classifiers\Brands\SaveBrandImageAction;
use App\Http\ApiV1\Modules\Catalog\Queries\Classifiers\BrandsQuery;
use App\Http\ApiV1\Modules\Catalog\Requests\Classifiers\CreateOrReplaceBrandRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Classifiers\UploadBrandImageRequest;
use App\Http\ApiV1\Modules\Catalog\Resources\Classifiers\BrandsResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class BrandsController
{
    public function search(BrandsQuery $query): AnonymousResourceCollection
    {
        return BrandsResource::collectPage($query->get());
    }

    public function create(
        CreateOrReplaceBrandRequest $request,
        CreateBrandAction $action
    ): BrandsResource {
        return new BrandsResource($action->execute($request->validated()));
    }

    public function get(
        int $brandId,
        BrandsQuery $query
    ): BrandsResource {
        return new BrandsResource($query->find($brandId));
    }

    public function replace(
        int $brandId,
        CreateOrReplaceBrandRequest $request,
        ReplaceBrandAction $action
    ): BrandsResource {
        return new BrandsResource($action->execute($brandId, $request->validated()));
    }

    public function delete(
        int $brandId,
        DeleteBrandAction $action
    ): EmptyResource {
        $action->execute($brandId);

        return new EmptyResource();
    }

    public function uploadImage(
        int $brandId,
        UploadBrandImageRequest $request,
        SaveBrandImageAction $action
    ): BrandsResource {
        return new BrandsResource($action->execute($brandId, $request->file('file')));
    }

    public function deleteImage(
        int $brandId,
        DeleteBrandImageAction $action
    ): BrandsResource {
        return new BrandsResource($action->execute($brandId));
    }
}
