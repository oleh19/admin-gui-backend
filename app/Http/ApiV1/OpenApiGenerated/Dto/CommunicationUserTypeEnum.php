<?php
/**
 * CommunicationUserTypeEnum
 *
 * PHP version 7
 *
 * @category Class
 * @package  App\Http\ApiV1\OpenApiGenerated
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Admin GUI Backend
 *
 * Admin GUI Backend
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace App\Http\ApiV1\OpenApiGenerated\Dto;

/**
 * CommunicationUserTypeEnum Class Doc Comment
 *
 * @category Class
 * @description Тип пользователя. Расшифровка значений:   * &#x60;1&#x60; - Сотрудник   * &#x60;2&#x60; - Продавец   * &#x60;3&#x60; - Покупатель
 * @package  App\Http\ApiV1\OpenApiGenerated
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class CommunicationUserTypeEnum
{
    /**
     * Possible values of this enum
     */
    const ADMIN = 1;
    const SELLER = 2;
    const CUSTOMER = 3;

    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public static function getAllowableEnumValues(): array
    {
        return [
            self::ADMIN,
            self::SELLER,
            self::CUSTOMER,
        ];
    }
}
