<?php


namespace App\Domain\Logistic\Actions\DeliveryKpis;

use Ensi\LogisticClient\Api\KpiApi;
use Ensi\LogisticClient\ApiException;

/**
 * Class DeleteDeliveryKpiCtAction
 * @package App\Domain\Logistic\Actions\DeliveryKpis
 */
class DeleteDeliveryKpiCtAction
{
    /**
     * DeleteDeliveryKpiCtAction constructor.
     * @param KpiApi $kpiApi
     */
    public function __construct(protected KpiApi $kpiApi)
    {
    }

    /**
     * @param int $sellerId
     * @throws ApiException
     */
    public function execute(int $sellerId): void
    {
        $this->kpiApi->deleteDeliveryKpiCt($sellerId);
    }
}
