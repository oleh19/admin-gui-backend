<?php

namespace App\Http\ApiV1\Modules\Communication\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\CommunicationManagerClient\Dto\UserTypeEnum;
use Illuminate\Validation\Rule;

class CreateMessageRequest extends BaseFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'chat_id' => ['required', 'integer'],
            'user_id' => ['required', 'integer'],
            'user_type' => ['required', Rule::in(UserTypeEnum::getAllowableEnumValues())],
            'text' => ['nullable', 'string'],
            'files' => ['nullable', 'array'],
        ];
    }
}
