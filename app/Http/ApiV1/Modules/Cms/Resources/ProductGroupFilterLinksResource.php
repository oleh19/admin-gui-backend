<?php

namespace App\Http\ApiV1\Modules\Cms\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CmsClient\Dto\ProductGroupFilter;
use Illuminate\Http\Request;

/** @mixin ProductGroupFilter */
class ProductGroupFilterLinksResource extends BaseJsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),
            'code' => $this->getCode(),
            'value' => $this->getValue(),
            'product_group_id' => $this->getProductGroupId(),
        ];
    }
}
