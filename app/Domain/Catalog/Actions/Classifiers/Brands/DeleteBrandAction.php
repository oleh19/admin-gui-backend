<?php

namespace App\Domain\Catalog\Actions\Classifiers\Brands;

use Ensi\PimClient\Api\BrandsApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\EmptyDataResponse;

class DeleteBrandAction
{
    public function __construct(
        private BrandsApi $brandsApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $brandId): EmptyDataResponse
    {
        return $this->brandsApi->deleteBrand($brandId);
    }
}
