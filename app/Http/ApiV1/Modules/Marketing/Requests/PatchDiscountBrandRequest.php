<?php

namespace App\Http\ApiV1\Modules\Marketing\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchDiscountBrandRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'brand_id' => ['integer'],
            'except' => ['boolean'],
        ];
    }
}
