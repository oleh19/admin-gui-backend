<?php

namespace App\Domain\Catalog\Tests\Factories\Classifiers;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use App\Http\ApiV1\Support\Tests\Factories\EnsiFileFactory;
use Ensi\PimClient\Dto\Brand;
use Ensi\PimClient\Dto\File;

class BrandFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->randomNumber(),

            'name' => $this->faker->text(20),
            'code' => $this->faker->word(),
            'description' => $this->faker->text(50),
            'file' => new File(EnsiFileFactory::new()->make()),

            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): Brand
    {
        return new Brand($this->makeArray($extra));
    }
}
