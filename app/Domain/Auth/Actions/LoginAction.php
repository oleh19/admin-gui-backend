<?php

namespace App\Domain\Auth\Actions;

use Ensi\AdminAuthClient\Api\OauthApi;
use Ensi\AdminAuthClient\Dto\CreateTokenRequest;
use Ensi\AdminAuthClient\Dto\CreateTokenResponse;
use Ensi\AdminAuthClient\Dto\ErrorResponse;
use Ensi\AdminAuthClient\Dto\GrantTypeEnum;

class LoginAction
{
    public function __construct(private OauthApi $oauthApi)
    {
    }

    public function execute(array $fields): CreateTokenResponse|ErrorResponse
    {
        $request = new CreateTokenRequest();
        $request->setGrantType(GrantTypeEnum::PASSWORD);
        $request->setClientId(config('openapi-clients.units.admin-auth.client.id'));
        $request->setClientSecret(config('openapi-clients.units.admin-auth.client.secret'));
        $request->setUsername($fields['login']);
        $request->setPassword($fields['password']);

        return $this->oauthApi->createToken($request);
    }
}
