<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Categories;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\Category;

/**
 * @mixin Category
 */
class CategoriesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'code' => $this->getCode(),

            'parent_id' => $this->getParentId(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
        ];
    }
}
