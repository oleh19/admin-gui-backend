<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Products;

use App\Http\ApiV1\Modules\Catalog\Resources\Categories\CategoriesResource;
use App\Http\ApiV1\Modules\Catalog\Resources\Classifiers\BrandsResource;
use App\Http\ApiV1\Modules\Catalog\Resources\Classifiers\CountriesResource;
use App\Http\ApiV1\Modules\Catalog\Resources\Classifiers\ManufacturersResource;
use App\Http\ApiV1\Modules\Catalog\Resources\Classifiers\ProductTypesResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\Product;

/**
 * @mixin Product
 */
class ProductsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'code' => $this->getCode(),
            'name' => $this->getName(),
            'description' => $this->getDescription(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),

            'external_id' => $this->getExternalId(),
            'product_type_id' => $this->getProductTypeId(),
            'country_id' => $this->getCountryId(),
            'manufacturer_id' => $this->getManufacturerId(),
            'brand_id' => $this->getBrandId(),
            'category_id' => $this->getCategoryId(),

            'barcode' => $this->getBarcode(),
            'weight' => $this->getWeight(),
            'weight_gross' => $this->getWeightGross(),
            'length' => $this->getLength(),
            'height' => $this->getHeight(),
            'width' => $this->getWidth(),
            'ingredients' => $this->getIngredients(),

            'archive' => $this->getArchive(),
            'is_new' => $this->getIsNew(),

            'category' => new CategoriesResource($this->whenNotNull($this->getCategory())),

            'brand' => new BrandsResource($this->whenNotNull($this->getBrand())),
            'type' => new ProductTypesResource($this->whenNotNull($this->getType())),
            'country' => new CountriesResource($this->whenNotNull($this->getCountry())),
            'manufacturer' => new ManufacturersResource($this->whenNotNull($this->getManufacturer())),

            'images' => ProductImagesResource::collection($this->whenNotNull($this->getImages())),
            'attributes' => ProductAttributeValuesResource::collection($this->whenNotNull($this->getAttributes())),
        ];
    }
}
