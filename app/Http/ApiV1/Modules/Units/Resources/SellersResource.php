<?php

namespace App\Http\ApiV1\Modules\Units\Resources;

use App\Domain\Units\Data\SellerData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/**
 * Class SellersResource
 * @package App\Http\ApiV1\Modules\Units\Resources
 * @mixin SellerData
 */
class SellersResource extends BaseJsonResource
{
    /**
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->seller->getId(),
            'legal_name' => $this->seller->getLegalName(),
            'external_id' => $this->seller->getExternalId(),
            'legal_address' => $this->seller->getLegalAddress(),
            'fact_address'  => $this->seller->getFactAddress(),
            'inn' => $this->seller->getInn(),
            'kpp' => $this->seller->getKpp(),
            'payment_account' => $this->seller->getPaymentAccount(),
            'bank' => $this->seller->getBank(),
            'bank_address' => $this->seller->getBankAddress(),
            'bank_bik' => $this->seller->getBankBik(),
            'correspondent_account' => $this->seller->getCorrespondentAccount(),
            'status' => $this->seller->getStatus(),
            'status_at' => $this->seller->getStatusAt(),
            'manager_id' => $this->seller->getManagerId(),
            'rating_id' => $this->seller->getRating(),
            'storage_address' => $this->seller->getStorageAddress(),
            'site' => $this->seller->getSite(),
            'can_integration' => $this->seller->getCanIntegration(),
            'sale_info' => $this->seller->getSaleInfo(),
            'city'  => $this->seller->getCity(),
            'created_at' => $this->seller->getCreatedAt(),
            'updated_at' => $this->seller->getUpdatedAt(),

            'owner' => SellerUsersResource::make($this->whenNotNull($this->owner)),
            'manager' => AdminUsersResource::make($this->whenNotNull($this->manager)),
        ];
    }
}
