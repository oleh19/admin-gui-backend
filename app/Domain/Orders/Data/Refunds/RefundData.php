<?php


namespace App\Domain\Orders\Data\Refunds;

use App\Domain\Orders\Data\Orders\OrderData;
use App\Domain\Orders\Data\Orders\OrderItemData;
use Ensi\OmsClient\Dto\Refund;

class RefundData
{
    public function __construct(public Refund $refund)
    {
    }

    public function getOrder(): ?OrderData
    {
        if (is_null($this->refund->getOrder())) {
            return null;
        }

        return new OrderData($this->refund->getOrder());
    }

    public function getOrderItems(): ?array
    {
        if (is_null($this->refund->getItems())) {
            return null;
        }

        $items = [];
        foreach ($this->refund->getItems() as $item) {
            $items[] = new OrderItemData($item);
        }

        return $items;
    }
}
