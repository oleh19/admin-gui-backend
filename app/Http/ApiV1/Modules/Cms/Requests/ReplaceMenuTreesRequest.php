<?php

namespace App\Http\ApiV1\Modules\Cms\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class ReplaceMenuTreesRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'items' => 'array|required',
            'items.*.name' => 'string|required',
            'items.*.url' => 'string|nullable',
            'items.*.children' => 'array',
        ];
    }
}
