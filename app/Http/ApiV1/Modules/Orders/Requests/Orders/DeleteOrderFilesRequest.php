<?php

namespace App\Http\ApiV1\Modules\Orders\Requests\Orders;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class DeleteOrderFilesRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'file_ids' => ['required', 'array', 'min:1'],
        ];
    }

    public function getFileIds(): array
    {
        return $this->get('file_ids');
    }
}
