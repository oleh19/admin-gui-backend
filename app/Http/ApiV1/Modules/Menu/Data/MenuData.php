<?php

namespace App\Http\ApiV1\Modules\Menu\Data;

use App\Http\ApiV1\OpenApiGenerated\Dto\MenuItemCodeEnum;
use Ensi\AdminAuthClient\Dto\RoleEnum;

/**
 * Class MenuData
 * @package App\Http\ApiV1\Modules\Menu\Data
 */
class MenuData
{
    /**
     * @return array[]
     */
    public static function menu(): array
    {
        return [
            //Товары
            MenuItemCodeEnum::PRODUCTS_CODE => [
                'roleIds' => [
                    RoleEnum::ADMIN,
                    RoleEnum::PIM_MANAGER,
                    RoleEnum::SEO_MANAGER,
                ],
                'items' => [
                    //Каталог товаров
                    MenuItemCodeEnum::PRODUCTS_CATALOG_CODE => [],
                    //Предложения продавцов
                    MenuItemCodeEnum::PRODUCTS_OFFERS_CODE => [],
                    //Товарные группы
                    MenuItemCodeEnum::PRODUCTS_VARIANT_GROUPS_CODE => [],
                    //Справочники
                    MenuItemCodeEnum::PRODUCTS_DIRECTORIES_CODE => [
                        'items' => [
                            //Бренды
                            MenuItemCodeEnum::PRODUCTS_DIRECTORIES_BRANDS_CODE => [],
                            //Категории
                            MenuItemCodeEnum::PRODUCTS_DIRECTORIES_CATEGORIES_CODE => [],
                            //Товарные атрибуты
                            MenuItemCodeEnum::PRODUCTS_DIRECTORIES_PROPERTIES_CODE => [],
                        ],
                    ],
                ],
            ],

            //Заказы
            MenuItemCodeEnum::ORDERS_CODE => [
                'roleIds' => [
                    RoleEnum::ADMIN,
                    RoleEnum::OMS_CALL_CENTER,
                    RoleEnum::OMS_LOGISTIC,
                ],
                'items' => [
                    //Список  заказов
                    MenuItemCodeEnum::ORDERS_LIST_CODE => [],
                    //Грузы
                    MenuItemCodeEnum::ORDERS_CARGOS_CODE => [
                        'roleIds' => [
                            RoleEnum::ADMIN,
                            RoleEnum::OMS_LOGISTIC,
                        ],
                    ],
                    //Статусы заказов
                    MenuItemCodeEnum::ORDERS_STATUSES_CODE => [],
                ],
            ],

            //Заявки
            MenuItemCodeEnum::REQUESTS_CODE => [
                'roleIds' => [
                    RoleEnum::ADMIN,
                    RoleEnum::PIM_MANAGER,
                ],
                'items' => [
                    //Проверка товаров
                    MenuItemCodeEnum::REQUESTS_CHECK_CODE => [],
                    //Производство контента
                    MenuItemCodeEnum::REQUESTS_CONTENT_CODE => [],
                    //Изменение цен
                    MenuItemCodeEnum::REQUESTS_PRICES_CODE => [],
                ],
            ],

            //Контент
            MenuItemCodeEnum::CONTENT_CODE => [
                'roleIds' => [
                    RoleEnum::ADMIN,
                    RoleEnum::PIM_MANAGER,
                    RoleEnum::SEO_MANAGER,
                ],
                'items' => [
                    //Меню сайта
                    MenuItemCodeEnum::CONTENT_MENU_CODE => [],
                    //Управление страницами
                    MenuItemCodeEnum::CONTENT_LANDING_CODE => [],
                    //Управление контактами и соц. сетями
                    MenuItemCodeEnum::CONTENT_CONTACTS_CODE => [],
                    //Управление категориями
                    MenuItemCodeEnum::CONTENT_CATEGORIES_CODE => [
                        'roleIds' => [
                            RoleEnum::ADMIN,
                            RoleEnum::PIM_MANAGER,
                            RoleEnum::SEO_MANAGER,
                        ],
                    ],
                    //Подборки товаров
                    MenuItemCodeEnum::CONTENT_PRODUCT_GROUPS_CODE => [
                        'roleIds' => [
                            RoleEnum::ADMIN,
                            RoleEnum::PIM_MANAGER,
                            RoleEnum::SEO_MANAGER,
                        ],
                    ],
                    //Баннеры
                    MenuItemCodeEnum::CONTENT_BANNERS_CODE => [],
                    //Товарные шильдики
                    MenuItemCodeEnum::CONTENT_BADGES_CODE => [
                        'roleIds' => [
                            RoleEnum::ADMIN,
                            RoleEnum::PIM_MANAGER,
                            RoleEnum::SEO_MANAGER,
                        ],
                    ],
                    //Поисковые запросы
                    MenuItemCodeEnum::CONTENT_SEARCH_REQUESTS_CODE => [],
                    //Поисковые синонимы
                    MenuItemCodeEnum::CONTENT_SEARCH_SYNONYMS_CODE => [],
                    //Популярные бренды
                    MenuItemCodeEnum::CONTENT_POPULAR_BRANDS_CODE => [
                        'roleIds' => [
                            RoleEnum::ADMIN,
                            RoleEnum::PIM_MANAGER,
                            RoleEnum::SEO_MANAGER,
                        ],
                    ],
                    //Популярные товары
                    MenuItemCodeEnum::CONTENT_POPULAR_PRODUCTS_CODE => [
                        'roleIds' => [
                            RoleEnum::ADMIN,
                            RoleEnum::PIM_MANAGER,
                            RoleEnum::SEO_MANAGER,
                        ],
                    ],
                ],
            ],

            //Логистика
            MenuItemCodeEnum::LOGISTIC_CODE => [
                'roleIds' => [
                    RoleEnum::ADMIN,
                    RoleEnum::OMS_LOGISTIC,
                ],
                'items' => [
                    //Логистические операторы
                    MenuItemCodeEnum::LOGISTIC_DELIVERY_SERVICES_CODE => [],
                    //Стоимость доставки по регионам
                    MenuItemCodeEnum::LOGISTIC_DELIVERY_PRICES_CODE => [],
                    //Планировщик времени статусов
                    MenuItemCodeEnum::LOGISTIC_KPI_CODE => [],
                ],
            ],

            //Склады
            MenuItemCodeEnum::STORES_SELLER_STORES_CODE => [
                'roleIds' => [
                    RoleEnum::ADMIN,
                    RoleEnum::MANAGER_SELLER,
                ],
            ],

            //Клиенты
            MenuItemCodeEnum::CUSTOMERS_CODE => [
                'roleIds' => [
                    RoleEnum::ADMIN,
                    RoleEnum::MANAGER_CLIENT,
                ],
            ],

            //Продавцы
            MenuItemCodeEnum::SELLER_CODE => [
                'roleIds' => [
                    RoleEnum::ADMIN,
                    RoleEnum::MANAGER_SELLER,
                ],
                'items' => [
                    //Заявка на регистрацию
                    MenuItemCodeEnum::SELLER_LIST_REGISTRATION_CODE => [],
                    //Список продавцов
                    MenuItemCodeEnum::SELLER_LIST_ACTIVE_CODE => [],
                ],
            ],

            //Маркетинг
            MenuItemCodeEnum::MARKETING_CODE => [
                'roleIds' => [
                    RoleEnum::ADMIN,
                    RoleEnum::PIM_MANAGER,
                ],
                'items' => [
                    //Промокоды
                    MenuItemCodeEnum::MARKETING_PROMOCODES_CODE => [],
                    //Скидки
                    MenuItemCodeEnum::MARKETING_DISCOUNTS_CODE => [],
                    //Бандлы
                    MenuItemCodeEnum::MARKETING_BUNDLES_CODE => [],
                ],
            ],

            //Отчеты
            MenuItemCodeEnum::REPORTS_CODE => [
                'roleIds' => [
                    RoleEnum::ADMIN,
                ],
            ],

            //Коммуникации
            MenuItemCodeEnum::COMMUNICATIONS_CODE => [
                'roleIds' => [
                    RoleEnum::ADMIN,
                    RoleEnum::MANAGER_CLIENT,
                ],
                'items' => [
                    //Непрочитанные сообщения
                    MenuItemCodeEnum::COMMUNICATIONS_MESSAGES_CODE => [],
                    //Сервисные уведомления
                    MenuItemCodeEnum::COMMUNICATIONS_NOTIFICATIONS_CODE => [],
                    //Массовая рассылка
                    MenuItemCodeEnum::COMMUNICATIONS_BROADCAST_CODE => [],
                    //Статусы
                    MenuItemCodeEnum::COMMUNICATIONS_STATUSES_CODE => [],
                    //Темы
                    MenuItemCodeEnum::COMMUNICATIONS_SUBJECTS_CODE => [],
                    //Типы
                    MenuItemCodeEnum::COMMUNICATIONS_TYPES_CODE => [],
                ],
            ],

            //Настройки
            MenuItemCodeEnum::SETTINGS_CODE => [
                'roleIds' => [
                    RoleEnum::ADMIN,
                ],
                'items' => [
                    //Пользователи и права
                    MenuItemCodeEnum::SETTINGS_USERS_CODE => [],
                    //Карточка организации
                    MenuItemCodeEnum::SETTINGS_ORGANIZATIONS_CODE => [],
                ],
            ],
        ];
    }
}
