<?php

namespace App\Http\ApiV1\Modules\Marketing\Requests;

use App\Http\ApiV1\OpenApiGenerated\Dto\MarketingDiscountConditionEnum as KeyEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class PatchDiscountConditionRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'condition' => ['nullable', 'array'],
            'condition.*' => Rule::in(KeyEnum::getAllowableEnumValues()),

            /** Condition Min Price */
            'condition.' . KeyEnum::FIELD_MIN_PRICE => [
                'integer',
                'gt:0',
            ],

            /** Condition Brands */
            'condition.' . KeyEnum::FIELD_BRANDS => [
                'array',
            ],
            'condition.' . KeyEnum::FIELD_BRANDS . '.*' => ['integer'],

            /** Condition Categories */
            'condition.' . KeyEnum::FIELD_CATEGORIES => [
                'array',
            ],
            'condition.' . KeyEnum::FIELD_CATEGORIES . '.*' => ['integer'],

            /** Condition Count */
            'condition.' . KeyEnum::FIELD_COUNT => [
                'integer',
                'gt:0',
            ],
            'condition.' . KeyEnum::FIELD_OFFER => [
                'integer',
            ],

            /** Condition Delivery Methods */
            'condition.' . KeyEnum::FIELD_DELIVERY_METHODS => [
                'array',
            ],
            'condition.' . KeyEnum::FIELD_DELIVERY_METHODS . '.*' => ['integer'],

            /** Condition Payment Methods */
            'condition.' . KeyEnum::FIELD_PAYMENT_METHODS => [
                'array',
            ],
            'condition.' . KeyEnum::FIELD_PAYMENT_METHODS . '.*' => ['integer'],

            /** Condition Regions */
            'condition.' . KeyEnum::FIELD_REGIONS => [
                'array',
            ],
            'condition.' . KeyEnum::FIELD_REGIONS . '.*' => ['integer'],

            /** Condition Order Sequence Number */
            'condition.' . KeyEnum::FIELD_ORDER_SEQUENCE_NUMBER => [
                'integer',
                'gt:0',
            ],

            /** Condition Customers */
            'condition.' . KeyEnum::FIELD_CUSTOMERS => [
                'array',
            ],
            'condition.' . KeyEnum::FIELD_CUSTOMERS . '.*' => ['integer'],

            /** Condition Synergies */
            'condition.' . KeyEnum::FIELD_SYNERGY => [
                'array',
            ],
            'condition.' . KeyEnum::FIELD_SYNERGY . '.*' => ['integer'],
        ];
    }
}
