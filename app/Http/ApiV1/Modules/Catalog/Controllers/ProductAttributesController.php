<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers;

use App\Domain\Catalog\Actions\Attributes\CreateProductAttributeAction;
use App\Domain\Catalog\Actions\Attributes\DeleteProductAttributeAction;
use App\Domain\Catalog\Actions\Attributes\ReplaceProductAttributeAction;
use App\Http\ApiV1\Modules\Catalog\Queries\Attributes\ProductAttributesQuery;
use App\Http\ApiV1\Modules\Catalog\Requests\Attributes\CreateOrReplaceProductAttributeRequest;
use App\Http\ApiV1\Modules\Catalog\Resources\Attributes\ProductAttributesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ProductAttributesController
{
    public function create(
        CreateOrReplaceProductAttributeRequest $request,
        CreateProductAttributeAction $action
    ): ProductAttributesResource {
        return new ProductAttributesResource($action->execute($request->validated()));
    }

    public function replace(
        int $attributeId,
        CreateOrReplaceProductAttributeRequest $request,
        ReplaceProductAttributeAction $action
    ): ProductAttributesResource {
        return new ProductAttributesResource($action->execute($attributeId, $request->validated()));
    }

    public function delete(int $attributeId, DeleteProductAttributeAction $action): EmptyResource
    {
        $action->execute($attributeId);

        return new EmptyResource();
    }

    public function search(ProductAttributesQuery $query): AnonymousResourceCollection
    {
        return ProductAttributesResource::collectPage($query->get());
    }

    public function get(int $attributeId, ProductAttributesQuery $query): ProductAttributesResource
    {
        return new ProductAttributesResource($query->find($attributeId));
    }
}
