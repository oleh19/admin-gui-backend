<?php

namespace App\Domain\Common\Data\Meta\Enum;

use Ensi\OmsClient\Api\EnumsApi;
use Ensi\OmsClient\Dto\OrderStatusesResponse;
use GuzzleHttp\Promise\PromiseInterface;

class OrdersOrderStatusEnumInfo extends AbstractEnumInfo implements AsyncEnumInfo
{
    public function __construct(protected EnumsApi $enumsApi)
    {
    }

    public function requestAsync(): PromiseInterface
    {
        return $this->enumsApi->getOrderStatusesAsync();
    }

    /**
     * @param OrderStatusesResponse $response
     */
    public function processResponse($response)
    {
        foreach ($response->getData() as $status) {
            $this->addValue($status->getId(), $status->getName());
        }
    }
}
