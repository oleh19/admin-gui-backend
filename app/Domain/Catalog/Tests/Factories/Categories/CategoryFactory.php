<?php

namespace App\Domain\Catalog\Tests\Factories\Categories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use App\Http\ApiV1\Support\Tests\Factories\EnsiFileFactory;
use Ensi\PimClient\Dto\Category;
use Ensi\PimClient\Dto\File;

class CategoryFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->randomNumber(),

            'name' => $this->faker->text(20),
            'code' => $this->faker->word(),
            'parent_id' => $this->faker->randomNumber(),
            'file' => new File(EnsiFileFactory::new()->make()),

            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): Category
    {
        return new Category($this->makeArray($extra));
    }
}
