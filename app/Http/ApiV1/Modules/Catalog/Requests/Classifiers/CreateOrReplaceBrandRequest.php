<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Classifiers;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateOrReplaceBrandRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'code' => ['nullable', 'string'],
            'description' => ['nullable', 'string'],
        ];
    }
}
