<?php

namespace App\Domain\Orders\Tests\Factories\Orders\Data;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\OmsClient\Dto\Timeslot;

class TimeslotFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->uuid(),
            'from' => $this->faker->time('H:i'),
            'to' => $this->faker->time('H:i'),
        ];
    }

    public function make(array $extra = []): Timeslot
    {
        return new Timeslot($this->makeArray($extra));
    }
}
