<?php

namespace App\Http\ApiV1\Modules\Communication\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchStatusRequest extends BaseFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => ['sometimes', 'required', 'string'],
            'active' => ['sometimes', 'boolean'],
            'default' => ['sometimes', 'boolean'],
            'channel' => ['sometimes', 'array'],
            'channel.*' => ['sometimes', 'required', 'integer'],
        ];
    }
}
