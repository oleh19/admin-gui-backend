<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Classifiers;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\Brand;

/**
 * @mixin Brand
 */
class BrandsResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'file' => $this->fileUrl($this->getFile()),
            'name' => $this->getName(),
            'code' => $this->getCode(),
            'description' => $this->getDescription(),

            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
        ];
    }
}
