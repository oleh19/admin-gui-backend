<?php

namespace App\Http\ApiV1\Modules\Logistic\Queries\CargoOrders;

use App\Domain\Logistic\Data\CargoData;
use App\Domain\Orders\Data\Orders\ShipmentData;
use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\LogisticClient\Api\CargoOrdersApi;
use Ensi\LogisticClient\Dto\Cargo;
use Ensi\LogisticClient\Dto\RequestBodyPagination;
use Ensi\LogisticClient\Dto\SearchCargoRequest;
use Ensi\OmsClient\Api\ShipmentsApi;
use Ensi\OmsClient\Dto\PaginationTypeEnum as OmsPaginationTypeEnum;
use Ensi\OmsClient\Dto\RequestBodyPagination as OmsRequestBodyPagination;
use Ensi\OmsClient\Dto\SearchShipmentsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class CargoQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;

    protected bool $withShipments = false;

    public function __construct(protected Request $httpRequest, protected CargoOrdersApi $cargoOrdersApi, protected ShipmentsApi $shipmentsApi)
    {
        parent::__construct($httpRequest);
    }

    protected function requestGetClass(): string
    {
        return SearchCargoRequest::class;
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function search($request)
    {
        return $this->cargoOrdersApi->searchCargo($request);
    }

    protected function convertGetToItems($response)
    {
        return $this->convertArray($response->getData());
    }

    protected function convertArray(array $allCargo)
    {
        /** @var Collection|Cargo[] $allCargo */
        $allCargo = collect($allCargo);

        $shipments = $this->loadShipments($allCargo->pluck('shipment_links')->collapse()->pluck('shipment_id')->unique()->values()->all());

        $allCargoData = [];
        foreach ($allCargo as $cargo) {
            $cargoData = new CargoData($cargo);

            $shipmentLinks = $cargo->getShipmentLinks();
            if ($shipmentLinks) {
                foreach ($shipmentLinks as $cargoShipmentLink) {
                    $shipment = $shipments->get($cargoShipmentLink->getShipmentId());

                    if ($shipment) {
                        $cargoData->shipments[] = new ShipmentData($shipment);
                    }
                }
            }
            $allCargoData[] = $cargoData;
        }

        return $allCargoData;
    }

    protected function getHttpInclude(): array
    {
        $httpIncludes = parent::getHttpInclude();

        $includes = [];
        foreach ($httpIncludes as $include) {
            switch ($include) {
                case "shipments":
                    $this->withShipments();
                    $includes[] = "shipment_links";

                    break;
                default:
                    $includes[] = $include;
            }
        }

        return $includes;
    }

    public function withShipments(): self
    {
        $this->withShipments = true;

        return $this;
    }

    protected function loadShipments(array $shipmentIds)
    {
        if (!$this->withShipments || !$shipmentIds) {
            return collect();
        }

        $request = new SearchShipmentsRequest();
        $request->setFilter((object)['id' => $shipmentIds]);
        $request->setPagination((new OmsRequestBodyPagination())->setType(OmsPaginationTypeEnum::OFFSET)->setLimit(count($shipmentIds)));


        $response = $this->shipmentsApi->searchShipments($request);

        return collect($response->getData())->keyBy('id');
    }
}
