<?php

namespace App\Domain\Units\Actions\AdminUsers;

use Ensi\AdminAuthClient\Api\UsersApi as AdminUsersApi;
use Ensi\AdminAuthClient\Dto\PatchUserRequest;
use Ensi\AdminAuthClient\Dto\User as AdminUser;

class PatchAdminUsersAction
{
    public function __construct(
        protected AdminUsersApi $adminUsersApi,
    ) {
    }

    public function execute(int $id, array $data): AdminUser
    {
        $request = new PatchUserRequest($data);

        return $this->adminUsersApi->patchUser($id, $request)->getData();
    }
}
