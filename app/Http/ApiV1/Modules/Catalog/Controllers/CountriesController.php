<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers;

use App\Domain\Catalog\Actions\Classifiers\Countries\CreateCountryAction;
use App\Domain\Catalog\Actions\Classifiers\Countries\DeleteCountryAction;
use App\Domain\Catalog\Actions\Classifiers\Countries\ReplaceCountryAction;
use App\Http\ApiV1\Modules\Catalog\Queries\Classifiers\CountriesQuery;
use App\Http\ApiV1\Modules\Catalog\Requests\Classifiers\CreateOrReplaceCountryRequest;
use App\Http\ApiV1\Modules\Catalog\Resources\Classifiers\CountriesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CountriesController
{
    public function search(CountriesQuery $query): AnonymousResourceCollection
    {
        return CountriesResource::collectPage($query->get());
    }

    public function create(
        CreateOrReplaceCountryRequest $request,
        CreateCountryAction $action
    ): CountriesResource {
        return new CountriesResource($action->execute($request->validated()));
    }

    public function get(
        int $countryId,
        CountriesQuery $query
    ): CountriesResource {
        return new CountriesResource($query->find($countryId));
    }

    public function replace(
        int $countryId,
        CreateOrReplaceCountryRequest $request,
        ReplaceCountryAction $action
    ): CountriesResource {
        return new CountriesResource($action->execute($countryId, $request->validated()));
    }

    public function delete(
        int $countryId,
        DeleteCountryAction $action
    ): EmptyResource {
        $action->execute($countryId);

        return new EmptyResource();
    }
}
