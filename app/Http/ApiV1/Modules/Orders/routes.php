<?php

use App\Http\ApiV1\Modules\Orders\Controllers;
use App\Http\ApiV1\Modules\Orders\Controllers\Orders\DeliveriesController;
use App\Http\ApiV1\Modules\Orders\Controllers\Orders\OrdersController;
use App\Http\ApiV1\Modules\Orders\Controllers\Refunds\RefundReasonsController;
use App\Http\ApiV1\Modules\Orders\Controllers\Refunds\RefundsController;
use Illuminate\Support\Facades\Route;

Route::post('orders:search', [OrdersController::class, 'search']);
Route::get('orders:meta', [OrdersController::class, 'meta']);
Route::get('orders/{id}', [OrdersController::class, 'get']);
Route::patch('orders/{id}', [OrdersController::class, 'patch']);
Route::post('orders/{id}:change-payment-method', [OrdersController::class, 'changePaymentSystem']);
Route::post('orders/{id}:change-delivery', [OrdersController::class, 'changeDelivery']);
Route::post('orders/{id}:attach-file', [OrdersController::class, 'attachFile']);
Route::delete('orders/{id}:delete-files', [OrdersController::class, 'deleteFiles']);

Route::post('refunds', [RefundsController::class, 'create']);
Route::post('refunds:search', [RefundsController::class, 'search']);
Route::get('refunds/{id}', [RefundsController::class, 'get']);
Route::patch('refunds/{id}', [RefundsController::class, 'patch']);
Route::post('refunds/{id}:attach-file', [RefundsController::class, 'attachFile']);
Route::delete('refunds/{id}:delete-files', [RefundsController::class, 'deleteFiles']);

Route::post('refund-reasons', [RefundReasonsController::class, 'create']);
Route::patch('refund-reasons/{id}', [RefundReasonsController::class, 'patch']);

Route::patch('deliveries/{id}', [DeliveriesController::class, 'patch']);

Route::get('oms-settings', [Controllers\OmsCommon\SettingsController::class, 'search']);
Route::patch('oms-settings', [Controllers\OmsCommon\SettingsController::class, 'patchSeveral']);
Route::get('baskets-settings', [Controllers\BasketsCommon\SettingsController::class, 'search']);
Route::patch('baskets-settings', [Controllers\BasketsCommon\SettingsController::class, 'patchSeveral']);

Route::get('order-statuses', [Controllers\Orders\EnumsController::class, 'orderStatuses']);
Route::get('order-sources', [Controllers\Orders\EnumsController::class, 'orderSources']);
Route::get('payment-methods', [Controllers\Orders\EnumsController::class, 'paymentMethods']);
Route::get('payment-statuses', [Controllers\Orders\EnumsController::class, 'paymentStatuses']);
Route::get('delivery-statuses', [Controllers\Orders\EnumsController::class, 'deliveryStatuses']);
Route::get('shipment-statuses', [Controllers\Orders\EnumsController::class, 'shipmentStatuses']);
Route::get('refund-statuses', [Controllers\Refunds\EnumsController::class, 'refundStatuses']);
Route::get('refund-reasons', [Controllers\Refunds\EnumsController::class, 'refundReasons']);
