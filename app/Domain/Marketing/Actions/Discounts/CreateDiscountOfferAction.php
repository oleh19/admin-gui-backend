<?php

namespace App\Domain\Marketing\Actions\Discounts;

use Ensi\MarketingClient\Api\DiscountOffersApi;
use Ensi\MarketingClient\Dto\DiscountOffer;
use Ensi\MarketingClient\Dto\DiscountOfferRaw;

class CreateDiscountOfferAction
{
    public function __construct(protected DiscountOffersApi $discountOffersApi)
    {
    }

    public function execute(array $data): DiscountOffer
    {
        $requestDiscountOffersApi = new DiscountOfferRaw($data);
        $responseDiscountOffersApi = $this->discountOffersApi->createDiscountOffer($requestDiscountOffersApi);

        return $responseDiscountOffersApi->getData();
    }
}
