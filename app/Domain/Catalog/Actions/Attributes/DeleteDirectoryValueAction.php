<?php

namespace App\Domain\Catalog\Actions\Attributes;

use Ensi\PimClient\Api\PropertiesApi;

class DeleteDirectoryValueAction
{
    public function __construct(private PropertiesApi $api)
    {
    }

    public function execute(int $directoryValueId): void
    {
        $this->api->deleteDirectoryValue($directoryValueId);
    }
}
