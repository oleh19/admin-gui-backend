<?php

namespace App\Http\ApiV1\Modules\Communication\Queries;

use Ensi\CommunicationManagerClient\Api\TypesApi;
use Ensi\CommunicationManagerClient\ApiException;
use Ensi\CommunicationManagerClient\Dto\SearchTypesRequest;
use Ensi\CommunicationManagerClient\Dto\SearchTypesResponse;
use Illuminate\Http\Request;

class TypesQuery extends CommunicationQuery
{
    public function __construct(Request $request, private TypesApi $api)
    {
        parent::__construct($request, SearchTypesRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchTypesResponse
    {
        return $this->api->searchTypes($request);
    }
}
