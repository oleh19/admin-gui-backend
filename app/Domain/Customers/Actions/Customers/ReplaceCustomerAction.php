<?php

namespace App\Domain\Customers\Actions\Customers;

use App\Domain\Customers\Data\CustomerData;
use Ensi\CustomersClient\Dto\CustomerForReplace;

/**
 * Class ReplaceCustomerAction
 * @package App\Domain\Customers\Actions\Customers
 */
class ReplaceCustomerAction extends CustomerAction
{
    /**
     * @param int $customerId
     * @param array $fields
     * @return CustomerData
     * @throws \Ensi\CustomerAuthClient\ApiException
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function execute(int $customerId, array $fields): CustomerData
    {
        return $this->prepareCustomer($this->customersApi->replaceCustomer(
            $customerId,
            new CustomerForReplace($fields)
        )->getData());
    }
}
