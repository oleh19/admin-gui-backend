<?php

namespace App\Domain\Marketing\Actions\Discounts;

use Ensi\MarketingClient\Api\DiscountConditionsApi;
use Ensi\MarketingClient\Dto\DiscountCondition;
use Ensi\MarketingClient\Dto\DiscountConditionRaw;

class CreateDiscountConditionAction
{
    public function __construct(protected DiscountConditionsApi $discountConditionsApi)
    {
    }

    public function execute(array $data): DiscountCondition
    {
        $requestDiscountConditionsApi = new DiscountConditionRaw($data);
        $responseDiscountConditionsApi = $this->discountConditionsApi->createDiscountCondition($requestDiscountConditionsApi);

        return $responseDiscountConditionsApi->getData();
    }
}
