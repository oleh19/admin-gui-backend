<?php

namespace App\Domain\Catalog\Actions\Products;

use Ensi\PimClient\Api\ProductsApi;

class DeleteProductAction
{
    public function __construct(private ProductsApi $api)
    {
    }

    public function execute(int $productId): void
    {
        $this->api->deleteProduct($productId);
    }
}
