<?php


namespace App\Domain\Logistic\Actions\Geos;

use Ensi\LogisticClient\Api\GeosApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\Region;
use Ensi\LogisticClient\Dto\ReplaceRegionRequest;

/**
 * Class ReplaceRegionAction
 * @package App\Domain\Logistic\Actions\Geos
 */
class ReplaceRegionAction
{
    protected GeosApi $geosApi;

    /**
     * PatchRegionAction constructor.
     * @param  GeosApi  $geosApi
     */
    public function __construct(GeosApi $geosApi)
    {
        $this->geosApi = $geosApi;
    }

    /**
     * @param  int  $regionId
     * @param  array  $fields
     * @return Region
     * @throws ApiException
     */
    public function execute(int $regionId, array $fields): Region
    {
        $request = new ReplaceRegionRequest();

        $request->setFederalDistrictId($fields['federal_district_id']);
        $request->setName($fields['name']);
        $request->setGuid($fields['guid']);

        return $this->geosApi->replaceRegion($regionId, $request)->getData();
    }
}
