<?php


namespace App\Http\ApiV1\Modules\Marketing\Controllers;

use App\Domain\Marketing\Actions\PromoCodes\CreatePromoCodeAction;
use App\Domain\Marketing\Actions\PromoCodes\DeletePromoCodeAction;
use App\Domain\Marketing\Actions\PromoCodes\PatchPromoCodeAction;
use App\Domain\Marketing\Actions\PromoCodes\ReplacePromoCodeAction;
use App\Http\ApiV1\Modules\Marketing\Queries\PromoCodesQuery;
use App\Http\ApiV1\Modules\Marketing\Requests\CreateOrReplacePromoCodeRequest;
use App\Http\ApiV1\Modules\Marketing\Requests\PatchPromoCodeRequest;
use App\Http\ApiV1\Modules\Marketing\Resources\PromoCodesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class PromoCodesController
{
    public function findById(int $promoCodeId, PromoCodesQuery $query)
    {
        return new PromoCodesResource($query->find($promoCodeId));
    }

    public function search(PromoCodesQuery $query)
    {
        return PromoCodesResource::collectPage(
            $query->get()
        );
    }

    public function create(CreateOrReplacePromoCodeRequest $request, CreatePromoCodeAction $action): PromoCodesResource
    {
        return new PromoCodesResource($action->execute($request->validated()));
    }

    public function replace(int $promoCodeId, CreateOrReplacePromoCodeRequest $request, ReplacePromoCodeAction $action)
    {
        return new PromoCodesResource($action->execute($promoCodeId, $request->validated()));
    }

    public function patch(int $promoCodeId, PatchPromoCodeRequest $request, PatchPromoCodeAction $action)
    {
        return new PromoCodesResource($action->execute($promoCodeId, $request->validated()));
    }

    public function delete(int $promoCodeId, DeletePromoCodeAction $action)
    {
        $action->execute($promoCodeId);

        return new EmptyResource();
    }
}
