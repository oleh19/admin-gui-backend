<?php

namespace App\Http\ApiV1\Modules\Cms\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/** @mixin \Ensi\CmsClient\Dto\ProductGroup */
class ProductGroupsResource extends BaseJsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'code' => $this->getCode(),
            'active' => $this->getActive(),
            'is_shown' => $this->getIsShown(),
            'preview_photo' => $this->fileUrl($this->getPreviewPhoto()),
            'type_id' => $this->getTypeId(),
            'banner_id' => $this->getBannerId(),
            'category_code' => $this->getCategoryCode(),
            'filters' => ProductGroupFilterLinksResource::collection($this->whenNotNull($this->getFilters())),
            'products' => ProductGroupProductsResource::collection($this->whenNotNull($this->getProducts())),
            'type' => ProductGroupTypesResource::make($this->whenNotNull($this->getType())),
            'banner' => BannersResource::make($this->whenNotNull($this->getBanner())),
        ];
    }
}
