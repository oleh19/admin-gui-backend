<?php

namespace App\Http\ApiV1\Modules\Units\Requests\Stores;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateOrReplaceStorePickupTimeRequest extends BaseFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'store_id' => ['required', 'integer'],
            'day' => ['required', 'integer'],
            'pickup_time_code' => ['nullable', 'string'],
            'pickup_time_start' => ['nullable', 'string'],
            'pickup_time_end' => ['nullable', 'string'],
            'cargo_export_time' => ['nullable', 'string'],
            'delivery_service' => ['nullable', 'integer'],
        ];
    }
}
