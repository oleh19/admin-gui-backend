<?php

namespace App\Domain\Catalog\Actions\Attributes;

use Ensi\PimClient\Api\PropertiesApi;
use Ensi\PimClient\Dto\CreatePropertyRequest;
use Ensi\PimClient\Dto\Property;

class CreateProductAttributeAction
{
    public function __construct(private PropertiesApi $api)
    {
    }

    public function execute(array $fields): Property
    {
        $request = new CreatePropertyRequest($fields);

        return $this->api->createProperty($request)->getData();
    }
}
