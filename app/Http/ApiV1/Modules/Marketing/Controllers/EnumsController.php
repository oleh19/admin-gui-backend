<?php


namespace App\Http\ApiV1\Modules\Marketing\Controllers;

use App\Http\ApiV1\Modules\Marketing\Resources\DiscountConditionTypePropsResource;
use App\Http\ApiV1\Modules\Marketing\Resources\DiscountConditionTypesResource;
use App\Http\ApiV1\Modules\Marketing\Resources\DiscountStatusesResource;
use App\Http\ApiV1\Modules\Marketing\Resources\DiscountTypesResource;
use App\Http\ApiV1\Modules\Marketing\Resources\PromoCodeStatusesResource;
use App\Http\ApiV1\Modules\Marketing\Resources\PromoCodeTypesResource;

use Ensi\MarketingClient\Api\EnumsApi;

class EnumsController
{
    public function discountStatuses(EnumsApi $enumsApi)
    {
        return DiscountStatusesResource::collection($enumsApi->getDiscountStatuses()->getData());
    }

    public function discountTypes(EnumsApi $enumsApi)
    {
        return DiscountTypesResource::collection($enumsApi->getDiscountTypes()->getData());
    }

    public function discountConditionTypes(EnumsApi $enumsApi)
    {
        return DiscountConditionTypesResource::collection($enumsApi->getDiscountConditionTypes()->getData());
    }

    public function discountConditionTypeProps(EnumsApi $enumsApi)
    {
        return DiscountConditionTypePropsResource::collection($enumsApi->getDiscountConditionTypeProps()->getData());
    }

    public function promoCodeStatuses(EnumsApi $enumsApi)
    {
        return PromoCodeStatusesResource::collection($enumsApi->getPromoCodeStatuses()->getData());
    }

    public function promoCodeTypes(EnumsApi $enumsApi)
    {
        return PromoCodeTypesResource::collection($enumsApi->getPromoCodeTypes()->getData());
    }
}
