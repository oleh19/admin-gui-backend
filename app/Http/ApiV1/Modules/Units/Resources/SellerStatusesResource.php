<?php

namespace App\Http\ApiV1\Modules\Units\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\BuClient\Dto\SellerStatus;

/**
 * Class SellerStatusesResource
 * @package App\Http\ApiV1\Modules\Units\Resources
 * @mixin SellerStatus
 */
class SellerStatusesResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
        ];
    }
}
