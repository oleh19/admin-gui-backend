<?php

namespace App\Http\ApiV1\Modules\Units\Requests\AdminUsers;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class MassChangeActiveRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'user_ids' => ['required', 'array'],
            'user_ids.*' => ['integer'],
            'active' => ['required', 'boolean'],
            'cause_deactivation' => ['required_if:active,false', 'string'],
        ];
    }
}
