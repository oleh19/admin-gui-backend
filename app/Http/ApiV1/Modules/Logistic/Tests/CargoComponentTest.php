<?php

use App\Domain\Logistic\Tests\Factories\CargoFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LogisticClient\Dto\CargoStatusEnum;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

// cargo-orders/cargo{id}
test("PATCH /api/v1/cargo-orders/cargo/{id} success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $cargoId = 1;
    $cargoData = CargoFactory::new()->only(['shipping_problem_comment'])->make();

    $this->mockLogisticCargoOrdersApi()->allows([
        'patchCargo' => CargoFactory::new()->makeResponseOne($cargoData),
    ]);

    patchJson("/api/v1/logistic/cargo/{$cargoId}", $cargoData)
        ->assertStatus(200)
        ->assertJsonPath('data.shipping_problem_comment', $cargoData['shipping_problem_comment']);
});

// cargo-orders/cargo{id}:cancel
test("POST /api/v1/logistic/cargo/{id}:cancel success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $cargoId = 1;
    $this->mockLogisticCargoOrdersApi()->allows([
        'cancelCargo' => CargoFactory::new()->makeResponseOne(['is_canceled' => true]),
    ]);

    postJson("/api/v1/logistic/cargo/{$cargoId}:cancel")
        ->assertStatus(200)
        ->assertJsonPath('data.is_canceled', true);
});

// cargo-orders/cargo:search
test("POST /api/v1/logistic/cargo:search success", function () {
    $cargoId = 1;
    $cargoStatus = CargoStatusEnum::SHIPPED;

    $this->mockLogisticCargoOrdersApi()->allows([
        'searchCargo' => CargoFactory::new()->makeResponse(['id' => $cargoId, 'status' => $cargoStatus]),
    ]);

    postJson("/api/v1/logistic/cargo:search")
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $cargoId)
        ->assertJsonPath('data.0.status', $cargoStatus);
});
