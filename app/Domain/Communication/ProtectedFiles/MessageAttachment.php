<?php


namespace App\Domain\Communication\ProtectedFiles;

use App\Domain\Common\ProtectedFiles\ProtectedFile;
use Ensi\InternalMessenger\Dto\File;
use Ensi\InternalMessenger\Dto\Message;

class MessageAttachment extends ProtectedFile
{
    public static function entity(): string
    {
        return 'internal-messenger/message';
    }

    public static function createFromModel(Message $message, File $file): static
    {
        $object = new static();
        $object->entity_id = $message->getId();
        $object->file = $file->getPath();

        return $object;
    }
}
