<?php

namespace App\Domain\Common\Data\Meta\Enum;

use Ensi\OmsClient\Api\EnumsApi;
use Ensi\OmsClient\Dto\OrderSourcesResponse;
use GuzzleHttp\Promise\PromiseInterface;

class OrdersOrderSourceEnumInfo extends AbstractEnumInfo implements AsyncEnumInfo
{
    public function __construct(protected EnumsApi $enumsApi)
    {
    }

    public function requestAsync(): PromiseInterface
    {
        return $this->enumsApi->getOrderSourcesAsync();
    }

    /**
     * @param OrderSourcesResponse $response
     */
    public function processResponse($response)
    {
        foreach ($response->getData() as $source) {
            $this->addValue($source->getId(), $source->getName());
        }
    }
}
