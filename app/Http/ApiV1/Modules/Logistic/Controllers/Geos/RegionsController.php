<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\Geos;

use App\Domain\Logistic\Actions\Geos\CreateRegionAction;
use App\Domain\Logistic\Actions\Geos\DeleteRegionAction;
use App\Domain\Logistic\Actions\Geos\PatchRegionAction;
use App\Domain\Logistic\Actions\Geos\ReplaceRegionAction;
use App\Http\ApiV1\Modules\Logistic\Queries\Geos\RegionsQuery;
use App\Http\ApiV1\Modules\Logistic\Requests\Geos\CreateOrReplaceRegionRequest;
use App\Http\ApiV1\Modules\Logistic\Requests\Geos\PatchRegionRequest;
use App\Http\ApiV1\Modules\Logistic\Resources\Geos\RegionsResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Ensi\LogisticClient\ApiException;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class RegionsController
 * @package App\Http\ApiV1\Modules\Logistic\Controllers\Geos
 */
class RegionsController
{
    /**
     * @param  CreateOrReplaceRegionRequest  $request
     * @param  CreateRegionAction  $action
     * @return RegionsResource
     * @throws ApiException
     */
    public function create(CreateOrReplaceRegionRequest $request, CreateRegionAction $action): RegionsResource
    {
        return new RegionsResource($action->execute($request->validated()));
    }

    /**
     * @param  int  $regionId
     * @param  CreateOrReplaceRegionRequest  $request
     * @param  ReplaceRegionAction  $action
     * @return RegionsResource
     * @throws ApiException
     */
    public function replace(
        int $regionId,
        CreateOrReplaceRegionRequest $request,
        ReplaceRegionAction $action
    ): RegionsResource {
        return new RegionsResource($action->execute($regionId, $request->validated()));
    }

    /**
     * @param  int  $regionId
     * @param  PatchRegionRequest  $request
     * @param  PatchRegionAction  $action
     * @return RegionsResource
     * @throws ApiException
     */
    public function patch(int $regionId, PatchRegionRequest $request, PatchRegionAction $action): RegionsResource
    {
        return new RegionsResource($action->execute($regionId, $request->validated()));
    }

    /**
     * @param  int  $regionId
     * @param  DeleteRegionAction  $action
     * @return EmptyResource
     * @throws ApiException
     */
    public function delete(int $regionId, DeleteRegionAction $action): EmptyResource
    {
        $action->execute($regionId);

        return new EmptyResource();
    }

    /**
     * @param  int  $federalDistrictId
     * @param  RegionsQuery  $query
     * @return RegionsResource
     */
    public function get(
        int $federalDistrictId,
        RegionsQuery $query
    ): RegionsResource {
        return new RegionsResource($query->find($federalDistrictId));
    }

    /**
     * @param  RegionsQuery  $query
     * @return AnonymousResourceCollection
     */
    public function search(RegionsQuery $query): AnonymousResourceCollection
    {
        return RegionsResource::collectPage(
            $query->get()
        );
    }

    /**
     * @param  RegionsQuery  $query
     * @return RegionsResource
     */
    public function searchOne(RegionsQuery $query): RegionsResource
    {
        return new RegionsResource($query->first());
    }
}
