<?php

namespace App\Http\ApiV1\Modules\Auth\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class RefreshRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'refresh_token' => ['required'],
        ];
    }
}
