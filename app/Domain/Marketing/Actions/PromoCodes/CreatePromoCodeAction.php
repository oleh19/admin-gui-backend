<?php

namespace App\Domain\Marketing\Actions\PromoCodes;

use App\Domain\Auth\Models\User;
use Ensi\MarketingClient\Api\PromoCodesApi;
use Ensi\MarketingClient\Dto\PromoCode;
use Ensi\MarketingClient\Dto\PromoCodeRequest;

class CreatePromoCodeAction
{
    public function __construct(protected PromoCodesApi $promoCodesApi)
    {
    }

    public function execute(array $data): PromoCode
    {
        /** @var User $user */
        $user = auth()->user();

        $data['creator_id'] = $user->getId();
        $data['seller_id'] = null;

        $requestPromoCodesApi = new PromoCodeRequest($data);
        $responsePromoCodesApi = $this->promoCodesApi->createPromoCode($requestPromoCodesApi);

        return $responsePromoCodesApi->getData();
    }
}
