<?php

namespace App\Domain\Catalog\Actions\Classifiers\ProductTypes;

use Ensi\PimClient\Api\ProductTypesApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\ProductType;
use Ensi\PimClient\Dto\ReplaceProductTypeRequest;

class ReplaceProductTypeAction
{
    public function __construct(
        private ProductTypesApi $productTypesApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $countryId, array $fields): ProductType
    {
        $request = new ReplaceProductTypeRequest($fields);

        return $this->productTypesApi->updateProductType($countryId, $request)->getData();
    }
}
