<?php

namespace App\Http\ApiV1\Modules\Customers\Requests\Customers;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

/**
 * Class UploadAvatarRequest
 * @package App\Http\ApiV1\Modules\Customers\Requests\Customers
 */
class UploadAvatarRequest extends BaseFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'file' => ['required', 'image', 'max:2048'],
        ];
    }
}
