<?php

namespace App\Domain\Common\Data\Meta\Fields;

use App\Http\ApiV1\OpenApiGenerated\Dto\FieldTypeEnum;

class PriceField extends AbstractField
{
    protected function type(): string
    {
        return FieldTypeEnum::PRICE;
    }

    protected function init()
    {
        $this->sort()->filterRange();
    }
}
