<?php

namespace App\Domain\Orders\Tests\Factories\Orders;

use App\Domain\Orders\Tests\Factories\Orders\Data\TimeslotFactory;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\OmsClient\Dto\Delivery;
use Ensi\OmsClient\Dto\DeliveryResponse;
use Ensi\OmsClient\Dto\DeliveryStatusEnum;
use Ensi\OmsClient\Dto\SearchDeliveriesResponse;
use Ensi\OmsClient\Dto\Shipment;

class DeliveryFactory extends BaseApiFactory
{
    protected array $shipments = [];

    protected function definition(): array
    {
        $definition = [
            'id' => $this->faker->randomNumber(),
            'order_id' => $this->faker->randomNumber(),
            'status' => $this->faker->randomElement(DeliveryStatusEnum::getAllowableEnumValues()),
            'status_at' => $this->faker->dateTime(),
            'number' => $this->faker->unique()->numerify('######-#'),
            'cost' => $this->faker->numberBetween(0, 1000),
            'width' => $this->faker->randomFloat(),
            'height' => $this->faker->randomFloat(),
            'length' => $this->faker->randomFloat(),
            'weight' => $this->faker->randomFloat(),
            'date' => $this->faker->dateTime(),
            'timeslot' => $this->faker->boolean() ? TimeslotFactory::new()->make() : null,
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];

        if ($this->shipments) {
            $definition['shipments'] = $this->shipments;
        }

        return $definition;
    }

    public function withShipment(?Shipment $shipment = null): self
    {
        $this->shipments[] = $shipment ?: ShipmentFactory::new()->make();

        return $this;
    }

    public function make(array $extra = []): Delivery
    {
        return new Delivery($this->makeArray($extra));
    }

    public function makeResponseOne(array $extra = []): DeliveryResponse
    {
        return new DeliveryResponse([
            'data' => $this->make($this->makeArray($extra)),
        ]);
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchDeliveriesResponse
    {
        return $this->generateResponseSearch(SearchDeliveriesResponse::class, $extra, $count);
    }
}
