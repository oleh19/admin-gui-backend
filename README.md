# Admin GUI Backend

## Резюме

Название: Admin GUI Backend  
Домен: Admin GUI  
Назначение: Backend для административного интерфейса  

## Разработка сервиса

Инструкцию описывающую разворот, запуск и тестирование сервиса на локальной машине можно найти в отдельном документе в [Confluence](https://greensight.atlassian.net/wiki/spaces/ENSI/pages/362676232/Backend-)

Регламент работы над задачами тоже находится в [Confluence](https://greensight.atlassian.net/wiki/spaces/ENSI/pages/477528081)

### Настройка авторизации

Для настройки авторизации пользователей в .env файл необходимо добавить переменные *UNITS_ADMIN_AUTH_SERVICE_CLIENT_ID* и *UNITS_ADMIN_AUTH_SERVICE_CLIENT_SECRET*. Значения данных переменных могут быть получены:
1. Из консоли при создании клиента в соответствующем *-auth сервисе (подробнее про создание клиента см. readme соответствующего *-auth сервиса)
   
2. Если клиент уже сгенерирован, то в таблице *oauth_clients* в соответствующем *-auth сервисе

## Структура сервиса

Почитать про структуру сервиса можно здесь [здесь](docs/structure.md)

## Среды

### Test

CI: https://jenkins-infra.ensi.tech/job/ensi-stage-1/job/admin-gui/job/admin-gui-backend/  
URL: https://admin-gui-backend-master-dev.ensi.tech/docs/swagger  

### Preprod

Отсутствует

### Prod

Отсутствует

## Контакты

Команда поддерживающая данный сервис: https://gitlab.com/groups/greensight/ensi/-/group_members  
Email для связи: mail@greensight.ru

## Лицензия

[Открытая лицензия на право использования программы для ЭВМ Greensight Ecom Platform (GEP)](LICENSE.md).
