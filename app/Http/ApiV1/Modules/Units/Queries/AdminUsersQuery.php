<?php


namespace App\Http\ApiV1\Modules\Units\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFilterEnumTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use Ensi\AdminAuthClient\Api\UsersApi as AdminUsersApi;
use Ensi\AdminAuthClient\Dto\RequestBodyPagination;
use Ensi\AdminAuthClient\Dto\SearchUsersRequest as SearchAdminUsersRequest;
use Illuminate\Http\Request;

class AdminUsersQuery extends QueryBuilder
{
    use QueryBuilderFilterEnumTrait;
    use QueryBuilderFindTrait;

    public function __construct(
        protected Request $httpRequest,
        protected AdminUsersApi $adminUsersApi,
    ) {
        parent::__construct($httpRequest);
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function requestGetClass(): string
    {
        return SearchAdminUsersRequest::class;
    }

    protected function search($request)
    {
        return $this->adminUsersApi->searchUsers($request);
    }

    protected function searchById($id)
    {
        return $this->adminUsersApi->getUser($id, $this->getInclude());
    }

    /**
     * @param SearchAdminUsersRequest $request
     * @param null|array $id
     * @param null|string $query
     */
    protected function prepareEnumRequest($request, ?array $id, ?string $query)
    {
        $filter = [];
        if ($id) {
            $filter['id'] = $id;
        }
        if ($query) {
            $filter['full_name'] = $query;
        }

        $request->setFilter((object)$filter);
    }
}
