<?php

namespace App\Domain\Auth\Actions;

use Ensi\AdminAuthClient\Api\OauthApi;
use Lcobucci\JWT\Encoding\JoseEncoder;
use Lcobucci\JWT\Token\Parser;

class LogoutAction
{
    public function __construct(private OauthApi $oauthApi)
    {
    }

    public function execute(string $token): void
    {
        $tokenId = (new Parser(new JoseEncoder()))->parse($token)->claims()->all()['jti'];
        $this->oauthApi->getConfig()->setAccessToken($token);
        $this->oauthApi->deleteToken($tokenId);
    }
}
