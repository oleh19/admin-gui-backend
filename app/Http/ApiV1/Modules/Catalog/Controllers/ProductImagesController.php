<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers;

use App\Domain\Catalog\Actions\Products\CreateProductImageAction;
use App\Domain\Catalog\Actions\Products\DeleteProductImageAction;
use App\Domain\Catalog\Actions\Products\PatchProductImageAction;
use App\Domain\Catalog\Actions\Products\UploadProductImageAction;
use App\Http\ApiV1\Modules\Catalog\Queries\Products\ProductImagesQuery;
use App\Http\ApiV1\Modules\Catalog\Requests\Products\CreateProductImageRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Products\PatchProductImageRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Products\UploadProductImageRequest;
use App\Http\ApiV1\Modules\Catalog\Resources\Products\ProductImagesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ProductImagesController
{
    public function create(CreateProductImageRequest $request, CreateProductImageAction $action): ProductImagesResource
    {
        return new ProductImagesResource($action->execute($request->validated()));
    }

    public function upload(
        int $imageId,
        UploadProductImageRequest $request,
        UploadProductImageAction $action
    ): ProductImagesResource {
        return new ProductImagesResource($action->execute($imageId, $request->file('file')));
    }

    public function patch(
        int $imageId,
        PatchProductImageRequest $request,
        PatchProductImageAction $action
    ): ProductImagesResource {
        return new ProductImagesResource($action->execute($imageId, $request->validated()));
    }

    public function delete(int $imageId, DeleteProductImageAction $action): EmptyResource
    {
        $action->execute($imageId);

        return new EmptyResource();
    }

    public function search(ProductImagesQuery $query): AnonymousResourceCollection
    {
        return ProductImagesResource::collectPage($query->get());
    }

    public function get(int $imageId, ProductImagesQuery $query): ProductImagesResource
    {
        return new ProductImagesResource($query->find($imageId));
    }
}
