<?php

namespace App\Domain\Catalog\Actions\Classifiers\ProductTypes;

use Ensi\PimClient\Api\ProductTypesApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\CreateProductTypeRequest;
use Ensi\PimClient\Dto\ProductType;

class CreateProductTypeAction
{
    public function __construct(
        private ProductTypesApi $productTypesApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): ProductType
    {
        $request = new CreateProductTypeRequest($fields);

        return $this->productTypesApi->createProductType($request)->getData();
    }
}
