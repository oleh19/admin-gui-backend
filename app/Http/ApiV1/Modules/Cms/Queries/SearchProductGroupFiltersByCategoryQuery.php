<?php

namespace App\Http\ApiV1\Modules\Cms\Queries;

use App\Http\ApiV1\Support\Pagination\Page;
use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFilterTrait;
use Ensi\PimClient\Api\CategoriesApi;
use Ensi\PimClient\Dto\SearchCategoryPropertiesRequest;
use Illuminate\Http\Request;

class SearchProductGroupFiltersByCategoryQuery extends QueryBuilder
{
    use QueryBuilderFilterTrait;

    public function __construct(
        Request $httpRequest,
        protected CategoriesApi $api,
    ) {
        parent::__construct($httpRequest);
    }

    protected function requestGetClass(): string
    {
        return SearchCategoryPropertiesRequest::class;
    }

    protected function search($request)
    {
        $id = $this->httpRequest->get('category');

        return $this->api->getCategoryProperties($id, $request);
    }

    protected function convertGetToItems($response)
    {
        return $response->getData();
    }

    public function get(): Page
    {
        $requestClass = $this->requestGetClass();
        $request = new $requestClass();

        $this->fillFilters($request);
        $this->fillInclude($request);

        $response = $this->search($request);

        $items = $this->convertGetToItems($response);

        return new Page($items, []);
    }
}
