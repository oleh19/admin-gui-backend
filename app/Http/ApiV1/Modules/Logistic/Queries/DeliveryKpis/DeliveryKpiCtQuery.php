<?php


namespace App\Http\ApiV1\Modules\Logistic\Queries\DeliveryKpis;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderFirstTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\LogisticClient\Api\KpiApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\DeliveryKpiCtResponse;
use Ensi\LogisticClient\Dto\RequestBodyPagination;
use Ensi\LogisticClient\Dto\SearchDeliveryKpiCtRequest;
use Ensi\LogisticClient\Dto\SearchDeliveryKpiCtResponse;
use Illuminate\Http\Request;

/**
 * Class DeliveryKpiCtQuery
 * @package App\Http\ApiV1\Modules\Logistic\Queries\DeliveryKpis
 */
class DeliveryKpiCtQuery extends QueryBuilder
{
    use QueryBuilderFindTrait;
    use QueryBuilderFirstTrait;
    use QueryBuilderGetTrait;

    /**
     * DeliveryKpiCtQuery constructor.
     * @param Request $httpRequest
     * @param KpiApi $kpiApi
     */
    public function __construct(protected Request $httpRequest, protected KpiApi $kpiApi)
    {
        parent::__construct($httpRequest);
    }

    /**
     * @return string
     */
    protected function requestFirstClass(): string
    {
        return SearchDeliveryKpiCtRequest::class;
    }

    /**
     * @return string
     */
    protected function requestGetClass(): string
    {
        return SearchDeliveryKpiCtRequest::class;
    }

    /**
     * @return string
     */
    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    /**
     * @param $id
     * @return DeliveryKpiCtResponse
     * @throws ApiException
     */
    public function searchById($id): DeliveryKpiCtResponse
    {
        return $this->kpiApi->getDeliveryKpiCt($id);
    }

    /**
     * @param $request
     * @return SearchDeliveryKpiCtResponse
     * @throws ApiException
     */
    public function search($request): SearchDeliveryKpiCtResponse
    {
        return $this->kpiApi->searchDeliveryKpiCts($request);
    }

    /**
     * @param $request
     * @return DeliveryKpiCtResponse
     * @throws ApiException
     */
    public function searchOne($request): DeliveryKpiCtResponse
    {
        return $this->kpiApi->searchOneDeliveryKpiCt($request);
    }
}
