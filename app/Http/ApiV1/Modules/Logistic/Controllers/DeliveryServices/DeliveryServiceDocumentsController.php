<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryServices;

use App\Domain\Logistic\Actions\DeliveryServiceDocument\CreateDeliveryServiceDocumentAction;
use App\Domain\Logistic\Actions\DeliveryServiceDocument\DeleteDeliveryServiceDocumentAction;
use App\Domain\Logistic\Actions\DeliveryServiceDocument\PatchDeliveryServiceDocumentAction;
use App\Domain\Logistic\Actions\DeliveryServiceDocument\ReplaceDeliveryServiceDocumentAction;
use App\Http\ApiV1\Modules\Logistic\Queries\DeliveryServices\DeliveryServiceDocumentsQuery;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryServices\CreateOrReplaceDeliveryServiceDocumentRequest;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryServices\PatchDeliveryServiceDocumentRequest;
use App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices\DeliveryServiceDocumentsResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class DeliveryServiceDocumentsController
{
    public function create(CreateOrReplaceDeliveryServiceDocumentRequest $request, CreateDeliveryServiceDocumentAction $action): DeliveryServiceDocumentsResource
    {
        return new DeliveryServiceDocumentsResource($action->execute($request->validated()));
    }

    public function replace(
        int $federalDistrictId,
        CreateOrReplaceDeliveryServiceDocumentRequest $request,
        ReplaceDeliveryServiceDocumentAction $action
    ): DeliveryServiceDocumentsResource {
        return new DeliveryServiceDocumentsResource($action->execute($federalDistrictId, $request->validated()));
    }

    public function patch(int $federalDistrictId, PatchDeliveryServiceDocumentRequest $request, PatchDeliveryServiceDocumentAction $action): DeliveryServiceDocumentsResource
    {
        return new DeliveryServiceDocumentsResource($action->execute($federalDistrictId, $request->validated()));
    }

    public function delete(int $federalDistrictId, DeleteDeliveryServiceDocumentAction $action): EmptyResource
    {
        $action->execute($federalDistrictId);

        return new EmptyResource();
    }

    public function get(
        int $federalDistrictId,
        DeliveryServiceDocumentsQuery $query
    ): DeliveryServiceDocumentsResource {
        return new DeliveryServiceDocumentsResource($query->find($federalDistrictId));
    }

    public function search(DeliveryServiceDocumentsQuery $query): AnonymousResourceCollection
    {
        return DeliveryServiceDocumentsResource::collectPage(
            $query->get()
        );
    }

    public function searchOne(DeliveryServiceDocumentsQuery $query): DeliveryServiceDocumentsResource
    {
        return new DeliveryServiceDocumentsResource($query->first());
    }
}
