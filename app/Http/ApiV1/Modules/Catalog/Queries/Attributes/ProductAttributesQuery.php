<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\Attributes;

use App\Http\ApiV1\Modules\Catalog\Queries\PimQuery;
use Ensi\PimClient\Api\PropertiesApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\PropertyResponse;
use Ensi\PimClient\Dto\SearchPropertiesRequest;
use Ensi\PimClient\Dto\SearchPropertiesResponse;
use Illuminate\Http\Request;

class ProductAttributesQuery extends PimQuery
{
    public function __construct(Request $request, private PropertiesApi $api)
    {
        parent::__construct($request, SearchPropertiesRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function searchById($id): PropertyResponse
    {
        return $this->api->getProperty($id, $this->getInclude());
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchPropertiesResponse
    {
        return $this->api->searchProperties($request);
    }
}
