<?php

namespace App\Domain\Catalog\Actions\Attributes;

use Ensi\PimClient\Api\PropertiesApi;
use Ensi\PimClient\Dto\Property;
use Ensi\PimClient\Dto\ReplacePropertyRequest;

class ReplaceProductAttributeAction
{
    public function __construct(private PropertiesApi $api)
    {
    }

    public function execute(int $attributeId, array $fields): Property
    {
        $request = new ReplacePropertyRequest($fields);

        return $this->api->replaceProperty($attributeId, $request)->getData();
    }
}
