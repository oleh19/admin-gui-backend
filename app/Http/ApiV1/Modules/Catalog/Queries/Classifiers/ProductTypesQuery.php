<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\Classifiers;

use App\Http\ApiV1\Modules\Catalog\Queries\PimQuery;
use Ensi\PimClient\Api\ProductTypesApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\ProductTypeResponse;
use Ensi\PimClient\Dto\SearchProductTypesRequest;
use Ensi\PimClient\Dto\SearchProductTypesResponse;
use Illuminate\Http\Request;

class ProductTypesQuery extends PimQuery
{
    public function __construct(Request $request, protected ProductTypesApi $productTypesApi)
    {
        parent::__construct($request, SearchProductTypesRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function searchById($productTypeId): ProductTypeResponse
    {
        return $this->productTypesApi->getProductType($productTypeId);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchProductTypesResponse
    {
        return $this->productTypesApi->searchProductTypes($request);
    }
}
