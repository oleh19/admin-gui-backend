<?php

namespace App\Domain\Marketing\Actions\Discounts;

use Ensi\MarketingClient\Api\DiscountSegmentsApi;
use Ensi\MarketingClient\Dto\DiscountSegment;
use Ensi\MarketingClient\Dto\DiscountSegmentUpdatableProperties;

class PatchDiscountSegmentAction
{
    public function __construct(protected DiscountSegmentsApi $discountsApi)
    {
    }

    public function execute(int $discountSegmentId, array $data): DiscountSegment
    {
        $requestDiscountSegmentsApi  = new DiscountSegmentUpdatableProperties($data);
        $responseDiscountSegmentsApi = $this->discountsApi->patchDiscountSegment(
            $discountSegmentId,
            $requestDiscountSegmentsApi
        );

        return $responseDiscountSegmentsApi->getData();
    }
}
