<?php

namespace App\Domain\Units\Actions\SellerUsers;

use App\Domain\Units\Data\SellerUserData;
use Ensi\AdminAuthClient\ApiException as AdminAuthApiException;
use Ensi\AdminAuthClient\Dto\PatchUserRequest;
use Ensi\BuClient\ApiException as BuApiException;
use Ensi\BuClient\Dto\OperatorForPatch;

/**
 * Class PatchSellerUserAction
 * @package App\Domain\Units\Actions\SellerUsers
 */
class PatchSellerUserAction extends SellerUserAction
{
    /**
     * @param  array  $fields
     * @return SellerUserData
     * @throws BuApiException
     * @throws AdminAuthApiException
     */
    public function execute(int $operatorId, array $fields): SellerUserData
    {
        $operatorRequest = new OperatorForPatch($fields);
        $operator = $this->operatorsApi->patchOperator($operatorId, $operatorRequest)->getData();
        $userRequest = new PatchUserRequest($fields);
        $user =  $this->usersApi->patchUser($operator->getUserId(), $userRequest)->getData();

        return $this->prepareSellerUser($operator, $user);
    }
}
