<?php

namespace App\Domain\Catalog\Actions\Classifiers\Countries;

use Ensi\PimClient\Api\CountriesApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\Country;
use Ensi\PimClient\Dto\CreateCountryRequest;

class CreateCountryAction
{
    public function __construct(
        private CountriesApi $countriesApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): Country
    {
        $request = new CreateCountryRequest($fields);

        return $this->countriesApi->createCountry($request)->getData();
    }
}
