<?php

namespace App\Domain\Marketing\Actions\PromoCodes;

use App\Domain\Auth\Models\User;
use Ensi\MarketingClient\Api\PromoCodesApi;
use Ensi\MarketingClient\Dto\PromoCode;
use Ensi\MarketingClient\Dto\PromoCodeRequest;

class ReplacePromoCodeAction
{
    public function __construct(protected PromoCodesApi $promoCodesApi)
    {
    }

    public function execute(int $promoCodeId, array $data): PromoCode
    {
        /** @var User $user */
        $user = auth()->user();

        $data['creator_id'] = $user->getId();
        $data['seller_id'] = null;

        $requestPromoCodesApi = new PromoCodeRequest($data);
        $responsePromoCodesApi = $this->promoCodesApi->replacePromoCode($promoCodeId, $requestPromoCodesApi);

        return $responsePromoCodesApi->getData();
    }
}
