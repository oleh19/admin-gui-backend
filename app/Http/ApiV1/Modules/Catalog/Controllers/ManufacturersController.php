<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers;

use App\Domain\Catalog\Actions\Classifiers\Manufacturers\CreateManufacturerAction;
use App\Domain\Catalog\Actions\Classifiers\Manufacturers\DeleteManufacturerAction;
use App\Domain\Catalog\Actions\Classifiers\Manufacturers\ReplaceManufacturerAction;
use App\Http\ApiV1\Modules\Catalog\Queries\Classifiers\ManufacturersQuery;
use App\Http\ApiV1\Modules\Catalog\Requests\Classifiers\CreateOrReplaceManufacturerRequest;
use App\Http\ApiV1\Modules\Catalog\Resources\Classifiers\ManufacturersResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ManufacturersController
{
    public function search(ManufacturersQuery $query): AnonymousResourceCollection
    {
        return ManufacturersResource::collectPage($query->get());
    }

    public function create(
        CreateOrReplaceManufacturerRequest $request,
        CreateManufacturerAction $action
    ): ManufacturersResource {
        return new ManufacturersResource($action->execute($request->validated()));
    }

    public function get(
        int $manufacturerId,
        ManufacturersQuery $query
    ): ManufacturersResource {
        return new ManufacturersResource($query->find($manufacturerId));
    }

    public function replace(
        int $manufacturerId,
        CreateOrReplaceManufacturerRequest $request,
        ReplaceManufacturerAction $action
    ): ManufacturersResource {
        return new ManufacturersResource($action->execute($manufacturerId, $request->validated()));
    }

    public function delete(
        int $manufacturerId,
        DeleteManufacturerAction $action
    ): EmptyResource {
        $action->execute($manufacturerId);

        return new EmptyResource();
    }
}
