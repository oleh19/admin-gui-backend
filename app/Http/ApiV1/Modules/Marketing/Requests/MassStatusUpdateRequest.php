<?php

namespace App\Http\ApiV1\Modules\Marketing\Requests;

use App\Http\ApiV1\OpenApiGenerated\Dto\MarketingDiscountStatusEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class MassStatusUpdateRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'id' => ['required', 'array'],
            'id.*' => ['integer'],
            'status' => ['required', 'integer', Rule::in(MarketingDiscountStatusEnum::getAllowableEnumValues())],
        ];
    }
}
