<?php

namespace App\Domain\Logistic\Actions\Geos;

use Ensi\LogisticClient\Api\GeosApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\CreateFederalDistrictRequest;
use Ensi\LogisticClient\Dto\FederalDistrict;

/**
 * Class CreateFederalDistrictAction
 * @package App\Domain\Logistic\Actions\Geos
 */
class CreateFederalDistrictAction
{
    protected GeosApi $geosApi;

    /**
     * CreateFederalDistrictAction constructor.
     * @param  GeosApi  $geosApi
     */
    public function __construct(GeosApi $geosApi)
    {
        $this->geosApi = $geosApi;
    }

    /**
     * @param  array  $fields
     * @return FederalDistrict
     * @throws ApiException
     */
    public function execute(array $fields): FederalDistrict
    {
        $request = new CreateFederalDistrictRequest();
        $request->setName($fields['name']);

        return $this->geosApi->createFederalDistrict($request)->getData();
    }
}
