<?php

namespace App\Http\ApiV1\Modules\Cms\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\CmsClient\Dto\BannerImageTypeEnum;
use Illuminate\Validation\Rule;

class UploadBannerFileRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'file' => ['required', 'image', 'max:2048'],
            'type' => ['required', Rule::in(BannerImageTypeEnum::getAllowableEnumValues())],
        ];
    }
}
