<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Products;

use App\Http\ApiV1\OpenApiGenerated\Dto\CatalogProductArchiveTypeEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateOrReplaceProductRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'external_id' => ['required', 'string'],
            'name' => ['required', 'string'],
            'code' => ['required', 'string'],
            'description' => ['nullable', 'string'],
            'product_type_id' => ['nullable', 'integer'],
            'brand_id' => ['nullable', 'integer'],
            'category_id' => ['nullable', 'integer'],
            'manufacturer_id' => ['nullable', 'integer'],
            'country_id' => ['nullable', 'integer'],
            'barcode' => ['nullable', 'string'],
            'weight' => ['nullable', 'numeric'],
            'weight_gross' => ['nullable', 'numeric'],
            'length' => ['nullable', 'numeric'],
            'width' => ['nullable', 'numeric'],
            'height' => ['nullable', 'numeric'],
            'ingredients' => ['nullable', 'string'],
            'is_new' => ['nullable', 'boolean'],
            'archive' => ['nullable', Rule::in(CatalogProductArchiveTypeEnum::getAllowableEnumValues())],
        ];
    }
}
