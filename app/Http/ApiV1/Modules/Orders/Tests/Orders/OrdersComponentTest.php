<?php

use App\Domain\Catalog\Tests\Factories\Categories\CategoryFactory;
use App\Domain\Catalog\Tests\Factories\Classifiers\BrandFactory;
use App\Domain\Catalog\Tests\Factories\Offers\OfferFactory;
use App\Domain\Catalog\Tests\Factories\Products\ProductFactory;
use App\Domain\Customers\Tests\Factories\CustomerFactory;
use App\Domain\Customers\Tests\Factories\CustomerUserFactory;
use App\Domain\Logistic\Tests\DeliveryServices\Factories\DeliveryServiceFactory;
use App\Domain\Logistic\Tests\Factories\DeliveryMethodFactory;
use App\Domain\Orders\Tests\Factories\Orders\DeliveryFactory;
use App\Domain\Orders\Tests\Factories\Orders\Enums\OrderStatusFactory;
use App\Domain\Orders\Tests\Factories\Orders\Enums\PaymentMethodFactory;
use App\Domain\Orders\Tests\Factories\Orders\Enums\PaymentStatusFactory;
use App\Domain\Orders\Tests\Factories\Orders\OrderFactory;
use App\Domain\Orders\Tests\Factories\Orders\OrderItemFactory;
use App\Domain\Orders\Tests\Factories\Orders\OrderSourceFactory;
use App\Domain\Orders\Tests\Factories\Orders\ShipmentFactory;
use App\Domain\Orders\Tests\Factories\PaymentSystems\PaymentSystemFactory;
use App\Domain\Units\Tests\Factories\AdminUserFactory;
use App\Http\ApiV1\Modules\Orders\Tests\Orders\Factories\OrderRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use App\Http\ApiV1\Support\Tests\Factories\PaginationFactory;
use App\Http\ApiV1\Support\Tests\Factories\PromiseFactory;
use Ensi\OmsClient\Dto\SearchOrdersRequest;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function PHPUnit\Framework\assertEquals;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'orders', 'orders.orders');

test('POST /api/v1/orders/orders:search 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $orderId = 1;

    $this->mockOmsOrdersApi()->allows([
        'searchOrders' => OrderFactory::new()->makeResponseSearch(['id' => $orderId]),
    ]);

    postJson("/api/v1/orders/orders:search", ['pagination' => PaginationFactory::new()->makeOffsetRequest()])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $orderId);
});

test('POST /api/v1/orders/orders:search all includes 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $orderId = 1;
    $responsibleId = 1;
    $customerId = 1;
    $customerUserId = 1;
    $offerId = 1;
    $productId = 1;
    $categoryId = 1;
    $brandId = 1;
    $omsIncludes = [
        "files",
        "deliveries",
        "deliveries.shipments",
        "deliveries.shipments.orderItems",
    ];

    $orderItem = OrderItemFactory::new()->make(['offer_id' => $offerId]);
    $shipment = ShipmentFactory::new()->withOrderItem($orderItem)->make();
    $delivery = DeliveryFactory::new()->withShipment($shipment)->make();
    $ordersResponse = OrderFactory::new()
        ->withFile()
        ->withDelivery($delivery)
        ->makeResponseSearch(['id' => $orderId, 'responsible_id' => $responsibleId, 'customer_id' => $customerId]);
    $this->mockOmsOrdersApi()
        ->shouldReceive('searchOrders')
        ->withArgs(function ($arg) use ($omsIncludes) {
            /** @var SearchOrdersRequest $arg */
            assertEquals($omsIncludes, $arg->getInclude());

            return true;
        })
        ->andReturn($ordersResponse);
    $this->mockAdminAuthUsersApi()->allows([
        'searchUsers' => AdminUserFactory::new()->makeResponseSearch(['id' => $responsibleId]),
    ]);
    $this->mockCustomersCustomersApi()->allows([
        'searchCustomers' => CustomerFactory::new()->makeResponseSearch(['id' => $customerId, 'user_id' => $customerUserId]),
    ]);
    $this->mockCustomersAuthUsersApi()->allows([
        'searchUsers' => CustomerUserFactory::new()->makeResponseSearch(['id' => $customerUserId]),
    ]);
    $this->mockOffersOffersApi()->allows([
        'searchOffers' => OfferFactory::new()->makeResponseSearch(['id' => $offerId, 'product_id' => $productId]),
    ]);
    $category = CategoryFactory::new()->make(['id' => $categoryId]);
    $brand = BrandFactory::new()->make(['id' => $brandId]);
    $this->mockPimProductsApi()->allows([
        'searchProducts' => ProductFactory::new()
            ->withImages()
            ->withCategory($category)
            ->withBrand($brand)
            ->makeResponseSearch(['id' => $productId]),
    ]);

    postJson("/api/v1/orders/orders:search", [
        "include" => array_merge($omsIncludes, [
            "responsible",
            "customer",
            "customer.user",
            "orderItems.product",
            "orderItems.product.images",
            "orderItems.product.category",
            "orderItems.product.brand",
        ]),
    ])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $orderId)
        ->assertJsonCount(1, 'data.0.files')
        ->assertJsonCount(1, 'data.0.deliveries')
        ->assertJsonCount(1, 'data.0.deliveries.0.shipments')
        ->assertJsonCount(1, 'data.0.deliveries.0.shipments.0.order_items')
        ->assertJsonPath('data.0.customer.id', $customerId)
        ->assertJsonPath('data.0.customer.user.id', $customerUserId)
        ->assertJsonPath('data.0.customer.user.id', $customerUserId)
        ->assertJsonPath('data.0.responsible.id', $responsibleId)
        ->assertJsonPath('data.0.deliveries.0.shipments.0.order_items.0.product.id', $productId)
        ->assertJsonCount(1, 'data.0.deliveries.0.shipments.0.order_items.0.product.images')
        ->assertJsonPath('data.0.deliveries.0.shipments.0.order_items.0.product.category.id', $categoryId)
        ->assertJsonPath('data.0.deliveries.0.shipments.0.order_items.0.product.brand.id', $brandId);
});

test('POST /api/v1/orders/orders/{id} 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $orderId = 1;

    $this->mockOmsOrdersApi()->allows([
        'getOrder' => OrderFactory::new()->makeResponseOne(['id' => $orderId]),
    ]);

    getJson("/api/v1/orders/orders/{$orderId}")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $orderId);
});

test('PATCH /api/v1/orders/orders/{id} 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $orderId = 1;
    $pathOrderData = OrderRequestFactory::new()->makePathOrder();

    $this->mockOmsOrdersApi()->allows([
        'patchOrder' => OrderFactory::new()->makeResponseOne(array_merge(['id' => $orderId], $pathOrderData)),
    ]);

    patchJson("/api/v1/orders/orders/{$orderId}", $pathOrderData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $orderId)
        ->assertJsonPath('data.responsible_id', $pathOrderData['responsible_id'])
        ->assertJsonPath('data.status', $pathOrderData['status'])
        ->assertJsonPath('data.client_comment', $pathOrderData['client_comment'])
        ->assertJsonPath('data.receiver_name', $pathOrderData['receiver_name'])
        ->assertJsonPath('data.receiver_phone', $pathOrderData['receiver_phone'])
        ->assertJsonPath('data.receiver_email', $pathOrderData['receiver_email'])
        ->assertJsonPath('data.is_problem', $pathOrderData['is_problem'])
        ->assertJsonPath('data.problem_comment', $pathOrderData['problem_comment']);
});

test('POST /api/v1/orders/orders/{id}:change-payment-method 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $paymentSystem = 4;

    $this->mockOmsOrdersApi()->allows([
        'changeOrderPaymentSystem' => OrderFactory::new()->makeResponseOne(['payment_system' => $paymentSystem]),
    ]);

    postJson("/api/v1/orders/orders/1:change-payment-method", ['payment_system' => $paymentSystem])
        ->assertStatus(200)
        ->assertJsonPath('data.payment_system', $paymentSystem);
});

test('POST /api/v1/orders/orders/{id}:change-delivery 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $orderDeliveryData = OrderRequestFactory::new()->makeChangeDelivery();

    $this->mockOmsOrdersApi()->allows([
        'changeOrderDelivery' => OrderFactory::new()->makeResponseOne($orderDeliveryData),
    ]);

    postJson("/api/v1/orders/orders/1:change-delivery", $orderDeliveryData)
        ->assertStatus(200)
        ->assertJsonPath('data.delivery_service', $orderDeliveryData['delivery_service'])
        ->assertJsonPath('data.delivery_method', $orderDeliveryData['delivery_method'])
        ->assertJsonPath('data.delivery_tariff_id', $orderDeliveryData['delivery_tariff_id'])
        ->assertJsonPath('data.delivery_point_id', $orderDeliveryData['delivery_point_id'])
        ->assertJsonPath('data.delivery_price', $orderDeliveryData['delivery_price'])
        ->assertJsonPath('data.delivery_cost', $orderDeliveryData['delivery_cost'])
        ->assertJsonPath('data.delivery_comment', $orderDeliveryData['delivery_comment']);
});

function mockMeta(ApiV1ComponentTestCase $testCase)
{
    $testCase->mockOmsEnumsApi()->allows([
        'getOrderSourcesAsync' => PromiseFactory::make(OrderSourceFactory::new()->makeResponseSearch()),
        'getOrderStatusesAsync' => PromiseFactory::make(OrderStatusFactory::new()->makeResponseSearch()),
        'getPaymentMethodsAsync' => PromiseFactory::make(PaymentMethodFactory::new()->makeResponseSearch()),
        'getPaymentStatusesAsync' => PromiseFactory::make(PaymentStatusFactory::new()->makeResponseSearch()),
        'getPaymentSystemsAsync' => PromiseFactory::make(PaymentSystemFactory::new()->makeResponseSearch()),
    ]);
    $testCase->mockLogisticDeliveryServicesApi()->allows([
        'getDeliveryMethodsAsync' => PromiseFactory::make(DeliveryMethodFactory::new()->makeResponseGet()),
        'searchDeliveryServicesAsync' => PromiseFactory::make(DeliveryServiceFactory::new()->makeResponseSearch()),
    ]);
}
test('POST /api/v1/orders/orders:meta 200', function () {
    mockMeta($this);
    getJson("/api/v1/orders/orders:meta")->assertStatus(200);
});

test('POST /api/v1/orders/orders:meta all fields 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    mockMeta($this);
    $request = getJson("/api/v1/orders/orders:meta");
    $metaFields = Arr::pluck($request->decodeResponseJson()['data']['fields'], 'code');
    sort($metaFields);

    $this->mockOmsOrdersApi()->allows([
        'searchOrders' => OrderFactory::new()->makeResponseSearch(),
    ]);
    $request = postJson("/api/v1/orders/orders:search");
    $orderFields = array_keys($request->decodeResponseJson()['data'][0]);
    $orderFields = array_diff($orderFields, ['delivery_address']);

    sort($metaFields);
    sort($orderFields);
    assertEquals($metaFields, $orderFields);
});
