<?php

namespace App\Domain\Catalog\Actions\Classifiers\Countries;

use Ensi\PimClient\Api\CountriesApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\EmptyDataResponse;

class DeleteCountryAction
{
    public function __construct(
        private CountriesApi $countriesApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $countryId): EmptyDataResponse
    {
        return $this->countriesApi->deleteCountry($countryId);
    }
}
