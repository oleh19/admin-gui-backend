<?php


namespace App\Domain\Orders\Data\Orders;

use Ensi\OffersClient\Dto\Offer;
use Ensi\OmsClient\Dto\Shipment;
use Ensi\PimClient\Dto\Product;

class ShipmentData
{
    /** @var Offer[] */
    public array $offers = [];
    /** @var Product[] */
    public array $products = [];

    public function __construct(public Shipment $shipment)
    {
    }

    public function getDelivery(): ?DeliveryData
    {
        if (!$this->shipment->getDelivery()) {
            return null;
        }

        $deliveryData = new DeliveryData($this->shipment->getDelivery());
        $deliveryData->offers = $this->offers;
        $deliveryData->products = $this->products;

        return $deliveryData;
    }

    /**
     * @return OrderItemData[]|null
     */
    public function getOrderItems(): ?array
    {
        if (is_null($this->shipment->getOrderItems())) {
            return null;
        }
        $orderItemsData = [];
        foreach ($this->shipment->getOrderItems() as $orderItem) {
            $orderItemData = new OrderItemData($orderItem);
            foreach ($this->offers as $offer) {
                if ($offer->getId() == $orderItem->getOfferId()) {
                    $orderItemData->offer = $offer;

                    break;
                }
            }
            if ($orderItemData->offer) {
                foreach ($this->products as $product) {
                    if ($product->getId() == $orderItemData->offer->getProductId()) {
                        $orderItemData->product = $product;

                        break;
                    }
                }
            }

            $orderItemsData[] = $orderItemData;
        }

        return $orderItemsData;
    }
}
