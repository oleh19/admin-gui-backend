<?php
/**
 * CmsBannerButtonLocationEnum
 *
 * PHP version 7
 *
 * @category Class
 * @package  App\Http\ApiV1\OpenApiGenerated
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Admin GUI Backend
 *
 * Admin GUI Backend
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace App\Http\ApiV1\OpenApiGenerated\Dto;

/**
 * CmsBannerButtonLocationEnum Class Doc Comment
 *
 * @category Class
 * @description Расположение кнопки:   * &#x60;top_left&#x60; - Сверху слева   * &#x60;top&#x60; - Сверху   * &#x60;top_right&#x60; - Сверху справа   * &#x60;right&#x60; - Справа   * &#x60;bottom_right&#x60; - Снизу справа   * &#x60;bottom&#x60; - Снизу   * &#x60;bottom_left&#x60; - Снизу слева   * &#x60;left&#x60; - Слева
 * @package  App\Http\ApiV1\OpenApiGenerated
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class CmsBannerButtonLocationEnum
{
    /**
     * Possible values of this enum
     */
    const LOCATION_TOP_LEFT = 'top_left';
    const LOCATION_TOP = 'top';
    const LOCATION_TOP_RIGHT = 'top_right';
    const LOCATION_RIGHT = 'right';
    const LOCATION_BOTTOM_RIGHT = 'bottom_right';
    const LOCATION_BOTTOM = 'bottom';
    const LOCATION_BOTTOM_LEFT = 'bottom_left';
    const LOCATION_LEFT = 'left';

    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public static function getAllowableEnumValues(): array
    {
        return [
            self::LOCATION_TOP_LEFT,
            self::LOCATION_TOP,
            self::LOCATION_TOP_RIGHT,
            self::LOCATION_RIGHT,
            self::LOCATION_BOTTOM_RIGHT,
            self::LOCATION_BOTTOM,
            self::LOCATION_BOTTOM_LEFT,
            self::LOCATION_LEFT,
        ];
    }
}
