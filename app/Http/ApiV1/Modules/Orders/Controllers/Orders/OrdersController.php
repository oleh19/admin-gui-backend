<?php


namespace App\Http\ApiV1\Modules\Orders\Controllers\Orders;

use App\Domain\Common\Actions\EnumInfoLoadAction;
use App\Domain\Common\Data\Meta\Enum\AdminUserEnumInfo;
use App\Domain\Common\Data\Meta\Enum\CustomerUserEnumInfo;
use App\Domain\Common\Data\Meta\Enum\LogisticDeliveryMethodEnumInfo;
use App\Domain\Common\Data\Meta\Enum\LogisticDeliveryServiceEnumInfo;
use App\Domain\Common\Data\Meta\Enum\LogisticPointEnumInfo;
use App\Domain\Common\Data\Meta\Enum\LogisticTariffEnumInfo;
use App\Domain\Common\Data\Meta\Enum\OrdersOrderSourceEnumInfo;
use App\Domain\Common\Data\Meta\Enum\OrdersOrderStatusEnumInfo;
use App\Domain\Common\Data\Meta\Enum\OrdersPaymentMethodEnumInfo;
use App\Domain\Common\Data\Meta\Enum\OrdersPaymentStatusEnumInfo;
use App\Domain\Common\Data\Meta\Enum\OrdersPaymentSystemEnumInfo;
use App\Domain\Common\Data\Meta\Fields\BoolField;
use App\Domain\Common\Data\Meta\Fields\DatetimeField;
use App\Domain\Common\Data\Meta\Fields\EmailField;
use App\Domain\Common\Data\Meta\Fields\EnumField;
use App\Domain\Common\Data\Meta\Fields\IdField;
use App\Domain\Common\Data\Meta\Fields\IntField;
use App\Domain\Common\Data\Meta\Fields\ObjectField;
use App\Domain\Common\Data\Meta\Fields\PhoneField;
use App\Domain\Common\Data\Meta\Fields\PriceField;
use App\Domain\Common\Data\Meta\Fields\StringField;
use App\Domain\Orders\Actions\Orders\AttachOrderFileAction;
use App\Domain\Orders\Actions\Orders\ChangeOrderDeliveryAction;
use App\Domain\Orders\Actions\Orders\ChangeOrderPaymentSystemAction;
use App\Domain\Orders\Actions\Orders\DeleteOrderFilesAction;
use App\Domain\Orders\Actions\Orders\PatchOrderAction;
use App\Http\ApiV1\Modules\Orders\Queries\Orders\OrdersQuery;
use App\Http\ApiV1\Modules\Orders\Requests\Orders\AttachOrderFileRequest;
use App\Http\ApiV1\Modules\Orders\Requests\Orders\ChangeOrderDeliveryRequest;
use App\Http\ApiV1\Modules\Orders\Requests\Orders\ChangeOrderPaymentSystemRequest;
use App\Http\ApiV1\Modules\Orders\Requests\Orders\DeleteOrderFilesRequest;
use App\Http\ApiV1\Modules\Orders\Requests\Orders\PatchOrderRequest;
use App\Http\ApiV1\Modules\Orders\Resources\Orders\OrderFilesResource;
use App\Http\ApiV1\Modules\Orders\Resources\Orders\OrdersResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;

class OrdersController
{
    public function search(OrdersQuery $query)
    {
        return OrdersResource::collectPage($query->get());
    }

    public function meta(
        EnumInfoLoadAction $loadAction,
        CustomerUserEnumInfo $customer,
        AdminUserEnumInfo $adminUser,
        OrdersOrderSourceEnumInfo $orderSource,
        OrdersOrderStatusEnumInfo $orderStatus,
        OrdersPaymentStatusEnumInfo $orderPaymentStatus,
        OrdersPaymentMethodEnumInfo $orderPaymentMethod,
        OrdersPaymentSystemEnumInfo $orderPaymentSystem,
        LogisticDeliveryMethodEnumInfo $deliveryMethod,
        LogisticDeliveryServiceEnumInfo $deliveryService,
        LogisticTariffEnumInfo $logisticTariff,
        LogisticPointEnumInfo $unitsPvz,
    ) {
        $loadAction->execute([
            $orderSource,
            $orderStatus,
            $orderPaymentStatus,
            $orderPaymentMethod,
            $orderPaymentSystem,
            $deliveryMethod,
            $deliveryService,
        ]);

        return new ModelMetaResource([
            (new IdField())->listDefault()->filterDefault(),
            (new StringField('number', '№ заказа'))->sort()->detailLink()->filterDefault(),
            (new EnumField('source', 'Источник', $orderSource))->filterDefault(),
            (new EnumField('customer_id', 'Покупатель', $customer))->listDefault()->filterDefault(),
            (new EnumField('responsible_id', 'Ответственный', $adminUser))->listDefault()->filterDefault(),
            (new EmailField('customer_email', 'Почта покупателя'))->sort()->filter('customer_email_like'),
            new PriceField('cost', 'Сумма до скидок'),
            (new PriceField('price', 'Сумма'))->listDefault()->filterDefault(),
            new IntField('spent_bonus', 'Списано баллов'),
            new IntField('added_bonus', 'Добавлено баллов'),
            (new StringField('promo_code', 'Промокод'))->sort(),
            new EnumField('delivery_method', 'Метод доставки', $deliveryMethod),
            new EnumField('delivery_service', 'Служба доставки', $deliveryService),
            new EnumField('delivery_tariff_id', 'Тариф доставки', $logisticTariff),
            new EnumField('delivery_point_id', 'ПВЗ', $unitsPvz),
            new ObjectField('delivery_address', 'Адрес доставки'),
            new PriceField('delivery_price', 'Цена доставки'),
            new PriceField('delivery_cost', 'Цена доставки до скидок'),
            (new StringField('receiver_name', 'ФИО получателя'))->sort(),
            (new PhoneField('receiver_phone', 'Телефон получателя'))->sort(),
            (new EmailField('receiver_email', 'Почта получателя'))->sort(),
            (new EnumField('status', 'Статус', $orderStatus))->listDefault()->filterDefault(),
            new DatetimeField('status_at', 'Дата изменения статуса'),
            new EnumField('payment_status', 'Статус оплаты', $orderPaymentStatus),
            new DatetimeField('payment_status_at', 'Дата изменения статуса оплаты'),
            new DatetimeField('payed_at', 'Дата оплаты'),
            new DatetimeField('payment_expires_at', 'Дата просрочки оплаты'),
            new EnumField('payment_method', 'Метод оплаты', $orderPaymentMethod),
            new EnumField('payment_system', 'Система оплаты', $orderPaymentSystem),
            // new UrlField('payment_link', 'Ссылка на оплату'),
            new StringField('payment_external_id', 'ID оплаты во внешней системе'),
            (new BoolField('is_expired', 'Заказ просрочен'))->listDefault()->filterDefault(),
            new DatetimeField('is_expired_at', 'Дата, когда заказ был просрочен'),
            (new BoolField('is_return', 'Заказ возвращен'))->listDefault()->filterDefault(),
            new DatetimeField('is_return_at', 'Дата изменения признака "Заказ возвращен"'),
            (new BoolField('is_partial_return', 'Заказ возвращен частично'))->listDefault()->filterDefault(),
            new DatetimeField('is_partial_return_at', 'Дата изменения признака "Заказ возвращен частично"'),
            (new BoolField('is_problem', 'Заказ проблемный'))->listDefault()->filterDefault(),
            new DatetimeField('is_problem_at', 'Дата изменения признака "Заказ проблемный"'),
            new StringField('problem_comment', 'Комментарий о проблеме'),
            (new DatetimeField('created_at', 'Дата создания'))->listDefault()->filterDefault(),
            new DatetimeField('updated_at', 'Дата обновления'),
            new StringField('delivery_comment', 'Комментарий к доставке'),
            new StringField('client_comment', 'Комментарий клиента'),
        ]);
    }

    public function get(int $id, OrdersQuery $ordersQuery)
    {
        return new OrdersResource($ordersQuery->find($id));
    }

    public function patch(int $id, PatchOrderAction $action, PatchOrderRequest $request)
    {
        return new OrdersResource($action->execute($id, $request->validated()));
    }

    public function changePaymentSystem(int $id, ChangeOrderPaymentSystemRequest $request, ChangeOrderPaymentSystemAction $action)
    {
        return new OrdersResource($action->execute($id, $request->getPaymentSystem()));
    }

    public function changeDelivery(int $id, ChangeOrderDeliveryRequest $request, ChangeOrderDeliveryAction $action)
    {
        return new OrdersResource($action->execute($id, $request->validated()));
    }

    public function attachFile(int $id, AttachOrderFileAction $action, AttachOrderFileRequest $request)
    {
        return new OrderFilesResource($action->execute($id, $request->getFile()));
    }

    public function deleteFiles(int $id, DeleteOrderFilesAction $action, DeleteOrderFilesRequest $request)
    {
        $action->execute($id, $request->getFileIds());

        return new EmptyResource();
    }
}
