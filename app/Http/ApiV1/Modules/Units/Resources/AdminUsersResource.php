<?php


namespace App\Http\ApiV1\Modules\Units\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\AdminAuthClient\Dto\User as AdminUser;

/**
 * @mixin AdminUser
 */
class AdminUsersResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        $roles = $this->getRoles();

        return [
            "id" => $this->getId(),
            "full_name" => $this->getFullName(),
            "short_name" => $this->getShortName(),
            "created_at" => $this->dateTimeToIso($this->getCreatedAt()),
            "updated_at" => $this->dateTimeToIso($this->getUpdatedAt()),
            "active" => $this->getActive(),
            "login" => $this->getLogin(),
            "last_name" => $this->getLastName(),
            "first_name" => $this->getFirstName(),
            "middle_name" => $this->getMiddleName(),
            "email" => $this->getEmail(),
            "phone" => $this->getPhone(),
            "timezone" => $this->getTimezone(),
            "roles" => AdminUserRolesIncludedResource::collection($this->when(!is_null($roles), $roles)),
        ];
    }
}
