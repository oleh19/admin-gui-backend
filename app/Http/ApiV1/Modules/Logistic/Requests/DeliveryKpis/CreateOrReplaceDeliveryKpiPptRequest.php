<?php


namespace App\Http\ApiV1\Modules\Logistic\Requests\DeliveryKpis;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

/**
 * Class CreateOrReplaceDeliveryKpiPptRequest
 * @package App\Http\ApiV1\Modules\Logistic\Requests\DeliveryKpis
 */
class CreateOrReplaceDeliveryKpiPptRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'ppt' => ['required', 'integer', 'min:0'],
        ];
    }
}
