<?php

namespace App\Domain\Customers\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use App\Http\ApiV1\Support\Tests\Factories\EnsiFileFactory;
use Ensi\CrmClient\Dto\Customer;
use Ensi\CrmClient\Dto\CustomerGenderEnum;
use Ensi\CrmClient\Dto\CustomerStatusEnum;
use Ensi\CrmClient\Dto\File;
use Ensi\CrmClient\Dto\SearchCustomersResponse;

class CustomerFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->randomNumber(),
            'avatar' => $this->faker->boolean() ? new File(EnsiFileFactory::new()->make()) : null,
            'user_id' => $this->faker->randomNumber(),
            'status' => $this->faker->randomElement(CustomerStatusEnum::getAllowableEnumValues()),
            'birthday' => $this->faker->optional()->date(),
            'gender' => $this->faker->optional()->randomElement(CustomerGenderEnum::getAllowableEnumValues()),
            'comment_internal' => $this->faker->optional()->text(50),
            'manager_id' => $this->faker->optional()->randomNumber(),
            'city' => $this->faker->optional()->city(),
            'comment_status' => $this->faker->optional()->text(50),
            'legal_info_company_name' => $this->faker->optional()->company(),
            'legal_info_company_address' => $this->faker->optional()->address(),
            'legal_info_inn' => $this->faker->optional()->numerify('#############'),
            'legal_info_payment_account' => $this->faker->optional()->numerify('#############'),
            'legal_info_bik' => $this->faker->optional()->numerify('#############'),
            'legal_info_bank' => $this->faker->optional()->company(),
            'legal_info_bank_correspondent_account' => $this->faker->optional()->numerify('#############'),
        ];
    }

    public function make(array $extra = []): Customer
    {
        return new Customer($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchCustomersResponse
    {
        return $this->generateResponseSearch(SearchCustomersResponse::class, $extra, $count);
    }
}
