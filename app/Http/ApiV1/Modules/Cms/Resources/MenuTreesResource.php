<?php

namespace App\Http\ApiV1\Modules\Cms\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CmsClient\Dto\MenuTree;
use Illuminate\Http\Request;

/**
 * @mixin MenuTree
 */
class MenuTreesResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->getName(),
            'url' => $this->getUrl(),
            'children' => $this->collection($this->getChildren()),
        ];
    }
}
