<?php


namespace App\Domain\Orders\Data\Orders;

use Ensi\OffersClient\Dto\Offer;
use Ensi\OmsClient\Dto\OrderItem;
use Ensi\PimClient\Dto\Product;

class OrderItemData
{
    public ?Offer $offer = null;
    public ?Product $product = null;

    public function __construct(public OrderItem $orderItem)
    {
    }
}
