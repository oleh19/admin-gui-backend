<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\Products;

use App\Http\ApiV1\Modules\Catalog\Queries\PimQuery;
use Ensi\PimClient\Api\ImagesApi;
use Ensi\PimClient\Dto\ProductImageResponse;
use Ensi\PimClient\Dto\SearchProductImagesRequest;
use Ensi\PimClient\Dto\SearchProductImagesResponse;
use Illuminate\Http\Request;

class ProductImagesQuery extends PimQuery
{
    public function __construct(Request $request, private ImagesApi $api)
    {
        parent::__construct($request, SearchProductImagesRequest::class);
    }

    protected function searchById($id): ProductImageResponse
    {
        return $this->api->getProductImage($id);
    }

    protected function search($request): SearchProductImagesResponse
    {
        return $this->api->searchProductImages($request);
    }
}
