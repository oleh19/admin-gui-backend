<?php

namespace App\Domain\Logistic\Data;

use App\Domain\Logistic\ProtectedFiles\DeliveryServiceDocumentFile;
use Ensi\LogisticClient\Dto\DeliveryServiceDocument;

class DeliveryServiceDocumentData
{
    public function __construct(public DeliveryServiceDocument $deliveryServiceDocument)
    {
    }

    public function getFile(): ?DeliveryServiceDocumentFile
    {
        $file = $this->deliveryServiceDocument->getFile();

        return $file ?
            DeliveryServiceDocumentFile::createFromModel($this->deliveryServiceDocument, $this->deliveryServiceDocument->getFile()) :
            null;
    }
}
