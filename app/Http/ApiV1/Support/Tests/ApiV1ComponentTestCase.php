<?php

namespace App\Http\ApiV1\Support\Tests;

use Ensi\BasketsClient\Api\CommonApi as BasketsCommonApi;
use Ensi\CrmClient\Api\CustomersApi;
use Ensi\CustomerAuthClient\Api\UsersApi as CustomerAuthUsersApi;
use Ensi\LogisticClient\Api\CargoOrdersApi;
use Ensi\LogisticClient\Api\DeliveryPricesApi;
use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Ensi\LogisticClient\Api\GeosApi;
use Ensi\OffersClient\Api\OffersApi;
use Ensi\OmsClient\Api\CommonApi as OmsCommonApi;
use Ensi\OmsClient\Api\DeliveriesApi;
use Ensi\OmsClient\Api\EnumsApi as OmsEnumsApi;
use Ensi\OmsClient\Api\OrdersApi;
use Ensi\OmsClient\Api\RefundsApi;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\UserAuthClient\Api\UsersApi as UserAuthUsersApi;
use Greensight\LaravelOpenApiTesting\ValidatesAgainstOpenApiSpec;
use Mockery\MockInterface;
use Tests\ComponentTestCase;

abstract class ApiV1ComponentTestCase extends ComponentTestCase
{
    use ValidatesAgainstOpenApiSpec;

    protected function getOpenApiDocumentPath(): string
    {
        return public_path('api-docs/v1/index.yaml');
    }

    // region service customers
    public function mockCustomersCustomersApi(): MockInterface|CustomersApi
    {
        return $this->mock(CustomersApi::class);
    }

    public function mockCustomersAuthUsersApi(): MockInterface|CustomerAuthUsersApi
    {
        return $this->mock(CustomerAuthUsersApi::class);
    }

    // endregion

    // region service admin auth
    public function mockAdminAuthUsersApi(): MockInterface|UserAuthUsersApi
    {
        return $this->mock(UserAuthUsersApi::class);
    }

    // endregion

    // region service oms
    public function mockOmsOrdersApi(): MockInterface|OrdersApi
    {
        return $this->mock(OrdersApi::class);
    }

    public function mockOmsDeliveriesApi(): MockInterface|DeliveriesApi
    {
        return $this->mock(DeliveriesApi::class);
    }

    public function mockOmsRefundsApi(): MockInterface|RefundsApi
    {
        return $this->mock(RefundsApi::class);
    }

    public function mockOmsEnumsApi(): MockInterface|OmsEnumsApi
    {
        return $this->mock(OmsEnumsApi::class);
    }

    public function mockOmsCommonApi(): MockInterface|OmsCommonApi
    {
        return $this->mock(OmsCommonApi::class);
    }

    // endregion

    // region service baskets
    public function mockBasketsCommonApi(): MockInterface|BasketsCommonApi
    {
        return $this->mock(BasketsCommonApi::class);
    }

    // endregion

    // region service logistic
    protected function mockLogisticCargoOrdersApi(): MockInterface|CargoOrdersApi
    {
        return $this->mock(CargoOrdersApi::class);
    }

    public function mockLogisticDeliveryServicesApi(): MockInterface|DeliveryServicesApi
    {
        return $this->mock(DeliveryServicesApi::class);
    }

    public function mockLogisticGeosApi(): MockInterface|GeosApi
    {
        return $this->mock(GeosApi::class);
    }

    public function mockLogisticDeliveryPricesApi(): MockInterface|DeliveryPricesApi
    {
        return $this->mock(DeliveryPricesApi::class);
    }

    // endregion

    // region service pim
    protected function mockPimProductsApi(): MockInterface|ProductsApi
    {
        return $this->mock(ProductsApi::class);
    }

    // endregion

    // region service offers
    protected function mockOffersOffersApi(): MockInterface|OffersApi
    {
        return $this->mock(OffersApi::class);
    }

    // endregion
}
