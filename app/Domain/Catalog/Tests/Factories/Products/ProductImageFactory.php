<?php

namespace App\Domain\Catalog\Tests\Factories\Products;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use App\Http\ApiV1\Support\Tests\Factories\EnsiFileFactory;
use Ensi\PimClient\Dto\File;
use Ensi\PimClient\Dto\ProductImage;
use Ensi\PimClient\Dto\ProductImageTypeEnum;

class ProductImageFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->randomNumber(),
            'product_id' => $this->faker->randomNumber(),
            'file' => new File(EnsiFileFactory::new()->make()),
            'type' => $this->faker->randomElement(ProductImageTypeEnum::getAllowableEnumValues()),
        ];
    }

    public function make(array $extra = []): ProductImage
    {
        return new ProductImage($this->makeArray($extra));
    }
}
