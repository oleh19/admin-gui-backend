<?php

use App\Domain\Orders\ProtectedFiles\OrderFile;
use App\Domain\Orders\Tests\Factories\Orders\OrderFactory;
use App\Domain\Orders\Tests\Factories\Orders\OrderFileFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use App\Http\ApiV1\Support\Tests\Factories\EnsiFileFactory;
use Ensi\OmsClient\Dto\File;
use Illuminate\Http\UploadedFile;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\post;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'orders', 'orders.orderFiles');

test('POST /api/v1/orders/orders/{id}:attach-file 200', function (string $ext) {
    /** @var ApiV1ComponentTestCase $this */
    $orderId = 1;
    $fileName = "file.{$ext}";
    $fileId = 1;

    $this->mockOmsOrdersApi()->allows([
        'attachOrderFile' => OrderFileFactory::new()->makeResponseOne(['id' => $fileId,]),
    ]);

    $requestBody = ['file' => UploadedFile::fake()->create($fileName, 100)];
    post("/api/v1/orders/orders/{$orderId}:attach-file", $requestBody, ['Content-Type' => "multipart/form-data"])
        ->assertStatus(200)
        ->assertJsonPath('data.id', $fileId);
})->with(['jpg', 'pdf']);

test('POST /common/files/download-protected order-file 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $orderId = 1;
    $orderFile = OrderFileFactory::new()->make([
        'order_id' => $orderId,
        'file' => new File(EnsiFileFactory::new()->makeReal()),
    ]);

    $this->mockOmsOrdersApi()->allows([
        'searchOrders' => OrderFactory::new()->withFile($orderFile)->makeResponseSearch(['id' => $orderId]),
    ]);

    postJson("/api/v1/common/files/download-protected", OrderFile::createFromModel($orderFile)->jsonSerialize())
        ->assertStatus(200);
});

test('POST /api/v1/orders/orders/{id}:delete-files 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $orderId = 1;

    $this->mockOmsOrdersApi()->shouldReceive('deleteOrderFiles');

    deleteJson("/api/v1/orders/orders/{$orderId}:delete-files", ['file_ids' => [1, 2]])
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});
