<?php


namespace App\Http\ApiV1\Modules\Orders\Resources\Orders;

use App\Domain\Orders\Data\Orders\OrderItemData;
use App\Http\ApiV1\Modules\Catalog\Resources\Products\ProductsResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class OrderItemsResource
 * @package App\Http\ApiV1\Modules\Orders\Resources\OrdersDetail
 * @mixin OrderItemData
 */
class OrderItemsResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            "id" => $this->orderItem->getId(),
            "order_id" => $this->orderItem->getOrderId(),
            "shipment_id" => $this->orderItem->getShipmentId(),
            "offer_id" => $this->orderItem->getOfferId(),
            "name" => $this->orderItem->getName(),
            "qty" => $this->orderItem->getQty(),
            "price" => $this->orderItem->getPrice(),
            "price_per_one" => $this->orderItem->getPricePerOne(),
            "cost" => $this->orderItem->getCost(),
            "cost_per_one" => $this->orderItem->getCostPerOne(),
            "refund_qty" => $this->orderItem->getRefundQty(),
            "product_data" => [
                'weight' => $this->orderItem->getProductWeight(),
                'weight_gross' => $this->orderItem->getProductWeightGross(),
                'width' => $this->orderItem->getProductWidth(),
                'height' => $this->orderItem->getProductHeight(),
                'length' => $this->orderItem->getProductLength(),
                'external_id' => $this->orderItem->getOfferExternalId(),
                'storage_address' => $this->orderItem->getOfferStorageAddress(),
                'storage_area' => $this->orderItem->getProductStorageArea(),
                'barcode' => $this->orderItem->getProductBarcode(),
            ],
            "created_at" => $this->dateTimeToIso($this->orderItem->getCreatedAt()),
            "updated_at" => $this->dateTimeToIso($this->orderItem->getUpdatedAt()),
            "product" => new ProductsResource($this->whenNotNull($this->product)),
        ];
    }
}
