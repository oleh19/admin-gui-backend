<?php

namespace App\Domain\Customers\Actions\CustomersInfo;

use Ensi\CrmClient\Dto\CustomerInfo;
use Ensi\CrmClient\Dto\CustomerInfoForPatch;

class ReplaceCustomersInfoAction extends CustomersInfoAction
{
    /**
     * @param int $customerInfoId
     * @param array $fields
     * @return CustomerInfo
     * @throws \Ensi\CrmClient\ApiException
     */
    public function execute(int $customerInfoId, array $fields): CustomerInfo
    {
        return $this->customersInfoApi->patchCustomerInfo(
            $customerInfoId,
            new CustomerInfoForPatch($fields)
        )->getData();
    }
}
