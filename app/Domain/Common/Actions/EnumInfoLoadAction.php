<?php

namespace App\Domain\Common\Actions;

use App\Domain\Common\Data\Meta\Enum\AsyncEnumInfo;
use GuzzleHttp\Promise\Utils;

class EnumInfoLoadAction
{
    /**
     * @param AsyncEnumInfo[] $enums
     */
    public function execute(array $enums)
    {
        $promises = [];
        foreach ($enums as $enumInfo) {
            $promises[$enumInfo::class] = $enumInfo->requestAsync();
        }

        $responses = Utils::unwrap($promises);

        foreach ($enums as $enumInfo) {
            $enumInfo->processResponse($responses[$enumInfo::class]);
        }
    }
}
