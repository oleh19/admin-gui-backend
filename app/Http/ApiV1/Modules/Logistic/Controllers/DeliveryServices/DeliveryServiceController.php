<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryServices;

use App\Domain\Logistic\Actions\DeliveryServices\PatchDeliveryServiceAction;
use App\Http\ApiV1\Modules\Logistic\Queries\DeliveryServices\DeliveryServicesQuery;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryServices\PatchDeliveryServiceRequest;
use App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices\DeliveryServicesResource;
use Ensi\LogisticClient\ApiException;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class DeliveryServiceController
 * @package App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryServices
 */
class DeliveryServiceController
{
    /**
     * @param int $deliveryServiceId
     * @param PatchDeliveryServiceRequest $request
     * @param PatchDeliveryServiceAction $action
     * @return DeliveryServicesResource
     * @throws ApiException
     */
    public function patch(int $deliveryServiceId, PatchDeliveryServiceRequest $request, PatchDeliveryServiceAction $action): DeliveryServicesResource
    {
        return new DeliveryServicesResource($action->execute($deliveryServiceId, $request->validated()));
    }

    /**
     * @param int $deliveryServiceId
     * @param DeliveryServicesQuery $query
     * @return DeliveryServicesResource
     */
    public function get(int $deliveryServiceId, DeliveryServicesQuery $query): DeliveryServicesResource
    {
        return new DeliveryServicesResource($query->find($deliveryServiceId));
    }

    /**
     * @param DeliveryServicesQuery $query
     * @return AnonymousResourceCollection
     */
    public function search(DeliveryServicesQuery $query): AnonymousResourceCollection
    {
        return DeliveryServicesResource::collectPage($query->get());
    }

    /**
     * @param DeliveryServicesQuery $query
     * @return DeliveryServicesResource
     */
    public function searchOne(DeliveryServicesQuery $query): DeliveryServicesResource
    {
        return new DeliveryServicesResource($query->first());
    }
}
