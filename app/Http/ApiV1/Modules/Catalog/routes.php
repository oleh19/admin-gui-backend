<?php

use App\Http\ApiV1\Modules\Catalog\Controllers\BrandsController;
use App\Http\ApiV1\Modules\Catalog\Controllers\CategoriesController;
use App\Http\ApiV1\Modules\Catalog\Controllers\CountriesController;
use App\Http\ApiV1\Modules\Catalog\Controllers\DirectoryValuesController;
use App\Http\ApiV1\Modules\Catalog\Controllers\ManufacturersController;
use App\Http\ApiV1\Modules\Catalog\Controllers\ProductAttributesController;
use App\Http\ApiV1\Modules\Catalog\Controllers\ProductImagesController;
use App\Http\ApiV1\Modules\Catalog\Controllers\ProductsController;
use App\Http\ApiV1\Modules\Catalog\Controllers\ProductTypesController;
use App\Http\ApiV1\Modules\Catalog\Controllers\VariantGroupsController;
use Illuminate\Support\Facades\Route;

//region Brands

Route::post('brands:search', [BrandsController::class, 'search'])->name('searchBrands');
Route::post('brands', [BrandsController::class, 'create'])->name('createBrand');
Route::prefix('brands/{id}')
    ->group(function () {
        Route::get('', [BrandsController::class, 'get'])->name('getBrand');
        Route::put('', [BrandsController::class, 'replace'])->name('replaceBrand');
        Route::delete('', [BrandsController::class, 'delete'])->name('deleteBrand');
    });
Route::post('brands/{entityId}:upload-image', [BrandsController::class, 'uploadImage'])
    ->name('uploadBrandImage');
Route::post('brands/{entityId}:delete-image', [BrandsController::class, 'deleteImage'])
    ->name('deleteBrandImage');

//endregion

//region Countries

Route::post('countries:search', [CountriesController::class, 'search'])->name('searchCountries');
Route::post('countries', [CountriesController::class, 'create'])->name('createCountry');
Route::prefix('countries/{entityId}')
    ->group(function () {
        Route::get('', [CountriesController::class, 'get'])->name('getCountry');
        Route::put('', [CountriesController::class, 'replace'])->name('replaceCountry');
        Route::delete('', [CountriesController::class, 'delete'])->name('deleteCountry');
    });

//endregion

//region Manufacturers

Route::post('manufacturers:search', [ManufacturersController::class, 'search'])->name('searchManufacturers');
Route::post('manufacturers', [ManufacturersController::class, 'create'])->name('createManufacturer');
Route::prefix('manufacturers/{entityId}')
    ->group(function () {
        Route::get('', [ManufacturersController::class, 'get'])->name('getManufacturer');
        Route::put('', [ManufacturersController::class, 'replace'])->name('replaceManufacturer');
        Route::delete('', [ManufacturersController::class, 'delete'])->name('deleteManufacturer');
    });

//endregion

//region Product types

Route::post('product-types:search', [ProductTypesController::class, 'search'])->name('searchProductTypes');
Route::post('product-types', [ProductTypesController::class, 'create'])->name('createProductType');
Route::prefix('product-types/{entityId}')
    ->group(function () {
        Route::get('', [ProductTypesController::class, 'get'])->name('getProductType');
        Route::put('', [ProductTypesController::class, 'replace'])->name('replaceProductType');
        Route::delete('', [ProductTypesController::class, 'delete'])->name('deleteProductType');
    });

//endregion

//region Товарные атрибуты

Route::post('properties', [ProductAttributesController::class, 'create']);
Route::get('properties/{id}', [ProductAttributesController::class, 'get']);
Route::put('properties/{id}', [ProductAttributesController::class, 'replace']);
Route::delete('properties/{id}', [ProductAttributesController::class, 'delete']);
Route::post('properties:search', [ProductAttributesController::class, 'search']);

Route::post('properties/{id}:add-directory', [DirectoryValuesController::class, 'create']);
Route::get('properties/directory/{id}', [DirectoryValuesController::class, 'get']);
Route::put('properties/directory/{id}', [DirectoryValuesController::class, 'replace']);
Route::delete('properties/directory/{id}', [DirectoryValuesController::class, 'delete']);
Route::post('properties/directory:search', [DirectoryValuesController::class, 'search']);

//endregion

//region Категории

Route::post('categories', [CategoriesController::class, 'create']);
Route::get('categories/{id}', [CategoriesController::class, 'get']);
Route::put('categories/{id}', [CategoriesController::class, 'replace']);
Route::delete('categories/{id}', [CategoriesController::class, 'delete']);
Route::post('categories:search', [CategoriesController::class, 'search']);
Route::post('categories:tree', [CategoriesController::class, 'tree']);

//endregion

//region Товары

Route::post('products', [ProductsController::class, 'create']);
Route::get('products/{id}', [ProductsController::class, 'get']);
Route::put('products/{id}', [ProductsController::class, 'replace']);
Route::delete('products/{id}', [ProductsController::class, 'delete']);
Route::post('products:search', [ProductsController::class, 'search']);

Route::put('products/{id}/attributes', [ProductsController::class, 'replaceAttributes']);
Route::patch('products/{id}/attributes', [ProductsController::class, 'patchAttributes']);

Route::post('products/images', [ProductImagesController::class, 'create']);
Route::get('products/images/{id}', [ProductImagesController::class, 'get']);
Route::patch('products/images/{id}', [ProductImagesController::class, 'patch']);
Route::delete('products/images/{id}', [ProductImagesController::class, 'delete']);

Route::post('products/images/{id}:upload-image', [ProductImagesController::class, 'upload']);
Route::post('products/images:search', [ProductImagesController::class, 'search']);

//endregion

//region Группы товаров (склейки)

Route::post('variants', [VariantGroupsController::class, 'create']);
Route::get('variants/{id}', [VariantGroupsController::class, 'get']);
Route::put('variants/{id}', [VariantGroupsController::class, 'replace']);
Route::delete('variants/{id}', [VariantGroupsController::class, 'delete']);

Route::post('variants:search', [VariantGroupsController::class, 'search']);

//endregion
