<?php

namespace App\Domain\Units\Actions\SellerUsers;

use App\Domain\Units\Data\SellerUserData;
use Ensi\AdminAuthClient\Api\UsersApi;
use Ensi\AdminAuthClient\ApiException;
use Ensi\AdminAuthClient\Dto\User;
use Ensi\BuClient\Api\OperatorsApi;
use Ensi\BuClient\Dto\Operator;

/**
 * Class SellerUserAction
 * @package App\Domain\Units\Actions\SellerUsers
 */
abstract class SellerUserAction
{
    /**
     * SellerUser constructor.
     * @param  OperatorsApi  $operatorsApi
     * @param  UsersApi  $usersApi
     */
    public function __construct(protected OperatorsApi $operatorsApi, protected UsersApi $usersApi)
    {
    }

    /**
     * @param  Operator  $operator
     * @param  User|null  $user
     * @return SellerUserData
     * @throws ApiException
     */
    protected function prepareSellerUser(Operator $operator, ?User $user = null): SellerUserData
    {
        if (is_null($user)) {
            $user = $this->usersApi->getUser($operator->getUserId())->getData();
        }

        $operatorData = new SellerUserData($operator);
        if ($user) {
            $operatorData->setUser($user);
        }

        return $operatorData;
    }
}
