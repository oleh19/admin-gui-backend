<?php

namespace App\Domain\Contents\Actions;

use Ensi\CmsClient\Api\BannersApi;

class DeleteBannersAction
{
    public function __construct(
        private BannersApi $bannersApi
    ) {
    }

    public function execute(int $id): void
    {
        $this->bannersApi->deleteBanners($id);
    }
}
