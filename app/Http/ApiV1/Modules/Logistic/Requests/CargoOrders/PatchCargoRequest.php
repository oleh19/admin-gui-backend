<?php

namespace App\Http\ApiV1\Modules\Logistic\Requests\CargoOrders;

use App\Http\ApiV1\OpenApiGenerated\Dto\LogisticCargoStatusEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class PatchCargoRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'status' => [Rule::in(LogisticCargoStatusEnum::getAllowableEnumValues())],
            'is_problem' => ['sometimes', 'boolean', 'nullable'],
            'is_canceled' => ['sometimes', 'boolean', 'nullable'],
            'width' => ['nullable', 'numeric'],
            'height' => ['nullable', 'numeric'],
            'length' => ['nullable', 'numeric'],
            'weight' => ['nullable', 'numeric'],
            'shipping_problem_comment' => ['sometimes', 'string', 'nullable'],
        ];
    }
}
