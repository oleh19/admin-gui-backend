<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Http\ApiV1\Modules\Customers\Queries\CustomersPreferencesQuery;
use App\Http\ApiV1\Modules\Customers\Resources\CustomersPreferenceResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CustomersPreferencesController
{
    public function search(CustomersPreferencesQuery $query): AnonymousResourceCollection
    {
        return CustomersPreferenceResource::collectPage($query->get());
    }
}
