<?php

namespace App\Domain\Catalog\Actions\Products;

use Ensi\PimClient\Api\ImagesApi;

class DeleteProductImageAction
{
    public function __construct(private ImagesApi $api)
    {
    }

    public function execute(int $imageId): void
    {
        $this->api->deleteProductImage($imageId);
    }
}
