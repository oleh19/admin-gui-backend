<?php


namespace App\Http\ApiV1\Modules\Orders\Resources\Orders;

use App\Domain\Orders\Data\Orders\DeliveryData;
use App\Http\ApiV1\Modules\Orders\Resources\Orders\Data\TimeslotResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class DeliveriesResource
 * @package App\Http\ApiV1\Modules\Orders\Resources\OrdersDetail
 * @mixin DeliveryData
 */
class DeliveriesResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            "id" => $this->delivery->getId(),
            "order_id" => $this->delivery->getOrderId(),
            "number" => $this->delivery->getNumber(),
            "status" => $this->delivery->getStatus(),
            "status_at" => $this->dateTimeToIso($this->delivery->getStatusAt()),
            "date" => $this->dateToIso($this->delivery->getDate()),
            "timeslot" => TimeslotResource::make($this->delivery->getTimeslot()),
            "cost" => $this->delivery->getCost(),
            "width" => $this->delivery->getWidth(),
            "height" => $this->delivery->getHeight(),
            "length" => $this->delivery->getLength(),
            "weight" => $this->delivery->getWeight(),
            "created_at" => $this->dateTimeToIso($this->delivery->getCreatedAt()),
            "updated_at" => $this->dateTimeToIso($this->delivery->getUpdatedAt()),
            "shipments" => ShipmentsResource::collection($this->whenNotNull($this->getShipments())),
        ];
    }
}
