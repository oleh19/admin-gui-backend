<?php

namespace App\Domain\Units\Actions\Stores;

use Ensi\BuClient\Api\StoresApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\Store;
use Ensi\BuClient\Dto\StoreForReplace;

class ReplaceStoreAction
{
    public function __construct(
        private StoresApi $storesApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): Store
    {
        $store = new StoreForReplace($fields);

        return $this->storesApi->replaceStore($id, $store)->getData();
    }
}
