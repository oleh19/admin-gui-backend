<?php

use App\Http\ApiV1\Modules\Logistic\Tests\Factories\CargoOrdersFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LogisticClient\Dto\CargoOrderStatusEnum;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

// cargo-orders/cargo{id}:cancel
test("POST /api/v1/logistic/cargo-orders/{id}:cancel success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $cargoOrdersId = 1;
    $this->mockLogisticCargoOrdersApi()->allows([
        'cancelCargoOrders' => CargoOrdersFactory::new()->makeResponseOne(['status' => CargoOrderStatusEnum::CANCELED]),
    ]);

    postJson("/api/v1/logistic/cargo-orders/{$cargoOrdersId}:cancel")
        ->assertStatus(200)
        ->assertJsonPath('data.status', CargoOrderStatusEnum::CANCELED);
});

// cargo-orders/cargo:search
test("POST /api/v1/logistic/cargo-orders:search success", function () {
    $cargoOrdersId = 1;
    $cargoId = 2;

    $this->mockLogisticCargoOrdersApi()->allows([
        'searchCargoOrders' => CargoOrdersFactory::new()->makeResponse(['id' => $cargoOrdersId, 'cargo_id' => $cargoId]),
    ]);

    postJson("/api/v1/logistic/cargo-orders:search")
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $cargoOrdersId)
        ->assertJsonPath('data.0.cargo_id', $cargoId);
});
