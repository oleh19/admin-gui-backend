<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\Products;

use App\Http\ApiV1\OpenApiGenerated\Dto\PaginationTypeEnum;
use App\Http\ApiV1\Support\Pagination\Page;
use Ensi\PimClient\Api\SearchApi;
use Ensi\PimClient\Dto\IndexSearchProductsFilter;
use Ensi\PimClient\Dto\ProductFieldEnum;
use Illuminate\Http\Request;

class ProductsIndexQuery
{
    public function __construct(private Request $request, private SearchApi $api)
    {
    }

    public function get(): Page
    {
        $request = new IndexSearchProductsFilter();
        $request->setFields([ProductFieldEnum::ID]);

        $this->fillFilter($request);
        $pagination = $this->fillPagination($request);

        $response = $this->api->searchProductsIndex($request);

        $pagination['total'] = $response->getTotal();

        return new Page($response->getItems(), $pagination);
    }

    private function fillFilter(IndexSearchProductsFilter $filter): void
    {
        $items = $this->request->get('filter') ?? [];
        foreach (IndexSearchProductsFilter::setters() as $key => $method) {
            if (!isset($items[$key])) {
                continue;
            }

            $filter->{$method}($items[$key]);
        }
    }

    private function fillPagination(IndexSearchProductsFilter $request): array
    {
        $pagination = [
            'type' => PaginationTypeEnum::OFFSET,
            'limit' => config('pagination.default_limit'),
            'offset' => 0,
            'total' => 0,
        ];

        $input = $this->request->get('pagination');
        if (!$input) {
            return $pagination;
        }

        abort_if($input['type'] === PaginationTypeEnum::CURSOR, 400, 'Курсор не поддерживается');

        if (isset($input['limit'])) {
            $pagination['limit'] = (int)$input['limit'];
            $request->setLimit($pagination['limit']);
        }

        if (isset($input['offset'])) {
            $pagination['offset'] = (int)$input['offset'];
            $request->setOffset($pagination['offset']);
        }

        return $pagination;
    }
}
