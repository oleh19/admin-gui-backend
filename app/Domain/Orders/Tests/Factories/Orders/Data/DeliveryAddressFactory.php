<?php

namespace App\Domain\Orders\Tests\Factories\Orders\Data;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\OmsClient\Dto\Address;

class DeliveryAddressFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'address_string' => $this->faker->address(),
            'country_code' => 'RU',
            'post_index' => $this->faker->postcode(),
            'region' => $this->faker->city(),
            'region_guid' => $this->faker->uuid(),
            'area' => $this->faker->city(),
            'area_guid' => $this->faker->uuid(),
            'city' => $this->faker->city(),
            'city_guid' => $this->faker->uuid(),
            'street' => $this->faker->streetAddress(),
            'house' => $this->faker->buildingNumber(),
            'block' => $this->faker->numerify('##'),
            'flat' => $this->faker->numerify('##'),
            'floor' => $this->faker->numerify('##'),
            'porch' => $this->faker->numerify('##'),
            'intercom' => $this->faker->numerify('##-##'),
            'geo_lat' => (string)$this->faker->latitude(),
            'geo_lon' => (string)$this->faker->longitude(),
        ];
    }

    public function make(array $extra = []): Address
    {
        return new Address($this->makeArray($extra));
    }
}
