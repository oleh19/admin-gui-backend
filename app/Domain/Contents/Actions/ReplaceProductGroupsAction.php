<?php

namespace App\Domain\Contents\Actions;

use Ensi\CmsClient\Api\ProductGroupsApi;
use Ensi\CmsClient\Dto\ProductGroup;
use Ensi\CmsClient\Dto\ProductGroupForReplace;

class ReplaceProductGroupsAction
{
    public function __construct(
        protected ProductGroupsApi $productGroupApi
    ) {
    }

    public function execute(int $id, $data): ProductGroup
    {
        $requestProductGroupsApi = new ProductGroupForReplace($data);
        $responseProductGroupsApi =  $this->productGroupApi->replaceProductGroups($id, $requestProductGroupsApi);

        return $responseProductGroupsApi->getData();
    }
}
