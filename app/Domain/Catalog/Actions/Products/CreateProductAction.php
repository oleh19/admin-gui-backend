<?php

namespace App\Domain\Catalog\Actions\Products;

use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\CreateProductRequest;
use Ensi\PimClient\Dto\Product;

class CreateProductAction
{
    public function __construct(private ProductsApi $api)
    {
    }

    public function execute(array $fields): Product
    {
        $request = new CreateProductRequest($fields);

        return $this->api->createProduct($request)->getData();
    }
}
