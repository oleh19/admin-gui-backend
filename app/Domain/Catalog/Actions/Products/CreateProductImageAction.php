<?php

namespace App\Domain\Catalog\Actions\Products;

use Ensi\PimClient\Api\ImagesApi;
use Ensi\PimClient\Dto\CreateProductImageRequest;
use Ensi\PimClient\Dto\ProductImage;

class CreateProductImageAction
{
    public function __construct(private ImagesApi $api)
    {
    }

    public function execute(array $fields): ProductImage
    {
        $request = new CreateProductImageRequest($fields);

        return $this->api->createProductImage($request)->getData();
    }
}
