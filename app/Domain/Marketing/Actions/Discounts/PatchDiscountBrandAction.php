<?php

namespace App\Domain\Marketing\Actions\Discounts;

use Ensi\MarketingClient\Api\DiscountBrandsApi;
use Ensi\MarketingClient\Dto\DiscountBrand;
use Ensi\MarketingClient\Dto\DiscountBrandUpdatableProperties;

class PatchDiscountBrandAction
{
    public function __construct(protected DiscountBrandsApi $discountsApi)
    {
    }

    public function execute(int $discountBrandId, array $data): DiscountBrand
    {
        $requestDiscountBrandsApi  = new DiscountBrandUpdatableProperties($data);
        $responseDiscountBrandsApi = $this->discountsApi->patchDiscountBrand(
            $discountBrandId,
            $requestDiscountBrandsApi
        );

        return $responseDiscountBrandsApi->getData();
    }
}
