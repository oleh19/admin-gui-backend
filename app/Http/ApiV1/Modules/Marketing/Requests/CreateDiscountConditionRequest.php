<?php

namespace App\Http\ApiV1\Modules\Marketing\Requests;

use App\Http\ApiV1\OpenApiGenerated\Dto\MarketingDiscountConditionEnum as KeyEnum;
use App\Http\ApiV1\OpenApiGenerated\Dto\MarketingDiscountConditionTypeEnum as TypeEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateDiscountConditionRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'discount_id' => ['required', 'integer'],
            'type' => ['required', 'integer', Rule::in(TypeEnum::getAllowableEnumValues())],
            'condition' => ['nullable', 'array', 'required_unless:type,' . TypeEnum::FIRST_ORDER],
            'condition.*' => Rule::in(KeyEnum::getAllowableEnumValues()),

            /** Condition Min Price */
            'condition.' . KeyEnum::FIELD_MIN_PRICE => [
                'integer',
                'gt:0',
                'required_if:type,' . TypeEnum::MIN_PRICE_ORDER,
                'exclude_unless:type,' . TypeEnum::MIN_PRICE_ORDER,
            ],

            /** Condition Brands */
            'condition.' . KeyEnum::FIELD_BRANDS => [
                'array',
                'required_if:type,' . TypeEnum::MIN_PRICE_BRAND,
                'exclude_unless:type,' . TypeEnum::MIN_PRICE_BRAND,
            ],
            'condition.' . KeyEnum::FIELD_BRANDS . '.*' => ['integer'],

            /** Condition Categories */
            'condition.' . KeyEnum::FIELD_CATEGORIES => [
                'array',
                'required_if:type,' . TypeEnum::MIN_PRICE_CATEGORY,
                'exclude_unless:type,' . TypeEnum::MIN_PRICE_CATEGORY,
            ],
            'condition.' . KeyEnum::FIELD_CATEGORIES . '.*' => ['integer'],

            /** Condition Count */
            'condition.' . KeyEnum::FIELD_COUNT => [
                'integer',
                'gt:0',
                'required_if:type,' . TypeEnum::EVERY_UNIT_PRODUCT,
                'exclude_unless:type,' . TypeEnum::EVERY_UNIT_PRODUCT,
            ],
            'condition.' . KeyEnum::FIELD_OFFER => [
                'integer',
                'required_if:type,' . TypeEnum::EVERY_UNIT_PRODUCT,
                'exclude_unless:type,' . TypeEnum::EVERY_UNIT_PRODUCT,
            ],

            /** Condition Delivery Methods */
            'condition.' . KeyEnum::FIELD_DELIVERY_METHODS => [
                'array',
                'required_if:type,' . TypeEnum::DELIVERY_METHOD,
                'exclude_unless:type,' . TypeEnum::DELIVERY_METHOD,
            ],
            'condition.' . KeyEnum::FIELD_DELIVERY_METHODS . '.*' => ['integer'],

            /** Condition Payment Methods */
            'condition.' . KeyEnum::FIELD_PAYMENT_METHODS => [
                'array',
                'required_if:type,' . TypeEnum::PAY_METHOD,
                'exclude_unless:type,' . TypeEnum::PAY_METHOD,
            ],
            'condition.' . KeyEnum::FIELD_PAYMENT_METHODS . '.*' => ['integer'],

            /** Condition Regions */
            'condition.' . KeyEnum::FIELD_REGIONS => [
                'array',
                'required_if:type,' . TypeEnum::REGION,
                'exclude_unless:type,' . TypeEnum::REGION,
            ],
            'condition.' . KeyEnum::FIELD_REGIONS . '.*' => ['integer'],

            /** Condition Order Sequence Number */
            'condition.' . KeyEnum::FIELD_ORDER_SEQUENCE_NUMBER => [
                'integer',
                'gt:0',
                'required_if:type,' . TypeEnum::ORDER_SEQUENCE_NUMBER,
                'exclude_unless:type,' . TypeEnum::ORDER_SEQUENCE_NUMBER,
            ],

            /** Condition Customers */
            'condition.' . KeyEnum::FIELD_CUSTOMERS => [
                'array',
                'required_if:type,' . TypeEnum::CUSTOMER,
                'exclude_unless:type,' . TypeEnum::CUSTOMER,
            ],
            'condition.' . KeyEnum::FIELD_CUSTOMERS . '.*' => ['integer'],

            /** Condition Synergies */
            'condition.' . KeyEnum::FIELD_SYNERGY => [
                'array',
                'required_if:type,' . TypeEnum::DISCOUNT_SYNERGY,
                'exclude_unless:type,' . TypeEnum::DISCOUNT_SYNERGY,
            ],
            'condition.' . KeyEnum::FIELD_SYNERGY . '.*' => ['integer'],
        ];
    }
}
