<?php

namespace App\Http\ApiV1\Modules\Logistic\Requests\Geos;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

/**
 * Class GetFederalDistrictRequest
 * @package App\Http\ApiV1\Modules\Logistic\Requests\Geos
 */
class GetFederalDistrictRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'include' => ['nullable', 'string'],
      ];
    }
}
