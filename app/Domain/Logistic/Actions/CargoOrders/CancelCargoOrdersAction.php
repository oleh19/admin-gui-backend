<?php

namespace App\Domain\Logistic\Actions\CargoOrders;

use Ensi\LogisticClient\Api\CargoOrdersApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\CargoOrders;

class CancelCargoOrdersAction
{
    public function __construct(protected CargoOrdersApi $cargoOrdersApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $cargoOrderId): CargoOrders
    {
        return $this->cargoOrdersApi->cancelCargoOrders($cargoOrderId)->getData();
    }
}
