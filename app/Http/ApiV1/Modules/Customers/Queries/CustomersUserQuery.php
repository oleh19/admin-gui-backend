<?php

namespace App\Http\ApiV1\Modules\Customers\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderFirstTrait;
use Ensi\CustomerAuthClient\Api\UsersApi;
use Ensi\CustomerAuthClient\ApiException;
use Ensi\CustomerAuthClient\Dto\SearchUsersRequest;
use Ensi\CustomerAuthClient\Dto\SearchUsersResponse;
use Ensi\CustomerAuthClient\Dto\UserResponse;
use Illuminate\Http\Request;

/**
 * Class CustomersUserQuery
 * @package App\Http\ApiV1\Modules\Customers\Queries
 */
class CustomersUserQuery extends CrmQuery
{
    use QueryBuilderFindTrait;
    use QueryBuilderFirstTrait;

    /**
     * CustomersUserQuery constructor.
     * @param  Request  $httpRequest
     * @param  UsersApi  $usersApi
     */
    public function __construct(
        protected Request $httpRequest,
        protected UsersApi $usersApi
    ) {
        parent::__construct($httpRequest, SearchUsersRequest::class);
    }

    /**
     * @return string
     */
    protected function requestFirstClass(): string
    {
        return $this->requestGetClass;
    }

    /**
     * @param int $id
     * @return UserResponse
     * @throws ApiException
     */
    protected function searchById($id): UserResponse
    {
        return $this->usersApi->getUser($id, $this->httpRequest->get('include'));
    }

    /**
     * @param $request
     * @return SearchUsersResponse
     * @throws ApiException
     */
    protected function search($request): SearchUsersResponse
    {
        return $this->usersApi->searchUsers($request);
    }

    /**
     * @param SearchUsersRequest $request
     * @return UserResponse
     * @throws ApiException
     */
    protected function searchOne($request): UserResponse
    {
        return $this->usersApi->searchOneUser($request);
    }
}
