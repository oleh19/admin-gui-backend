<?php


namespace App\Http\ApiV1\Modules\Marketing\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\MarketingClient\Dto\PromoCode;

/**
 * Class PromoCodesResource
 * @package App\Http\ApiV1\Modules\Marketing\Resources
 * @mixin PromoCode
 */
class PromoCodesResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),
            'creator_id' => $this->getCreatorId(),
            'seller_id' => $this->getSellerId(),
            'owner_id' => $this->getOwnerId(),
            'name' => $this->getName(),
            'code' => $this->getCode(),
            'counter' => $this->getCounter(),
            'start_date' => $this->dateTimeToIso($this->getStartDate()),
            'end_date' => $this->dateTimeToIso($this->getEndDate()),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'status' => $this->getStatus(),
            'type' => $this->getType(),
            'discount_id' => $this->getDiscountId(),
        ];
    }
}
