<?php

namespace App\Http\ApiV1\Modules\Units\Queries\SellerUsers;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\AdminAuthClient\Api\UsersApi;
use Ensi\AdminAuthClient\Dto\RequestBodyPagination;
use Ensi\AdminAuthClient\Dto\SearchRolesRequest;
use Ensi\BuClient\Api\OperatorsApi;
use Illuminate\Http\Request;

class SellerUserRolesQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;

    public function __construct(
        protected Request  $httpRequest,
        protected UsersApi $usersApi,
        protected OperatorsApi $operatorsApi,
    ) {
        parent::__construct($httpRequest);
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function requestGetClass(): string
    {
        return SearchRolesRequest::class;
    }

    protected function search($request)
    {
        if (isset($request->getFilter()->id)) {
            $operator = $this->operatorsApi->getOperator($request->getFilter()->id)->getData();
            $request->setFilter(['id' => $operator->getUserId()]);
        }

        return $this->usersApi->searchRoles($request);
    }
}
