<?php

namespace App\Http\ApiV1\Modules\Logistic\Requests\DeliveryPrices;

use App\Http\ApiV1\OpenApiGenerated\Dto\LogisticDeliveryMethodEnum;
use App\Http\ApiV1\OpenApiGenerated\Dto\LogisticDeliveryServiceEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

/**
 * Class PatchDeliveryPriceRequest
 * @package App\Http\ApiV1\Modules\Logistic\Requests\DeliveryPrices
 */
class PatchDeliveryPriceRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'federal_district_id' => ['nullable', 'integer'],
            'region_id' => ['nullable', 'integer'],
            'region_guid' => ['nullable', 'string'],
            'delivery_service' => ['nullable', Rule::in(LogisticDeliveryServiceEnum::getAllowableEnumValues())],
            'delivery_method' => ['nullable', Rule::in(LogisticDeliveryMethodEnum::getAllowableEnumValues())],
            'price' => ['nullable', 'integer'],
      ];
    }
}
