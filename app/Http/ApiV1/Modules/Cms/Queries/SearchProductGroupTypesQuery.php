<?php

namespace App\Http\ApiV1\Modules\Cms\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\CmsClient\Api\ProductGroupsApi;
use Ensi\CmsClient\Dto\RequestBodyPagination;
use Ensi\CmsClient\Dto\SearchProductGroupTypesRequest;
use Illuminate\Http\Request;

class SearchProductGroupTypesQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;

    public function __construct(
        Request $httpRequest,
        protected ProductGroupsApi $productGroupsApi,
    ) {
        parent::__construct($httpRequest);
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function requestGetClass(): string
    {
        return SearchProductGroupTypesRequest::class;
    }

    protected function search($request)
    {
        return $this->productGroupsApi->searchProductGroupTypes($request);
    }
}
