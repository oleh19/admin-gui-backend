<?php

namespace App\Domain\Orders\Tests\Factories\PaymentSystems;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\OmsClient\Dto\PaymentSystem;
use Ensi\OmsClient\Dto\PaymentSystemEnum;
use Ensi\OmsClient\Dto\PaymentSystemsResponse;

class PaymentSystemFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->randomElement(PaymentSystemEnum::getAllowableEnumValues()),
            'name' => $this->faker->text(20),
        ];
    }

    public function make(array $extra = []): PaymentSystem
    {
        return new PaymentSystem($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extra = []): PaymentSystemsResponse
    {
        return new PaymentSystemsResponse([
            'data' => [$this->make($this->makeArray($extra))],
        ]);
    }
}
