<?php

namespace App\Domain\Catalog\Actions\Classifiers\Manufacturers;

use Ensi\PimClient\Api\ManufacturersApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\Manufacturer;
use Ensi\PimClient\Dto\ReplaceManufacturerRequest;

class ReplaceManufacturerAction
{
    public function __construct(
        private ManufacturersApi $manufacturersApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $countryId, array $fields): Manufacturer
    {
        $request = new ReplaceManufacturerRequest($fields);

        return $this->manufacturersApi->replaceManufacturer($countryId, $request)->getData();
    }
}
