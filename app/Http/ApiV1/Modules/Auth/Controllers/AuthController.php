<?php

namespace App\Http\ApiV1\Modules\Auth\Controllers;

use App\Domain\Auth\Actions\LoginAction;
use App\Domain\Auth\Actions\LogoutAction;
use App\Domain\Auth\Actions\RefreshAction;
use App\Http\ApiV1\Modules\Auth\Requests\LoginRequest;
use App\Http\ApiV1\Modules\Auth\Requests\RefreshRequest;
use App\Http\ApiV1\Modules\Auth\Resources\TokensResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Request;

class AuthController
{
    public function login(LoginRequest $request, LoginAction $action)
    {
        return new TokensResource($action->execute($request->validated()));
    }

    public function logout(Request $request, LogoutAction $action)
    {
        $action->execute($request->user()->getRememberToken());

        return new EmptyResource();
    }

    public function refresh(RefreshRequest $request, RefreshAction $action)
    {
        return new TokensResource($action->execute($request->validated()));
    }
}
