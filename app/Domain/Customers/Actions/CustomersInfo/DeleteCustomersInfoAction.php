<?php

namespace App\Domain\Customers\Actions\CustomersInfo;

class DeleteCustomersInfoAction extends CustomersInfoAction
{
    /**
     * @param int $customerInfoId
     * @throws \Ensi\CrmClient\ApiException
     */
    public function execute(int $customerInfoId): void
    {
        $this->customersInfoApi->deleteCustomerInfo($customerInfoId);
    }
}
