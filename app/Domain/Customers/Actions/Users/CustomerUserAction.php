<?php

namespace App\Domain\Customers\Actions\Users;

use Ensi\CustomerAuthClient\Api\UsersApi;

/**
 * Class CustomerUserAction
 * @package App\Domain\Customers\Actions\Users
 */
abstract class CustomerUserAction
{
    /**
     * CustomerUserAction constructor.
     * @param  UsersApi  $usersApi
     */
    public function __construct(protected UsersApi $usersApi)
    {
    }
}
