<?php

namespace App\Domain\Logistic\Actions\DeliveryPrices;

use Ensi\LogisticClient\Api\DeliveryPricesApi;
use Ensi\LogisticClient\ApiException;

/**
 * Class DeleteDeliveryPriceAction
 * @package App\Domain\Logistic\Actions\DeliveryPrices
 */
class DeleteDeliveryPriceAction
{
    public function __construct(protected DeliveryPricesApi $deliveryPricesApi)
    {
    }

    /**
     * @param  int  $deliveryPriceId
     * @throws ApiException
     */
    public function execute(int $deliveryPriceId): void
    {
        $this->deliveryPricesApi->deleteDeliveryPrice($deliveryPriceId);
    }
}
