<?php


namespace App\Http\ApiV1\Modules\Logistic\Queries\DeliveryKpis;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderFirstTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\LogisticClient\Api\KpiApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\DeliveryKpiPptResponse;
use Ensi\LogisticClient\Dto\RequestBodyPagination;
use Ensi\LogisticClient\Dto\SearchDeliveryKpiPptRequest;
use Ensi\LogisticClient\Dto\SearchDeliveryKpiPptResponse;
use Illuminate\Http\Request;

/**
 * Class DeliveryKpiPptQuery
 * @package App\Http\ApiV1\Modules\Logistic\Queries\DeliveryKpis
 */
class DeliveryKpiPptQuery extends QueryBuilder
{
    use QueryBuilderFindTrait;
    use QueryBuilderFirstTrait;
    use QueryBuilderGetTrait;

    /**
     * DeliveryKpiPptQuery constructor.
     * @param Request $httpRequest
     * @param KpiApi $kpiApi
     */
    public function __construct(protected Request $httpRequest, protected KpiApi $kpiApi)
    {
        parent::__construct($httpRequest);
    }

    /**
     * @return string
     */
    protected function requestFirstClass(): string
    {
        return SearchDeliveryKpiPptRequest::class;
    }

    /**
     * @return string
     */
    protected function requestGetClass(): string
    {
        return SearchDeliveryKpiPptRequest::class;
    }

    /**
     * @return string
     */
    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    /**
     * @param $id
     * @return DeliveryKpiPptResponse
     * @throws ApiException
     */
    public function searchById($id): DeliveryKpiPptResponse
    {
        return $this->kpiApi->getDeliveryKpiPpt($id);
    }

    /**
     * @param $request
     * @return SearchDeliveryKpiPptResponse
     * @throws ApiException
     */
    public function search($request): SearchDeliveryKpiPptResponse
    {
        return $this->kpiApi->searchDeliveryKpiPpts($request);
    }

    /**
     * @param $request
     * @return DeliveryKpiPptResponse
     * @throws ApiException
     */
    public function searchOne($request): DeliveryKpiPptResponse
    {
        return $this->kpiApi->searchOneDeliveryKpiPpt($request);
    }
}
