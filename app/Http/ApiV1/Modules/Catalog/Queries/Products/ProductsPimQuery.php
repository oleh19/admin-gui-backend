<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\Products;

use App\Http\ApiV1\Modules\Catalog\Queries\PimQuery;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\ProductResponse;
use Ensi\PimClient\Dto\SearchProductsRequest;
use Ensi\PimClient\Dto\SearchProductsResponse;
use Illuminate\Http\Request;

class ProductsPimQuery extends PimQuery
{
    public function __construct(Request $request, private ProductsApi $api)
    {
        parent::__construct($request, SearchProductsRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function searchById($id): ProductResponse
    {
        return $this->api->getProduct($id, $this->getInclude());
    }

    protected function search($request): SearchProductsResponse
    {
        return $this->api->searchProducts($request);
    }
}
