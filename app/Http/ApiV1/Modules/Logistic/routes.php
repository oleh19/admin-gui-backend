<?php

use App\Http\ApiV1\Modules\Logistic\Controllers\CargoOrders\CargoController;
use App\Http\ApiV1\Modules\Logistic\Controllers\CargoOrders\CargoOrdersController;
use App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryKpis\DeliveryKpiController;
use App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryKpis\DeliveryKpiCtController;
use App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryKpis\DeliveryKpiPptController;
use App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryPrices\DeliveryPricesController;
use App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryPrices\TariffsController;
use App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryServices\DeliveryServiceController;
use App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryServices\DeliveryServiceDirectoryController;
use App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryServices\DeliveryServiceDocumentFilesController;
use App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryServices\DeliveryServiceDocumentsController;
use App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryServices\DeliveryServiceManagersController;
use App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryServices\DeliveryServicePaymentMethodLinksController;
use App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryServices\PointsController;
use App\Http\ApiV1\Modules\Logistic\Controllers\Geos\FederalDistrictsController;
use App\Http\ApiV1\Modules\Logistic\Controllers\Geos\RegionsController;
use Illuminate\Support\Facades\Route;

Route::post('points:search', [PointsController::class, 'search']);
Route::post('point-enum-values:search', [PointsController::class, 'searchEnumValues'])->name('logistic.searchPointEnumValues');

Route::get('delivery-kpi', [DeliveryKpiController::class, 'get']);
Route::patch('delivery-kpi', [DeliveryKpiController::class, 'patch']);
Route::put('delivery-kpi', [DeliveryKpiController::class, 'replace']);

Route::post('delivery-kpi-ct:search', [DeliveryKpiCtController::class, 'search']);
Route::post('delivery-kpi-ct:search-one', [DeliveryKpiCtController::class, 'searchOne']);
Route::get('delivery-kpi-ct/{sellerId}', [DeliveryKpiCtController::class, 'get']);
Route::post('delivery-kpi-ct/{sellerId}', [DeliveryKpiCtController::class, 'create']);
Route::put('delivery-kpi-ct/{sellerId}', [DeliveryKpiCtController::class, 'replace']);
Route::delete('delivery-kpi-ct/{sellerId}', [DeliveryKpiCtController::class, 'delete']);

Route::post('delivery-kpi-ppt:search', [DeliveryKpiPptController::class, 'search']);
Route::post('delivery-kpi-ppt:search-one', [DeliveryKpiPptController::class, 'searchOne']);
Route::get('delivery-kpi-ppt/{sellerId}', [DeliveryKpiPptController::class, 'get']);
Route::post('delivery-kpi-ppt/{sellerId}', [DeliveryKpiPptController::class, 'create']);
Route::put('delivery-kpi-ppt/{sellerId}', [DeliveryKpiPptController::class, 'replace']);
Route::delete('delivery-kpi-ppt/{sellerId}', [DeliveryKpiPptController::class, 'delete']);

Route::get('delivery-methods', [DeliveryServiceDirectoryController::class, 'getDeliveryMethods']);
Route::get('delivery-service-statuses', [DeliveryServiceDirectoryController::class, 'getDeliveryServiceStatuses']);
Route::get('shipment-methods', [DeliveryServiceDirectoryController::class, 'getShipmentMethods']);

Route::get('delivery-services/{deliveryServiceId}', [DeliveryServiceController::class, 'get']);
Route::patch('delivery-services/{deliveryServiceId}', [DeliveryServiceController::class, 'patch']);
Route::post('delivery-services:search', [DeliveryServiceController::class, 'search']);
Route::post('delivery-services:search-one', [DeliveryServiceController::class, 'searchOne']);

Route::post('delivery-services/{deliveryServiceId}:add-payment-methods', [DeliveryServicePaymentMethodLinksController::class, 'add']);
Route::post('delivery-services/{deliveryServiceId}:delete-payment-method', [DeliveryServicePaymentMethodLinksController::class, 'delete']);

Route::post('delivery-service-documents', [DeliveryServiceDocumentsController::class, 'create']);
Route::get('delivery-service-documents/{deliveryServiceDocumentId}', [DeliveryServiceDocumentsController::class, 'get']);
Route::put('delivery-service-documents/{deliveryServiceDocumentId}', [DeliveryServiceDocumentsController::class, 'replace']);
Route::patch('delivery-service-documents/{deliveryServiceDocumentId}', [DeliveryServiceDocumentsController::class, 'patch']);
Route::delete('delivery-service-documents/{deliveryServiceDocumentId}', [DeliveryServiceDocumentsController::class, 'delete']);
Route::post('delivery-service-documents:search', [DeliveryServiceDocumentsController::class, 'search']);
Route::post('delivery-service-documents:search-one', [DeliveryServiceDocumentsController::class, 'searchOne']);
Route::post('delivery-service-documents/{deliveryServiceDocumentId}:upload-file', [DeliveryServiceDocumentFilesController::class, 'upload']);
Route::post('delivery-service-documents/{deliveryServiceDocumentId}:delete-file', [DeliveryServiceDocumentFilesController::class, 'delete']);

Route::post('delivery-service-managers', [DeliveryServiceManagersController::class, 'create']);
Route::get('delivery-service-managers/{deliveryServiceManagerId}', [DeliveryServiceManagersController::class, 'get']);
Route::put('delivery-service-managers/{deliveryServiceManagerId}', [DeliveryServiceManagersController::class, 'replace']);
Route::patch('delivery-service-managers/{deliveryServiceManagerId}', [DeliveryServiceManagersController::class, 'patch']);
Route::delete('delivery-service-managers/{deliveryServiceManagerId}', [DeliveryServiceManagersController::class, 'delete']);
Route::post('delivery-service-managers:search', [DeliveryServiceManagersController::class, 'search']);
Route::post('delivery-service-managers:search-one', [DeliveryServiceManagersController::class, 'searchOne']);

Route::post('federal-districts', [FederalDistrictsController::class, 'create']);
Route::get('federal-districts/{federalDistrictId}', [FederalDistrictsController::class, 'get']);
Route::put('federal-districts/{federalDistrictId}', [FederalDistrictsController::class, 'replace']);
Route::patch('federal-districts/{federalDistrictId}', [FederalDistrictsController::class, 'patch']);
Route::delete('federal-districts/{federalDistrictId}', [FederalDistrictsController::class, 'delete']);
Route::post('federal-districts:search', [FederalDistrictsController::class, 'search']);
Route::post('federal-districts:search-one', [FederalDistrictsController::class, 'searchOne']);

Route::post('regions', [RegionsController::class, 'create']);
Route::get('regions/{regionId}', [RegionsController::class, 'get']);
Route::put('regions/{regionId}', [RegionsController::class, 'replace']);
Route::patch('regions/{regionId}', [RegionsController::class, 'patch']);
Route::delete('regions/{regionId}', [RegionsController::class, 'delete']);
Route::post('regions:search', [RegionsController::class, 'search']);
Route::post('regions:search-one', [RegionsController::class, 'searchOne']);

Route::post('delivery-prices', [DeliveryPricesController::class, 'create']);
Route::get('delivery-prices/{deliveryPriceId}', [DeliveryPricesController::class, 'get']);
Route::put('delivery-prices/{deliveryPriceId}', [DeliveryPricesController::class, 'replace']);
Route::patch('delivery-prices/{deliveryPriceId}', [DeliveryPricesController::class, 'patch']);
Route::delete('delivery-prices/{deliveryPriceId}', [DeliveryPricesController::class, 'delete']);
Route::post('delivery-prices:search', [DeliveryPricesController::class, 'search']);
Route::post('delivery-prices:search-one', [DeliveryPricesController::class, 'searchOne']);

Route::post('tariff-enum-values:search', [TariffsController::class, 'searchEnumValues'])->name('logistic.searchTariffEnumValues');

Route::patch('cargo/{id}', [CargoController::class, 'patch']);
Route::post('cargo/{id}:cancel', [CargoController::class, 'cancel']);
Route::post('cargo:search', [CargoController::class, 'search']);

Route::post('cargo-orders/{id}:cancel', [CargoOrdersController::class, 'cancel']);
Route::post('cargo-orders:search', [CargoOrdersController::class, 'search']);
