<?php

use App\Http\ApiV1\Modules\Cms\Controllers\BannerFilesController;
use App\Http\ApiV1\Modules\Cms\Controllers\BannersController;
use App\Http\ApiV1\Modules\Cms\Controllers\BannerTypesController;
use App\Http\ApiV1\Modules\Cms\Controllers\MenusController;
use App\Http\ApiV1\Modules\Cms\Controllers\MenuTreesController;
use App\Http\ApiV1\Modules\Cms\Controllers\ProductGroupFilesController;
use App\Http\ApiV1\Modules\Cms\Controllers\ProductGroupFiltersController;
use App\Http\ApiV1\Modules\Cms\Controllers\ProductGroupsController;
use App\Http\ApiV1\Modules\Cms\Controllers\ProductGroupTypesController;
use Illuminate\Support\Facades\Route;

Route::post('menus:search', [MenusController::class, 'search']);
Route::post('menus:search-one', [MenusController::class, 'searchOne']);

Route::prefix('menus/{id}')->where(['id' => '[0-9]+'])->group(function () {
    Route::put('trees', [MenuTreesController::class, 'update']);
});

Route::post('product-groups:search', [ProductGroupsController::class, 'search']);
Route::post('product-groups:search-one', [ProductGroupsController::class, 'searchOne']);
Route::post('product-groups', [ProductGroupsController::class, 'create']);
Route::put('product-groups/{id}', [ProductGroupsController::class, 'update'])->where(['id' => '[0-9]+']);
Route::delete('product-groups/{id}', [ProductGroupsController::class, 'delete'])->where(['id' => '[0-9]+']);
Route::post('product-groups/{id}:upload-file', [ProductGroupFilesController::class, 'upload'])->where(['id' => '[0-9]+']);
Route::post('product-groups/{id}:delete-file', [ProductGroupFilesController::class, 'delete'])->where(['id' => '[0-9]+']);

Route::post('product-group-types:search', [ProductGroupTypesController::class, 'search']);

Route::post('product-group-filters:search', [ProductGroupFiltersController::class, 'search']);

Route::post('banners:search', [BannersController::class, 'search']);
Route::post('banners:search-one', [BannersController::class, 'searchOne']);
Route::post('banners', [BannersController::class, 'create']);
Route::put('banners/{id}', [BannersController::class, 'update'])->where(['id' => '[0-9]+']);
Route::delete('banners/{id}', [BannersController::class, 'delete'])->where(['id' => '[0-9]+']);
Route::post('banners/{id}:upload-file', [BannerFilesController::class, 'upload'])->where(['id' => '[0-9]+']);
Route::post('banners/{id}:delete-file', [BannerFilesController::class, 'delete'])->where(['id' => '[0-9]+']);

Route::post('banner-types:search', [BannerTypesController::class, 'search']);
