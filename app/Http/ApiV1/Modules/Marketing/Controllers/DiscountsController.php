<?php


namespace App\Http\ApiV1\Modules\Marketing\Controllers;

use App\Domain\Marketing\Actions\Discounts\CreateDiscountAction;
use App\Domain\Marketing\Actions\Discounts\DeleteDiscountAction;
use App\Domain\Marketing\Actions\Discounts\MassStatusUpdateAction;
use App\Domain\Marketing\Actions\Discounts\PatchDiscountAction;
use App\Domain\Marketing\Actions\Discounts\ReplaceDiscountAction;
use App\Http\ApiV1\Modules\Marketing\Queries\DiscountsQuery;
use App\Http\ApiV1\Modules\Marketing\Requests\CreateDiscountRequest;
use App\Http\ApiV1\Modules\Marketing\Requests\MassStatusUpdateRequest;
use App\Http\ApiV1\Modules\Marketing\Requests\PatchDiscountRequest;
use App\Http\ApiV1\Modules\Marketing\Requests\ReplaceDiscountRequest;
use App\Http\ApiV1\Modules\Marketing\Resources\DiscountsResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class DiscountsController
{
    public function findById(int $discountId, DiscountsQuery $query)
    {
        return new DiscountsResource($query->find($discountId));
    }

    public function search(DiscountsQuery $query)
    {
        return DiscountsResource::collectPage($query->get());
    }

    public function create(CreateDiscountRequest $request, CreateDiscountAction $action): DiscountsResource
    {
        return new DiscountsResource($action->execute($request->validated()));
    }

    public function replace(
        int $discountId,
        ReplaceDiscountRequest $request,
        ReplaceDiscountAction $action
    ): DiscountsResource {
        return new DiscountsResource($action->execute($discountId, $request->validated()));
    }

    public function patch(
        int $discountId,
        PatchDiscountRequest $request,
        PatchDiscountAction $action
    ): DiscountsResource {
        return new DiscountsResource($action->execute($discountId, $request->validated()));
    }

    public function delete(int $discountId, DeleteDiscountAction $action): EmptyResource
    {
        $action->execute($discountId);

        return new EmptyResource();
    }

    public function massStatusUpdate(MassStatusUpdateRequest $request, MassStatusUpdateAction $action)
    {
        $action->execute($request->validated());

        return new EmptyResource();
    }
}
