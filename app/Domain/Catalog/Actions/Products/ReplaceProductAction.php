<?php

namespace App\Domain\Catalog\Actions\Products;

use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\Product;
use Ensi\PimClient\Dto\ReplaceProductRequest;

class ReplaceProductAction
{
    public function __construct(private ProductsApi $api)
    {
    }

    public function execute(int $productId, array $fields): Product
    {
        $request = new ReplaceProductRequest($fields);

        return $this->api->replaceProduct($productId, $request)->getData();
    }
}
