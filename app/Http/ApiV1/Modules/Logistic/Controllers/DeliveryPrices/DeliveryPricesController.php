<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryPrices;

use App\Domain\Logistic\Actions\DeliveryPrices\CreateDeliveryPriceAction;
use App\Domain\Logistic\Actions\DeliveryPrices\DeleteDeliveryPriceAction;
use App\Domain\Logistic\Actions\DeliveryPrices\PatchDeliveryPriceAction;
use App\Domain\Logistic\Actions\DeliveryPrices\ReplaceDeliveryPriceAction;
use App\Http\ApiV1\Modules\Logistic\Queries\DeliveryPrices\DeliveryPricesQuery;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryPrices\CreateOrReplaceDeliveryPriceRequest;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryPrices\PatchDeliveryPriceRequest;
use App\Http\ApiV1\Modules\Logistic\Resources\DeliveryPrices\DeliveryPricesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Ensi\LogisticClient\ApiException;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class DeliveryPricesController
 * @package App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryPrices
 */
class DeliveryPricesController
{
    /**
     * @param  CreateOrReplaceDeliveryPriceRequest  $request
     * @param  CreateDeliveryPriceAction  $action
     * @return DeliveryPricesResource
     * @throws ApiException
     */
    public function create(CreateOrReplaceDeliveryPriceRequest $request, CreateDeliveryPriceAction $action): DeliveryPricesResource
    {
        return new DeliveryPricesResource($action->execute($request->validated()));
    }

    /**
     * @param  int  $deliveryPriceId
     * @param  CreateOrReplaceDeliveryPriceRequest  $request
     * @param  ReplaceDeliveryPriceAction  $action
     * @return DeliveryPricesResource
     * @throws ApiException
     */
    public function replace(
        int $deliveryPriceId,
        CreateOrReplaceDeliveryPriceRequest $request,
        ReplaceDeliveryPriceAction $action
    ): DeliveryPricesResource {
        return new DeliveryPricesResource($action->execute($deliveryPriceId, $request->validated()));
    }

    /**
     * @param  int  $deliveryPriceId
     * @param  PatchDeliveryPriceRequest  $request
     * @param  PatchDeliveryPriceAction  $action
     * @return DeliveryPricesResource
     * @throws ApiException
     */
    public function patch(int $deliveryPriceId, PatchDeliveryPriceRequest $request, PatchDeliveryPriceAction $action): DeliveryPricesResource
    {
        return new DeliveryPricesResource($action->execute($deliveryPriceId, $request->validated()));
    }

    /**
     * @param  int  $deliveryPriceId
     * @param  DeleteDeliveryPriceAction  $action
     * @return EmptyResource
     * @throws ApiException
     */
    public function delete(int $deliveryPriceId, DeleteDeliveryPriceAction $action): EmptyResource
    {
        $action->execute($deliveryPriceId);

        return new EmptyResource();
    }

    /**
     * @param  int  $federalDistrictId
     * @param  DeliveryPricesQuery  $query
     * @return DeliveryPricesResource
     */
    public function get(
        int $federalDistrictId,
        DeliveryPricesQuery $query
    ): DeliveryPricesResource {
        return new DeliveryPricesResource($query->find($federalDistrictId));
    }

    /**
     * @param  DeliveryPricesQuery  $query
     * @return AnonymousResourceCollection
     */
    public function search(DeliveryPricesQuery $query): AnonymousResourceCollection
    {
        return DeliveryPricesResource::collectPage(
            $query->get()
        );
    }

    /**
     * @param  DeliveryPricesQuery  $query
     * @return DeliveryPricesResource
     */
    public function searchOne(DeliveryPricesQuery $query): DeliveryPricesResource
    {
        return new DeliveryPricesResource($query->first());
    }
}
