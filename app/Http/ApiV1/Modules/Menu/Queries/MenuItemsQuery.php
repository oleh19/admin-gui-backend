<?php

namespace App\Http\ApiV1\Modules\Menu\Queries;

use App\Domain\Auth\Models\User;
use App\Http\ApiV1\Modules\Menu\Data\MenuData;
use Ensi\AdminAuthClient\Dto\RoleEnum;
use Illuminate\Support\Facades\Auth;

/**
 * Class MenuItemsQuery
 * @package App\Http\ApiV1\Modules\Menu\Queries
 */
class MenuItemsQuery
{
    protected User $user;

    /**
     * MenuItemsQuery constructor.
     * @param  User  $user
     */
    public function __construct()
    {
        $this->user = Auth::user();
    }

    /**
     * @return array|string[]
     */
    public function getMenuItems(): array
    {
        $menu = MenuData::menu();
        $menuItems = $this->getMenuWithAccessRight($menu, [RoleEnum::SUPER_ADMIN]);

        return $this->getMenuItemCodes($menuItems);
    }

    /**
     * Получить пункты меню текущего уровня вложенности, к которому у пользователя есть права доступа
     * @param  array  $menu - пункты меню для текущего уровня вложенности и все вложенные в них пункты меню
     * @param  array  $parentRoles  - роли родительского пункта меню
     * (если у конкретного пункта меню не указаны роли для проверки, то проверим по ролям его родителя)
     * @return array
     */
    protected function getMenuWithAccessRight(array &$menu, array $parentRoles): array
    {
        foreach ($menu as $code => &$menuItem) {
            $roles = $menuItem['roleIds'] ?? $parentRoles;
            if (!$this->user->hasRole($roles)) {
                unset($menu[$code]);

                continue;
            }
            if (!empty($menuItem['items'])) {
                $menuItem['items'] = $this->getMenuWithAccessRight($menuItem['items'], $roles);
            }
        }

        return $menu;
    }

    /**
     * Получить коды пунктов меню со всех уровней вложенности
     * @param  array  $menuItems
     * @return array
     */
    protected function getMenuItemCodes(array $menuItems): array
    {
        $codes = [];

        foreach ($menuItems as $code => $menuItem) {
            $codes[] = $code;

            if (!empty($menuItem['items'])) {
                $codes = array_merge($codes, $this->getMenuItemCodes($menuItem['items']));
            }
        }

        return $codes;
    }
}
