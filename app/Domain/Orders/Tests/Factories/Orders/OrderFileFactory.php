<?php

namespace App\Domain\Orders\Tests\Factories\Orders;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use App\Http\ApiV1\Support\Tests\Factories\EnsiFileFactory;
use Ensi\OmsClient\Dto\File;
use Ensi\OmsClient\Dto\OrderAttachFileResponse;
use Ensi\OmsClient\Dto\OrderFile;

class OrderFileFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->randomNumber(),
            'order_id' => $this->faker->randomNumber(),
            'file' => new File(EnsiFileFactory::new()->make()),
            'original_name' => $this->faker->word() . '.' . $this->faker->fileExtension(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): OrderFile
    {
        return new OrderFile($this->makeArray($extra));
    }

    public function makeResponseOne(array $extra = []): OrderAttachFileResponse
    {
        return new OrderAttachFileResponse([
            'data' => $this->make($this->makeArray($extra)),
        ]);
    }
}
