<?php

namespace App\Http\ApiV1\Modules\Marketing\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\MarketingClient\Dto\PromoCodeStatusEnum;
use Ensi\MarketingClient\Dto\PromoCodeTypeEnum;
use Illuminate\Validation\Rule;

class PatchPromoCodeRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'owner_id' => ['nullable', 'integer'],
            'name' => ['string'],
            'code' => ['string'],
            'counter' => ['nullable', 'integer', 'gt:0'],
            'start_date' => ['nullable', 'date'],
            'end_date' => ['nullable', 'date', 'after_or_equal:start_date'],
            'status' => ['integer', Rule::in(PromoCodeStatusEnum::getAllowableEnumValues())],
            'type' => ['integer', Rule::in(PromoCodeTypeEnum::getAllowableEnumValues())],
            'discount_id' => ['nullable', 'integer'],
        ];
    }
}
