<?php

namespace App\Domain\Catalog\Actions\Classifiers\ProductTypes;

use Ensi\PimClient\Api\ProductTypesApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\EmptyDataResponse;

class DeleteProductTypeAction
{
    public function __construct(
        private ProductTypesApi $productTypesApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $countryId): EmptyDataResponse
    {
        return $this->productTypesApi->deleteProductType($countryId);
    }
}
