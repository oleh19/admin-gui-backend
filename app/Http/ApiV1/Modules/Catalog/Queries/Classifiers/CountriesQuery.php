<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\Classifiers;

use App\Http\ApiV1\Modules\Catalog\Queries\PimQuery;
use Ensi\PimClient\Api\CountriesApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\CountryResponse;
use Ensi\PimClient\Dto\SearchCountriesRequest;
use Ensi\PimClient\Dto\SearchCountriesResponse;
use Illuminate\Http\Request;

class CountriesQuery extends PimQuery
{
    public function __construct(Request $request, protected CountriesApi $countriesApi)
    {
        parent::__construct($request, SearchCountriesRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function searchById($countryId): CountryResponse
    {
        return $this->countriesApi->getCountry($countryId);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchCountriesResponse
    {
        return $this->countriesApi->searchCountries($request);
    }
}
