<?php

namespace App\Domain\Common\Data\Meta\Fields;

use App\Http\ApiV1\OpenApiGenerated\Dto\FieldTypeEnum;

class DateField extends AbstractField
{
    protected function type(): string
    {
        return FieldTypeEnum::DATE;
    }

    protected function init()
    {
        $this->sort()->filterRange();
    }
}
