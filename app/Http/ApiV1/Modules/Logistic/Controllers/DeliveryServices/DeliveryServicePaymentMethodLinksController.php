<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryServices;

use App\Domain\Logistic\Actions\DeliveryServices\AddPaymentMethodsToDeliveryServiceAction;
use App\Domain\Logistic\Actions\DeliveryServices\DeletePaymentMethodFromDeliveryServiceAction;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryServices\AddPaymentMethodsToDeliveryServiceRequest;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryServices\DeletePaymentMethodFromDeliveryServiceRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Ensi\LogisticClient\ApiException;

class DeliveryServicePaymentMethodLinksController
{
    /**
     * @param int $deliveryServiceId
     * @param AddPaymentMethodsToDeliveryServiceRequest $request
     * @param AddPaymentMethodsToDeliveryServiceAction $action
     * @return EmptyResource
     * @throws ApiException
     */
    public function add(
        int $deliveryServiceId,
        AddPaymentMethodsToDeliveryServiceRequest $request,
        AddPaymentMethodsToDeliveryServiceAction $action
    ) {
        $validated = $request->validated();
        $action->execute($deliveryServiceId, $validated['payment_methods']);

        return new EmptyResource();
    }

    /**
     * @param int $deliveryServiceId
     * @param DeletePaymentMethodFromDeliveryServiceRequest $request
     * @param DeletePaymentMethodFromDeliveryServiceAction $action
     * @return EmptyResource
     * @throws ApiException
     */
    public function delete(
        int $deliveryServiceId,
        DeletePaymentMethodFromDeliveryServiceRequest $request,
        DeletePaymentMethodFromDeliveryServiceAction $action
    ): EmptyResource {
        $validated = $request->validated();
        $action->execute($deliveryServiceId, $validated['payment_method']);

        return new EmptyResource();
    }
}
