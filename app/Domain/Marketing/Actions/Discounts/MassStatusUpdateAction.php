<?php

namespace App\Domain\Marketing\Actions\Discounts;

use Ensi\MarketingClient\Api\DiscountsApi;
use Ensi\MarketingClient\Dto\MassDiscountsStatusUpdateRequest;

class MassStatusUpdateAction
{
    public function __construct(protected DiscountsApi $discountsApi)
    {
    }

    public function execute(array $data)
    {
        $requestMassDiscountStatusApi = new MassDiscountsStatusUpdateRequest($data);
        $this->discountsApi->massDiscountsStatusUpdate($requestMassDiscountStatusApi);
    }
}
