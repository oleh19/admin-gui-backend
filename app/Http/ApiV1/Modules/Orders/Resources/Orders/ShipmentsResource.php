<?php


namespace App\Http\ApiV1\Modules\Orders\Resources\Orders;

use App\Domain\Orders\Data\Orders\ShipmentData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class ShipmentsResource
 * @package App\Http\ApiV1\Modules\Orders\Resources\OrdersDetail
 * @mixin ShipmentData
 */
class ShipmentsResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            "id" => $this->shipment->getId(),
            "status_at" => $this->dateTimeToIso($this->shipment->getStatusAt()),
            "cost" => $this->shipment->getCost(),
            "width" => $this->shipment->getWidth(),
            "height" => $this->shipment->getHeight(),
            "length" => $this->shipment->getLength(),
            "weight" => $this->shipment->getWeight(),
            "created_at" => $this->dateTimeToIso($this->shipment->getCreatedAt()),
            "updated_at" => $this->dateTimeToIso($this->shipment->getUpdatedAt()),
            "delivery_id" => $this->shipment->getDeliveryId(),
            "seller_id" => $this->shipment->getSellerId(),
            "store_id" => $this->shipment->getStoreId(),
            "number" => $this->shipment->getNumber(),
            "status" => $this->shipment->getStatus(),
            "delivery" => new DeliveriesResource($this->whenNotNull($this->getDelivery())),
            "order_items" => OrderItemsResource::collection($this->whenNotNull($this->getOrderItems())),
        ];
    }
}
