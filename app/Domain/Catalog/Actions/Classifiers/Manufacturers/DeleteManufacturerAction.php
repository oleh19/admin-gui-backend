<?php

namespace App\Domain\Catalog\Actions\Classifiers\Manufacturers;

use Ensi\PimClient\Api\ManufacturersApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\EmptyDataResponse;

class DeleteManufacturerAction
{
    public function __construct(
        private ManufacturersApi $manufacturersApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $countryId): EmptyDataResponse
    {
        return $this->manufacturersApi->deleteManufacturer($countryId);
    }
}
