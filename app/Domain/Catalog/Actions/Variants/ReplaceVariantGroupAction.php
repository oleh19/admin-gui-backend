<?php

namespace App\Domain\Catalog\Actions\Variants;

use Ensi\PimClient\Api\VariantsApi;
use Ensi\PimClient\Dto\ReplaceVariantGroupRequest;
use Ensi\PimClient\Dto\VariantGroup;

class ReplaceVariantGroupAction
{
    public function __construct(private VariantsApi $api)
    {
    }

    public function execute(int $groupId, array $fields): VariantGroup
    {
        $request = new ReplaceVariantGroupRequest($fields);

        return $this->api->replaceVariantGroup($groupId, $request)->getData();
    }
}
