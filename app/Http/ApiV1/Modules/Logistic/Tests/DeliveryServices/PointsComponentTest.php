<?php

use App\Domain\Logistic\Tests\DeliveryServices\Factories\PointFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/logistic/point-enum-values:search 200', function ($key, $value) {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockLogisticGeosApi()->allows([
        'searchPoints' => PointFactory::new()->makeResponseSearch(),
    ]);

    postJson("/api/v1/logistic/point-enum-values:search", ['filter' => [$key => $value]])->assertStatus(200);
})->with([
    ['id', [1, 2]],
    ['query', 'name'],
]);
