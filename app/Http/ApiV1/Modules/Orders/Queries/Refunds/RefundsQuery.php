<?php


namespace App\Http\ApiV1\Modules\Orders\Queries\Refunds;

use App\Domain\Orders\Data\Refunds\RefundData;
use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\OmsClient\Api\RefundsApi;
use Ensi\OmsClient\Dto\Refund;
use Ensi\OmsClient\Dto\RequestBodyPagination as RequestBodyPaginationOms;
use Ensi\OmsClient\Dto\SearchRefundsRequest;
use Illuminate\Http\Request;

class RefundsQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;
    use QueryBuilderFindTrait;

    public function __construct(
        protected Request $httpRequest,
        protected RefundsApi $refundsApi,
    ) {
        parent::__construct($httpRequest);
    }

    protected function requestGetClass(): string
    {
        return SearchRefundsRequest::class;
    }

    protected function paginationClass(): string
    {
        return RequestBodyPaginationOms::class;
    }

    protected function convertGetToItems($response)
    {
        return $this->convertArray($response->getData());
    }

    protected function convertFindToItem($response)
    {
        return current($this->convertArray([$response->getData()]));
    }

    /**
     * @param Refund[] $refunds
     * @return array
     */
    protected function convertArray(array $refunds)
    {
        $response = [];
        foreach ($refunds as $refund) {
            $response[] = new RefundData($refund);
        }

        return $response;
    }

    protected function search($request)
    {
        return $this->refundsApi->searchRefunds($request);
    }

    protected function searchById($id)
    {
        return $this->refundsApi->getRefund($id, $this->getInclude());
    }
}
