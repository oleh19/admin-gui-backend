<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\Classifiers;

use App\Http\ApiV1\Modules\Catalog\Queries\PimQuery;
use Ensi\PimClient\Api\ManufacturersApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\ManufacturerResponse;
use Ensi\PimClient\Dto\SearchManufacturersRequest;
use Ensi\PimClient\Dto\SearchManufacturersResponse;
use Illuminate\Http\Request;

class ManufacturersQuery extends PimQuery
{
    public function __construct(Request $request, protected ManufacturersApi $manufacturersApi)
    {
        parent::__construct($request, SearchManufacturersRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function searchById($manufacturerId): ManufacturerResponse
    {
        return $this->manufacturersApi->getManufacturer($manufacturerId);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchManufacturersResponse
    {
        return $this->manufacturersApi->searchManufacturers($request);
    }
}
