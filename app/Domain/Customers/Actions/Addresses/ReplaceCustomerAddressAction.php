<?php

namespace App\Domain\Customers\Actions\Addresses;

use Ensi\CustomersClient\Dto\CustomerAddress;
use Ensi\CustomersClient\Dto\CustomerAddressForReplace;

/**
 * Class ReplaceCustomerAddressAction
 * @package App\Domain\Customers\Actions\Addresses
 */
class ReplaceCustomerAddressAction extends CustomerAddressAction
{
    /**
     * @param int $addressId
     * @param array $fields
     * @return CustomerAddress
     * @throws \Ensi\CustomersClient\ApiException
     */
    public function execute(int $addressId, array $fields): CustomerAddress
    {
        return $this->addressesApi->replaceCustomerAddress(
            $addressId,
            new CustomerAddressForReplace($fields)
        )->getData();
    }
}
