<?php

namespace App\Domain\Customers\Actions\CustomersInfo;

use Ensi\CrmClient\Api\CustomersInfoApi;

abstract class CustomersInfoAction
{
    public function __construct(protected CustomersInfoApi $customersInfoApi)
    {
    }
}
