<?php

namespace App\Domain\Catalog\Tests\Factories\Offers;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\OffersClient\Dto\Offer;
use Ensi\OffersClient\Dto\OfferSaleStatusEnum;
use Ensi\OffersClient\Dto\SearchOffersResponse;

class OfferFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->randomNumber(),
            'product_id' => $this->faker->randomNumber(),
            'seller_id' => $this->faker->randomNumber(),
            'external_id' => $this->faker->numerify('##-##'),
            'sale_status' => $this->faker->randomElement(OfferSaleStatusEnum::getAllowableEnumValues()),
            'storage_address' => $this->faker->optional()->numerify('###'),
            'base_price' => $this->faker->numberBetween(0, 1000),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): Offer
    {
        return new Offer($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchOffersResponse
    {
        return $this->generateResponseSearch(SearchOffersResponse::class, $extra, $count);
    }
}
