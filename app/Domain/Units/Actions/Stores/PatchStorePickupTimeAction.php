<?php

namespace App\Domain\Units\Actions\Stores;

use Ensi\BuClient\Api\StorePickupTimesApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\StorePickupTime;
use Ensi\BuClient\Dto\StorePickupTimeForPatch;

class PatchStorePickupTimeAction
{
    public function __construct(
        private StorePickupTimesApi $storePickupTimesApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): StorePickupTime
    {
        $storePickupTime = new StorePickupTimeForPatch($fields);

        return $this->storePickupTimesApi->patchStorePickupTime($id, $storePickupTime)->getData();
    }
}
