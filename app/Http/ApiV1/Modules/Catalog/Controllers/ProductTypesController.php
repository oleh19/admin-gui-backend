<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers;

use App\Domain\Catalog\Actions\Classifiers\ProductTypes\CreateProductTypeAction;
use App\Domain\Catalog\Actions\Classifiers\ProductTypes\DeleteProductTypeAction;
use App\Domain\Catalog\Actions\Classifiers\ProductTypes\ReplaceProductTypeAction;
use App\Http\ApiV1\Modules\Catalog\Queries\Classifiers\ProductTypesQuery;
use App\Http\ApiV1\Modules\Catalog\Requests\Classifiers\CreateOrReplaceProductTypeRequest;
use App\Http\ApiV1\Modules\Catalog\Resources\Classifiers\ProductTypesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ProductTypesController
{
    public function search(ProductTypesQuery $query): AnonymousResourceCollection
    {
        return ProductTypesResource::collectPage($query->get());
    }

    public function create(
        CreateOrReplaceProductTypeRequest $request,
        CreateProductTypeAction $action
    ): ProductTypesResource {
        return new ProductTypesResource($action->execute($request->validated()));
    }

    public function get(
        int $productTypeId,
        ProductTypesQuery $query
    ): ProductTypesResource {
        return new ProductTypesResource($query->find($productTypeId));
    }

    public function replace(
        int $productTypeId,
        CreateOrReplaceProductTypeRequest $request,
        ReplaceProductTypeAction $action
    ): ProductTypesResource {
        return new ProductTypesResource($action->execute($productTypeId, $request->validated()));
    }

    public function delete(
        int $productTypeId,
        DeleteProductTypeAction $action
    ): EmptyResource {
        $action->execute($productTypeId);

        return new EmptyResource();
    }
}
