<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Products;

use App\Http\ApiV1\Modules\Catalog\Resources\Attributes\DirectoryValuesResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\ProductAttributeValue;

/**
 * @mixin ProductAttributeValue
 */
class ProductAttributeValuesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'property_id' => $this->getPropertyId(),
            'value' => $this->getValue(),
            'directory' => DirectoryValuesResource::collection($this->whenNotNull($this->getDirectory())),
        ];
    }
}
