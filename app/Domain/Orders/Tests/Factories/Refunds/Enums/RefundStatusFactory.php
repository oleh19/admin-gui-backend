<?php

namespace App\Domain\Orders\Tests\Factories\Refunds\Enums;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\OmsClient\Dto\RefundStatus;
use Ensi\OmsClient\Dto\RefundStatusEnum;
use Ensi\OmsClient\Dto\RefundStatusesResponse;

class RefundStatusFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->randomElement(RefundStatusEnum::getAllowableEnumValues()),
            'name' => $this->faker->text(20),
        ];
    }

    public function make(array $extra = []): RefundStatus
    {
        return new RefundStatus($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extra = []): RefundStatusesResponse
    {
        return new RefundStatusesResponse([
            'data' => [$this->make($this->makeArray($extra))],
        ]);
    }
}
