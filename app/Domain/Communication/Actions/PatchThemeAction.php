<?php

namespace App\Domain\Communication\Actions;

use Ensi\CommunicationManagerClient\Api\ThemesApi;
use Ensi\CommunicationManagerClient\ApiException;
use Ensi\CommunicationManagerClient\Dto\Theme;
use Ensi\CommunicationManagerClient\Dto\ThemeForPatch;

class PatchThemeAction
{
    public function __construct(
        private ThemesApi $themesApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $themeId, array $fields): Theme
    {
        $theme = new ThemeForPatch($fields);

        return $this->themesApi->patchTheme($themeId, $theme)->getData();
    }
}
