<?php


namespace App\Domain\Orders\ProtectedFiles;

use App\Domain\Common\ProtectedFiles\ProtectedFile;
use Ensi\OmsClient\Dto\OrderFile as OmsOrderFile;

class OrderFile extends ProtectedFile
{
    public static function entity(): string
    {
        return 'oms/order-file';
    }

    public static function createFromModel(OmsOrderFile $orderFile): static
    {
        $object = new static();
        $object->entity_id = $orderFile->getOrderId();
        $object->file = $orderFile->getId();

        return $object;
    }
}
