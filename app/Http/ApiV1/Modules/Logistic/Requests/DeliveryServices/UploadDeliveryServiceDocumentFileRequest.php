<?php

namespace App\Http\ApiV1\Modules\Logistic\Requests\DeliveryServices;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class UploadDeliveryServiceDocumentFileRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'file' => ['required', 'file', 'max:102400'],
        ];
    }
}
