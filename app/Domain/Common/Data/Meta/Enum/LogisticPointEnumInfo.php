<?php

namespace App\Domain\Common\Data\Meta\Enum;

class LogisticPointEnumInfo extends AbstractEnumInfo
{
    public function __construct()
    {
        $this->endpointName = 'logistic.searchPointEnumValues';
    }
}
