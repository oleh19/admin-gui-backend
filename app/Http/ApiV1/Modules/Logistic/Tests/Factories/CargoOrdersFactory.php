<?php

namespace App\Http\ApiV1\Modules\Logistic\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\LogisticClient\Dto\CargoOrders;
use Ensi\LogisticClient\Dto\CargoOrdersResponse;
use Ensi\LogisticClient\Dto\CargoOrderStatusEnum;
use Ensi\LogisticClient\Dto\ResponseBodyPagination;
use Ensi\LogisticClient\Dto\SearchCargoOrdersResponse;

class CargoOrdersFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->randomNumber(),
            'cargo_id' => $this->faker->randomNumber(),

            'timeslot_id' => $this->faker->uuid(),
            'timeslot_from' => $this->faker->dateTime(),
            'timeslot_to' => $this->faker->dateTime(),

            'cdek_intake_number' => $this->faker->unique()->numerify('######'),
            'external_id' => $this->faker->unique()->numerify('######'),
            'error_external_id' => $this->faker->text(20),
            'date' => $this->faker->dateTime(),
            'status' => $this->faker->randomElement(CargoOrderStatusEnum::getAllowableEnumValues()),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function makeResource(array $extra = []): CargoOrders
    {
        return new CargoOrders($this->make($extra));
    }

    public function makeResponseOne(array $extra = []): CargoOrdersResponse
    {
        return new CargoOrdersResponse([
            'data' => $this->makeResource($extra),
        ]);
    }

    public function makeResponse(array $extra = []): SearchCargoOrdersResponse
    {
        $meta = SearchCargoOrdersResponse::openAPITypes()['meta'];

        return new SearchCargoOrdersResponse([
            'data' => [$this->makeResource($extra)],
            'meta' => new $meta([
                'pagination' => new ResponseBodyPagination([
                    'limit' => 10,
                    'offset' => 0,
                    'total' => 1,
                    'type' => 'offset',
                ]),
            ]),
        ]);
    }
}
