<?php

namespace App\Domain\Logistic\Tests\DeliveryServices\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\LogisticClient\Dto\Point;
use Ensi\LogisticClient\Dto\SearchPointsResponse;

class PointFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        // todo доделать остальные поля
        return [
            'id' => $this->faker->randomNumber(),
            'name' => $this->faker->text(20),
        ];
    }

    public function make(array $extra = []): Point
    {
        return new Point($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchPointsResponse
    {
        return $this->generateResponseSearch(SearchPointsResponse::class, $extra, $count);
    }
}
