<?php

namespace App\Domain\Orders\Tests\Factories\Refunds;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use App\Http\ApiV1\Support\Tests\Factories\EnsiFileFactory;
use Ensi\OmsClient\Dto\File;
use Ensi\OmsClient\Dto\RefundFile;
use Ensi\OmsClient\Dto\RefundFileResponse;

class RefundFileFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->randomNumber(),
            'refund_id' => $this->faker->randomNumber(),
            'file' => new File(EnsiFileFactory::new()->make()),
            'original_name' => $this->faker->word() . '.' . $this->faker->fileExtension(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): RefundFile
    {
        return new RefundFile($this->makeArray($extra));
    }

    public function makeResponseOne(array $extra = []): RefundFileResponse
    {
        return new RefundFileResponse([
            'data' => $this->make($this->makeArray($extra)),
        ]);
    }
}
