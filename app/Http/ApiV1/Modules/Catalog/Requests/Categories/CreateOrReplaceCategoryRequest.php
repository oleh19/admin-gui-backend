<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Categories;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateOrReplaceCategoryRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'code' => ['nullable', 'string'],
            'parent_id' => ['nullable', 'integer'],
        ];
    }
}
