<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryServices;

use App\Http\ApiV1\Modules\Logistic\Queries\DeliveryServices\GetDeliveryMethodsQuery;
use App\Http\ApiV1\Modules\Logistic\Queries\DeliveryServices\GetDeliveryServiceStatusesQuery;
use App\Http\ApiV1\Modules\Logistic\Queries\DeliveryServices\GetShipmentMethodsQuery;
use App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices\DeliveryMethodsResource;
use App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices\DeliveryServiceStatusesResource;
use App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices\ShipmentMethodsResource;
use Ensi\LogisticClient\ApiException;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class DeliveryServiceDirectoryController
 * @package App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryServices
 */
class DeliveryServiceDirectoryController
{
    /**
     * @param GetDeliveryMethodsQuery $query
     * @return AnonymousResourceCollection
     * @throws ApiException
     */
    public function getDeliveryMethods(GetDeliveryMethodsQuery $query): AnonymousResourceCollection
    {
        return DeliveryMethodsResource::collection($query->execute());
    }

    /**
     * @param GetDeliveryServiceStatusesQuery $query
     * @return AnonymousResourceCollection
     * @throws ApiException
     */
    public function getDeliveryServiceStatuses(GetDeliveryServiceStatusesQuery $query): AnonymousResourceCollection
    {
        return DeliveryServiceStatusesResource::collection($query->execute());
    }

    /**
     * @param GetShipmentMethodsQuery $query
     * @return AnonymousResourceCollection
     * @throws ApiException
     */
    public function getShipmentMethods(GetShipmentMethodsQuery $query): AnonymousResourceCollection
    {
        return ShipmentMethodsResource::collection($query->execute());
    }
}
