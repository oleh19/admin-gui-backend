<?php

namespace App\Domain\Units\Actions\Stores;

use Ensi\BuClient\Api\StoreContactsApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\StoreContact;
use Ensi\BuClient\Dto\StoreContactForPatch;

class PatchStoreContactAction
{
    public function __construct(
        private StoreContactsApi $storeContactsApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): StoreContact
    {
        $storeContact = new StoreContactForPatch($fields);

        return $this->storeContactsApi->patchStoreContact($id, $storeContact)->getData();
    }
}
