<?php


namespace App\Domain\Orders\Data\Orders;

use App\Domain\Customers\Data\CustomerData;
use Ensi\AdminAuthClient\Dto\User as AdminUser;
use Ensi\OffersClient\Dto\Offer;
use Ensi\OmsClient\Dto\Order;
use Ensi\PimClient\Dto\Product;

class OrderData
{
    public ?CustomerData $customer = null;
    public ?AdminUser $responsible = null;

    /** @var Offer[] */
    public array $offers = [];
    /** @var Product[] */
    public array $products = [];

    public function __construct(public Order $order)
    {
    }

    /**
     * @return DeliveryData[]|null
     */
    public function getDeliveries(): ?array
    {
        if (is_null($this->order->getDeliveries())) {
            return null;
        }
        $deliveriesData = [];
        foreach ($this->order->getDeliveries() as $delivery) {
            $deliveryData = new DeliveryData($delivery);
            $deliveryData->offers = $this->offers;
            $deliveryData->products = $this->products;
            $deliveriesData[] = $deliveryData;
        }

        return $deliveriesData;
    }
}
