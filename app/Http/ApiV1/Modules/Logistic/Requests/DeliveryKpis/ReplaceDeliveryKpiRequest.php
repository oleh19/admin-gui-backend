<?php

namespace App\Http\ApiV1\Modules\Logistic\Requests\DeliveryKpis;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

/**
 * Class ReplaceDeliveryKpiRequest
 * @package App\Http\ApiV1\Modules\Logistic\Requests\DeliveryKpis
 */
class ReplaceDeliveryKpiRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'rtg' => ['required', 'integer', 'min:0'],
            'ct' => ['required', 'integer', 'min:0'],
            'ppt' => ['required', 'integer', 'min:0'],
      ];
    }
}
