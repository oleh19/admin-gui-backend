<?php


namespace App\Http\ApiV1\Modules\Marketing\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\MarketingClient\Dto\DiscountConditionType;

/**
 * Class DiscountConditionTypesResource
 * @package App\Http\ApiV1\Modules\Marketing\Resources
 * @mixin DiscountConditionType
 */
class DiscountConditionTypesResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'props' => $this->getProps(),
        ];
    }
}
