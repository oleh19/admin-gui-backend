<?php

namespace App\Domain\Logistic\Data;

use App\Domain\Orders\Data\Orders\ShipmentData;
use Ensi\LogisticClient\Dto\Cargo;

class CargoData
{
    /** @var ShipmentData[] */
    public array $shipments = [];

    public function __construct(public Cargo $cargo)
    {
    }

    /**
     * @return ShipmentData[]|null
     */
    public function getShipments(): ?array
    {
        return $this->shipments;
    }
}
