<?php

namespace App\Domain\Communication\Actions;

use Ensi\CommunicationManagerClient\Api\TypesApi;
use Ensi\CommunicationManagerClient\ApiException;
use Ensi\CommunicationManagerClient\Dto\Type;
use Ensi\CommunicationManagerClient\Dto\TypeForCreate;

class CreateTypeAction
{
    public function __construct(
        private TypesApi $typesApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): Type
    {
        $type = new TypeForCreate($fields);

        return $this->typesApi->createType($type)->getData();
    }
}
