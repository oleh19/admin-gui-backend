<?php


namespace App\Domain\Common\ProtectedFiles;

use App\Domain\Communication\ProtectedFiles\MessageAttachment;
use App\Domain\Communication\ProtectedFiles\MessageAttachmentLoader;
use App\Domain\Logistic\ProtectedFiles\DeliveryServiceDocumentFile;
use App\Domain\Logistic\ProtectedFiles\DeliveryServiceDocumentFileLoader;
use App\Domain\Orders\ProtectedFiles\OrderFile;
use App\Domain\Orders\ProtectedFiles\OrderFileLoader;
use App\Domain\Orders\ProtectedFiles\RefundFile;
use App\Domain\Orders\ProtectedFiles\RefundFileLoader;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ProtectedFileLoaderFactory
{
    public function build(ProtectedFile $file): ProtectedFileLoader
    {
        /** @var ProtectedFileLoader $loader */
        $loader = match ($file::entity()) {
            MessageAttachment::entity() => resolve(MessageAttachmentLoader::class),
            DeliveryServiceDocumentFile::entity() => resolve(DeliveryServiceDocumentFileLoader::class),
            OrderFile::entity() => resolve(OrderFileLoader::class),
            RefundFile::entity() => resolve(RefundFileLoader::class),
            default => throw new ModelNotFoundException("Не удалось определить сущность"),
        };

        $loader->setFile($file);

        return $loader;
    }
}
