<?php

namespace App\Domain\Common\Data\Meta\Enum;

use GuzzleHttp\Promise\PromiseInterface;

interface AsyncEnumInfo
{
    public function requestAsync(): PromiseInterface;

    public function processResponse($response);
}
