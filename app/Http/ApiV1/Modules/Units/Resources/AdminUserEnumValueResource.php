<?php

namespace App\Http\ApiV1\Modules\Units\Resources;

use App\Http\ApiV1\Support\Resources\EnumValueResource;
use Ensi\UserAuthClient\Dto\User;

/**
 * Class CustomerEnumValueResource
 * @package App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices
 *
 * @mixin User
 */
class AdminUserEnumValueResource extends EnumValueResource
{
    protected function getEnumId(): string
    {
        return $this->getId();
    }

    protected function getEnumTitle(): string
    {
        return $this->getFullName();
    }
}
