<?php

namespace App\Http\ApiV1\Modules\Customers\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CustomerAuthClient\Dto\User;
use Illuminate\Http\Request;

/**
 * Class CustomerUserResource
 * @package App\Http\ApiV1\Modules\Customers\Resources
 * @mixin User
 */
class CustomerUserResource extends BaseJsonResource
{
    /**
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'active' => $this->getActive(),
            'login' => $this->getLogin(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),
        ];
    }
}
