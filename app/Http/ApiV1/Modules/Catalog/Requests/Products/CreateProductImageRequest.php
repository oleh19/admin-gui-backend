<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Products;

use App\Http\ApiV1\OpenApiGenerated\Dto\CatalogProductImageTypeEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateProductImageRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'product_id' => ['required', 'integer'],
            'type' => ['required', Rule::in(CatalogProductImageTypeEnum::getAllowableEnumValues())],
        ];
    }
}
