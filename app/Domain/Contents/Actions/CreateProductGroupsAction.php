<?php

namespace App\Domain\Contents\Actions;

use Ensi\CmsClient\Api\ProductGroupsApi;
use Ensi\CmsClient\Dto\ProductGroup;
use Ensi\CmsClient\Dto\ProductGroupForCreate;

class CreateProductGroupsAction
{
    public function __construct(
        protected ProductGroupsApi $productGroupApi
    ) {
    }

    public function execute(array $data): ProductGroup
    {
        $requestProductGroupsApi = new ProductGroupForCreate($data);
        $responseProductGroupsApi =  $this->productGroupApi->createProductGroups($requestProductGroupsApi);

        return $responseProductGroupsApi->getData();
    }
}
