<?php


namespace App\Http\ApiV1\Modules\Orders\Resources\Refunds\Enums;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\OmsClient\Dto\RefundStatus;

/**
 * Class RefundStatusesResource
 * @package App\Http\ApiV1\Modules\Orders\Resources
 * @mixin RefundStatus
 */
class RefundStatusesResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
        ];
    }
}
