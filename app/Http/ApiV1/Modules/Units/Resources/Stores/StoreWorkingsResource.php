<?php

namespace App\Http\ApiV1\Modules\Units\Resources\Stores;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\BuClient\Dto\StoreWorking;

/** @mixin StoreWorking */
class StoreWorkingsResource extends BaseJsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),
            'store_id' => $this->getStoreId(),
            'active' => $this->getActive(),
            'day' => $this->getDay(),
            'working_start_time' => $this->getWorkingStartTime(),
            'working_end_time' => $this->getWorkingEndTime(),
            'created_at' => $this->getCreatedAt(),
            'updated_at' => $this->getUpdatedAt(),
        ];
    }
}
