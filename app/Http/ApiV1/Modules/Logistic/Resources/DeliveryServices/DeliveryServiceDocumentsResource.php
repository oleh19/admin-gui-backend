<?php

namespace App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices;

use App\Domain\Logistic\Data\DeliveryServiceDocumentData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class DeliveryServiceDocumentsResource
 * @package App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices
 *
 * @mixin DeliveryServiceDocumentData
 */
class DeliveryServiceDocumentsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->deliveryServiceDocument->getId(),
            'delivery_service_id' => $this->deliveryServiceDocument->getDeliveryServiceId(),
            'name' => $this->deliveryServiceDocument->getName(),
            'file' => $this->getFile(),
            'created_at' => $this->dateTimeToIso($this->deliveryServiceDocument->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->deliveryServiceDocument->getUpdatedAt()),
            'delivery_service' => DeliveryServicesResource::make($this->whenNotNull($this->deliveryServiceDocument->getDeliveryService())),
        ];
    }
}
