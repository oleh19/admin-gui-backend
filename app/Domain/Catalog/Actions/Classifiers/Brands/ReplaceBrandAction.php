<?php

namespace App\Domain\Catalog\Actions\Classifiers\Brands;

use Ensi\PimClient\Api\BrandsApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\Brand;
use Ensi\PimClient\Dto\ReplaceBrandRequest;

class ReplaceBrandAction
{
    public function __construct(
        private BrandsApi $brandsApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $brandId, array $fields): Brand
    {
        $request = new ReplaceBrandRequest($fields);

        return $this->brandsApi->replaceBrand($brandId, $request)->getData();
    }
}
