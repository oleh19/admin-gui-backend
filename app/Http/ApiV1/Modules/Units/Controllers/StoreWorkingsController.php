<?php

namespace App\Http\ApiV1\Modules\Units\Controllers;

use App\Domain\Units\Actions\Stores\CreateStoreWorkingAction;
use App\Domain\Units\Actions\Stores\PatchStoreWorkingAction;
use App\Domain\Units\Actions\Stores\ReplaceStoreWorkingAction;
use App\Http\ApiV1\Modules\Units\Requests\Stores\CreateOrReplaceStoreWorkingRequest;
use App\Http\ApiV1\Modules\Units\Requests\Stores\PatchStoreWorkingRequest;
use App\Http\ApiV1\Modules\Units\Resources\Stores\StoreWorkingsResource;

class StoreWorkingsController
{
    public function create(CreateOrReplaceStoreWorkingRequest $request, CreateStoreWorkingAction $action): StoreWorkingsResource
    {
        return new StoreWorkingsResource($action->execute($request->validated()));
    }

    public function patch(
        int $storeWorkingId,
        PatchStoreWorkingRequest $request,
        PatchStoreWorkingAction $action
    ): StoreWorkingsResource {
        return new StoreWorkingsResource($action->execute($storeWorkingId, $request->validated()));
    }

    public function replace(
        int $storeWorkingId,
        CreateOrReplaceStoreWorkingRequest $request,
        ReplaceStoreWorkingAction $action
    ): StoreWorkingsResource {
        return new StoreWorkingsResource($action->execute($storeWorkingId, $request->validated()));
    }
}
