<?php

namespace App\Domain\Marketing\Actions\Discounts;

use Ensi\MarketingClient\Api\DiscountConditionsApi;
use Ensi\MarketingClient\Dto\DiscountCondition;
use Ensi\MarketingClient\Dto\DiscountConditionUpdatableProperties;

class PatchDiscountConditionAction
{
    public function __construct(protected DiscountConditionsApi $discountsApi)
    {
    }

    public function execute(int $discountConditionId, array $data): DiscountCondition
    {
        $requestDiscountConditionsApi  = new DiscountConditionUpdatableProperties($data);
        $responseDiscountConditionsApi = $this->discountsApi->patchDiscountCondition(
            $discountConditionId,
            $requestDiscountConditionsApi
        );

        return $responseDiscountConditionsApi->getData();
    }
}
