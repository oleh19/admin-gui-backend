<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Classifiers;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\Manufacturer;

/**
 * @mixin Manufacturer
 */
class ManufacturersResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'code' => $this->getCode(),
            'name' => $this->getName(),

            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
        ];
    }
}
