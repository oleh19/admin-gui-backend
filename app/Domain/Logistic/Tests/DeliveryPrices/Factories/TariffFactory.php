<?php

namespace App\Domain\Logistic\Tests\DeliveryPrices\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\LogisticClient\Dto\SearchTariffsResponse;
use Ensi\LogisticClient\Dto\Tariff;

class TariffFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        // todo доделать остальные поля
        return [
            'id' => $this->faker->randomNumber(),
            'name' => $this->faker->text(20),
        ];
    }

    public function make(array $extra = []): Tariff
    {
        return new Tariff($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchTariffsResponse
    {
        return $this->generateResponseSearch(SearchTariffsResponse::class, $extra, $count);
    }
}
