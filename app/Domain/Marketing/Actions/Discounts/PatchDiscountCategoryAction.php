<?php

namespace App\Domain\Marketing\Actions\Discounts;

use Ensi\MarketingClient\Api\DiscountCategoriesApi;
use Ensi\MarketingClient\Dto\DiscountCategory;
use Ensi\MarketingClient\Dto\DiscountCategoryUpdatableProperties;

class PatchDiscountCategoryAction
{
    public function __construct(protected DiscountCategoriesApi $discountsApi)
    {
    }

    public function execute(int $discountCategoryId, array $data): DiscountCategory
    {
        $requestDiscountCategoriesApi  = new DiscountCategoryUpdatableProperties($data);
        $responseDiscountCategoriesApi = $this->discountsApi->patchDiscountCategory(
            $discountCategoryId,
            $requestDiscountCategoriesApi
        );

        return $responseDiscountCategoriesApi->getData();
    }
}
