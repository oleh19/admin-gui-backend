<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Attributes;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\PimClient\Dto\PropertyTypeEnum;
use Illuminate\Validation\Rule;

class CreateOrReplaceProductAttributeRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'type' => ['required', Rule::in(PropertyTypeEnum::getAllowableEnumValues())],
            'display_name' => ['required', 'string'],
            'code' => ['nullable', 'string'],
            'is_multiple' => ['boolean'],
            'is_filterable' => ['boolean'],
            'is_color' => ['boolean'],
        ];
    }
}
