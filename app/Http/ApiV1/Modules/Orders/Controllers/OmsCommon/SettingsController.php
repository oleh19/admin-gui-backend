<?php


namespace App\Http\ApiV1\Modules\Orders\Controllers\OmsCommon;

use App\Domain\Orders\Actions\OmsCommon\PatchSeveralSettingsAction;
use App\Http\ApiV1\Modules\Orders\Requests\OmsCommon\PatchSeveralSettingsRequest;
use App\Http\ApiV1\Modules\Orders\Resources\OmsCommon\SettingResource;
use Ensi\OmsClient\Api\CommonApi;

class SettingsController
{
    public function search(CommonApi $commonApi)
    {
        return SettingResource::collection($commonApi->searchSettings()->getData());
    }

    public function patchSeveral(PatchSeveralSettingsAction $action, PatchSeveralSettingsRequest $request)
    {
        return SettingResource::collection($action->execute($request->validated()));
    }
}
