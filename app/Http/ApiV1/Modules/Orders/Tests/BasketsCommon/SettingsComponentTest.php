<?php

use App\Domain\Orders\Tests\Factories\BasketsCommon\SettingFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'orders', 'orders.settings');

test('GET /api/v1/orders/baskets-settings 200', function () {
    $settingId = 1;
    $settingValue = 'test';

    $this->mockBasketsCommonApi()->allows([
        'searchSettings' => SettingFactory::new()->makeResponseSearch(['id' => $settingId, 'code' => $settingValue]),
    ]);

    getJson("/api/v1/orders/baskets-settings")
        ->assertStatus(200);
});

test('PATCH /api/v1/orders/baskets-settings 200', function () {
    $settingId = 1;
    $settingValue = 'test';

    $this->mockBasketsCommonApi()->allows([
        'patchSettings' => SettingFactory::new()->makeResponseSearch(['id' => $settingId, 'value' => $settingValue]),
    ]);

    patchJson('/api/v1/orders/baskets-settings', ['settings' => [['id' => $settingId, 'value' => $settingValue]]])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $settingId)
        ->assertJsonPath('data.0.value', $settingValue);
});
