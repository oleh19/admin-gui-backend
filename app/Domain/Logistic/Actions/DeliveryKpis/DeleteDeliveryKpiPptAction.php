<?php


namespace App\Domain\Logistic\Actions\DeliveryKpis;

use Ensi\LogisticClient\Api\KpiApi;
use Ensi\LogisticClient\ApiException;

/**
 * Class DeleteDeliveryKpiPptAction
 * @package App\Domain\Logistic\Actions\DeliveryKpis
 */
class DeleteDeliveryKpiPptAction
{
    /**
     * DeleteDeliveryKpiPptAction constructor.
     * @param KpiApi $kpiApi
     */
    public function __construct(protected KpiApi $kpiApi)
    {
    }

    /**
     * @param int $sellerId
     * @throws ApiException
     */
    public function execute(int $sellerId): void
    {
        $this->kpiApi->deleteDeliveryKpiPpt($sellerId);
    }
}
