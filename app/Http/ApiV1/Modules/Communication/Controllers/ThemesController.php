<?php

namespace App\Http\ApiV1\Modules\Communication\Controllers;

use App\Domain\Communication\Actions\CreateThemeAction;
use App\Domain\Communication\Actions\DeleteThemeAction;
use App\Domain\Communication\Actions\PatchThemeAction;
use App\Http\ApiV1\Modules\Communication\Queries\ThemesQuery;
use App\Http\ApiV1\Modules\Communication\Requests\CreateThemeRequest;
use App\Http\ApiV1\Modules\Communication\Requests\PatchThemeRequest;
use App\Http\ApiV1\Modules\Communication\Resources\ThemesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class ThemesController
{
    public function search(ThemesQuery $query)
    {
        return ThemesResource::collectPage($query->get());
    }

    public function create(CreateThemeRequest $request, CreateThemeAction $action)
    {
        return new ThemesResource($action->execute($request->validated()));
    }

    public function patch(int $themeId, PatchThemeRequest $request, PatchThemeAction $action)
    {
        return new ThemesResource($action->execute($themeId, $request->validated()));
    }

    public function delete(int $themeId, DeleteThemeAction $action)
    {
        $action->execute($themeId);

        return new EmptyResource();
    }
}
