<?php

namespace App\Domain\Auth\Actions;

use Ensi\AdminAuthClient\Api\OauthApi;
use Ensi\AdminAuthClient\Dto\CreateTokenRequest;
use Ensi\AdminAuthClient\Dto\CreateTokenResponse;
use Ensi\AdminAuthClient\Dto\ErrorResponse;
use Ensi\AdminAuthClient\Dto\GrantTypeEnum;

class RefreshAction
{
    public function __construct(private OauthApi $oauthApi)
    {
    }

    public function execute(array $fields): CreateTokenResponse|ErrorResponse
    {
        $request = new CreateTokenRequest();
        $request->setGrantType(GrantTypeEnum::REFRESH_TOKEN);
        $request->setClientId(config('openapi-clients.units.admin-auth.client.id'));
        $request->setClientSecret(config('openapi-clients.units.admin-auth.client.secret'));
        $request->setRefreshToken($fields['refresh_token']);

        return $this->oauthApi->createToken($request);
    }
}
