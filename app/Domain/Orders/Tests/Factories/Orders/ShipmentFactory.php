<?php

namespace App\Domain\Orders\Tests\Factories\Orders;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\OmsClient\Dto\OrderItem;
use Ensi\OmsClient\Dto\Shipment;
use Ensi\OmsClient\Dto\ShipmentStatusEnum;

class ShipmentFactory extends BaseApiFactory
{
    protected array $orderItems = [];

    protected function definition(): array
    {
        $definition = [
            'id' => $this->faker->randomNumber(),
            'delivery_id' => $this->faker->randomNumber(),
            'seller_id' => $this->faker->randomNumber(),
            'store_id' => $this->faker->randomNumber(),
            'status' => $this->faker->randomElement(ShipmentStatusEnum::getAllowableEnumValues()),
            'status_at' => $this->faker->dateTime(),
            'number' => $this->faker->unique()->numerify('######-#-#'),
            'cost' => $this->faker->numberBetween(0, 1000),
            'width' => $this->faker->randomFloat(),
            'height' => $this->faker->randomFloat(),
            'length' => $this->faker->randomFloat(),
            'weight' => $this->faker->randomFloat(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];

        if ($this->orderItems) {
            $definition['order_items'] = $this->orderItems;
        }

        return $definition;
    }

    public function withOrderItem(?OrderItem $orderItem = null): self
    {
        $this->orderItems[] = $orderItem ?: OrderItemFactory::new()->make();

        return $this;
    }

    public function make(array $extra = []): Shipment
    {
        return new Shipment($this->makeArray($extra));
    }
}
