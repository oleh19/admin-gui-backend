<?php

namespace App\Domain\Catalog\Tests\Factories\Products;

use App\Domain\Catalog\Tests\Factories\Categories\CategoryFactory;
use App\Domain\Catalog\Tests\Factories\Classifiers\BrandFactory;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\PimClient\Dto\Brand;
use Ensi\PimClient\Dto\Category;
use Ensi\PimClient\Dto\Product;
use Ensi\PimClient\Dto\ProductArchiveTypeEnum;
use Ensi\PimClient\Dto\ProductImage;
use Ensi\PimClient\Dto\SearchProductsResponse;

class ProductFactory extends BaseApiFactory
{
    protected array $productImages = [];
    protected ?Category $category = null;
    protected ?Brand $brand = null;

    protected function definition(): array
    {
        $definition = [
            'id' => $this->faker->randomNumber(),
            'code' => $this->faker->word(),
            'name' => $this->faker->word(),
            'description' => $this->faker->text(50),
            'external_id' => $this->faker->uuid(),
            'product_type_id' => $this->faker->randomNumber(),
            'country_id' => $this->faker->randomNumber(),
            'manufacturer_id' => $this->faker->randomNumber(),
            'brand_id' => $this->faker->randomNumber(),
            'category_id' => $this->faker->randomNumber(),
            'variant_group_id' => $this->faker->randomNumber(),
            'barcode' => $this->faker->numerify('##############'),
            'weight' => $this->faker->randomFloat(4),
            'weight_gross' => $this->faker->randomFloat(4),
            'length' => $this->faker->randomNumber(),
            'height' => $this->faker->randomNumber(),
            'width' => $this->faker->randomNumber(),
            'ingredients' => $this->faker->text(50),
            'storage_area' => $this->faker->text(5),
            'archive' => $this->faker->randomElement(ProductArchiveTypeEnum::getAllowableEnumValues()),
            'is_new' => $this->faker->boolean(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];

        if ($this->productImages) {
            $definition['images'] = $this->productImages;
        }
        if ($this->category) {
            $definition['category'] = $this->category;
        }
        if ($this->brand) {
            $definition['brand'] = $this->brand;
        }

        return $definition;
    }

    public function withImages(?ProductImage $productImage = null): self
    {
        $this->productImages[] = $productImage ?: ProductImageFactory::new()->make();

        return $this;
    }

    public function withCategory(?Category $category = null): self
    {
        $this->category = $category ?: CategoryFactory::new()->make();

        return $this;
    }

    public function withBrand(?Brand $brand = null): self
    {
        $this->brand = $brand ?: BrandFactory::new()->make();

        return $this;
    }

    public function make(array $extra = []): Product
    {
        return new Product($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchProductsResponse
    {
        return $this->generateResponseSearch(SearchProductsResponse::class, $extra, $count);
    }
}
