<?php

namespace App\Domain\Common\Data\Meta\Fields;

use App\Http\ApiV1\OpenApiGenerated\Dto\FieldTypeEnum;

class ObjectField extends AbstractField
{
    public function __construct(string $code, string $name)
    {
        // Поля объектов всегда должны дополнительно представляться в основном ресурсе модели как строка для вывода
        parent::__construct("{$code}_string", $name);
    }

    protected function type(): string
    {
        return FieldTypeEnum::STRING;
    }
}
