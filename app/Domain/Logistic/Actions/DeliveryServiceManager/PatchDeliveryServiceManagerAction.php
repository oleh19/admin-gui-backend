<?php

namespace App\Domain\Logistic\Actions\DeliveryServiceManager;

use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Ensi\LogisticClient\Dto\DeliveryServiceManager;
use Ensi\LogisticClient\Dto\PatchDeliveryServiceManagerRequest;

class PatchDeliveryServiceManagerAction
{
    public function __construct(protected DeliveryServicesApi $deliveryServicesApi)
    {
    }

    public function execute(int $deliveryServiceManagerId, array $fields): DeliveryServiceManager
    {
        return $this->deliveryServicesApi->patchDeliveryServiceManager(
            $deliveryServiceManagerId,
            new PatchDeliveryServiceManagerRequest($fields)
        )->getData();
    }
}
