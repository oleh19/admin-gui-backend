<?php

namespace App\Domain\Logistic\Actions\DeliveryServiceDocument;

use App\Domain\Logistic\Data\DeliveryServiceDocumentData;
use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Illuminate\Http\UploadedFile;

class SaveDeliveryServiceDocumentFileAction
{
    public function __construct(protected DeliveryServicesApi $deliveryServicesApi)
    {
    }

    public function execute(int $deliveryServiceDocumentId, UploadedFile $file): DeliveryServiceDocumentData
    {
        return new DeliveryServiceDocumentData(
            $this->deliveryServicesApi->uploadDeliveryServiceDocumentFile(
                $deliveryServiceDocumentId,
                $file->openFile(),
                $file->getClientOriginalName()
            )->getData()
        );
    }
}
